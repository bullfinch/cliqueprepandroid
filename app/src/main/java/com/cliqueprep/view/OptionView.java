package com.cliqueprep.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cliqueprep.R;

import io.github.kexanie.library.MathView;

/**
 * TODO: document your custom view class.
 */
public class OptionView extends LinearLayout {

    private MathView mContentView;
    private ImageButton mHelpButton;
    private ImageView mCorrectIcon;
    private ImageView mWrongIcon;
    private Callback mCallback;
    private boolean mActive;
    private CardView mOptionView;

    public OptionView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public OptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public OptionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.option_view, this);

        mActive = true;

        mContentView = (MathView) findViewById(R.id.content);
        mHelpButton = (ImageButton) findViewById(R.id.help_button);
        mCorrectIcon = (ImageView) findViewById(R.id.correct_button);
        mWrongIcon = (ImageView) findViewById(R.id.wrong_button);
        mOptionView = (CardView) findViewById(R.id.option_card_view);

        if (mHelpButton != null) {
            mHelpButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallback != null) {
                        mCallback.helpClicked();
                    }
                }
            });
        }

        if (mContentView != null) {
            mContentView.setEngine(MathView.Engine.MATHJAX);
            mContentView.config("MathJax.Hub.Config({\n" +
                    " tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']]}});");
        }

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActive) {
                    if (mCallback != null) {
                        mCallback.onSelected();
                    }

                    mOptionView.setBackground(getResources().getDrawable(R.drawable.ic_rectangle_solid_blue_border));
                }
            }
        });
    }

    public void setSelected() {
        mOptionView.setBackground(getResources().getDrawable(R.drawable.ic_rectangle_solid_blue_border));
    }

    public void clearSelection() {
        mOptionView.setBackground(getResources().getDrawable(R.drawable.white_background_popup_curve));
    }

    public void setSelection(boolean correct) {
        if (correct) {
            mOptionView.setBackground(getResources().getDrawable(R.drawable.ic_rectangle_solid_greeen_border));
        } else {
            mOptionView.setBackground(getResources().getDrawable(R.drawable.ic_rectangle_solid_red_border));
            setWrong();
        }
    }

    public void setContent(String content) {
        if (mContentView != null) {
            String styledContent = "<div style = 'text-align : center'>" + content + "</div>";
            mContentView.setText(styledContent);
        }
    }

    public void setCorrect() {
        if (mCorrectIcon != null) {
            mCorrectIcon.setVisibility(VISIBLE);
        }

        showHelpButton();
    }

    public void setWrong() {
        if (mWrongIcon != null) {
            mWrongIcon.setVisibility(VISIBLE);
        }
    }

    public void showHelpButton() {
        if (mHelpButton != null) {
            mHelpButton.setVisibility(VISIBLE);
        }
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void setActive() {
        mActive = true;
    }

    public void setInactive() {
        mActive = false;
    }

    public interface Callback {
        void helpClicked();

        void onSelected();
    }
}
