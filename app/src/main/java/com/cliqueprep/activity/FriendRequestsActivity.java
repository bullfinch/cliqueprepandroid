package com.cliqueprep.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.cliqueprep.R;
import com.cliqueprep.adapter.FriendRequestRecylerViewAdapter;
import com.cliqueprep.framework.CliquePrepBaseActivity;

public class FriendRequestsActivity extends CliquePrepBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_requests);

        RecyclerView friendRequestListView = (RecyclerView) findViewById(R.id.friend_request_recycler_view);
        friendRequestListView.setHasFixedSize(true);
        friendRequestListView.setLayoutManager(new LinearLayoutManager(this));
        friendRequestListView.setAdapter(new FriendRequestRecylerViewAdapter());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_selection_course, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            int id = item.getItemId();

            switch (id) {
                case R.id.open_drawer:
                    openSideMenu();
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
