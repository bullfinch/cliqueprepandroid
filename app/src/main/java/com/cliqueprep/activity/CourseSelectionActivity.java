package com.cliqueprep.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Courses;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.adapter.SelectCourseAdapter;
import com.cliqueprep.asynTask.CourseTask;
import com.cliqueprep.framework.CliquePrepBaseActivity;

public class CourseSelectionActivity extends CliquePrepBaseActivity {

    private SelectCourseAdapter mSectionsPagerAdapter;
    private Context mContext;
    private ViewPager mViewPager;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_selection);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = this;

        User.load(mContext).setSelectedCourse(null);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);
        mSectionsPagerAdapter = new SelectCourseAdapter(getSupportFragmentManager(), mContext);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.container);
        if (mViewPager != null) {
            if (mSectionsPagerAdapter != null) {
                mViewPager.setAdapter(mSectionsPagerAdapter);
            }

            if (tabLayout != null) {
                tabLayout.setupWithViewPager(mViewPager);
            }
        }

        new CourseTask(mContext, mLoaderProgressBar, new CourseTask.Callback() {
            @Override
            public void onSuccess(Courses course) {
                if (course != null) {
                    if (mSectionsPagerAdapter != null) {
                        mSectionsPagerAdapter.setCourse(course);
                    }

                    if (mViewPager != null) {
                        if (course.getSubscribedCourse().length == 0) {
                            mViewPager.setCurrentItem(1);
                        } else {
                            mViewPager.setCurrentItem(0);
                        }
                    }
                }
            }
        }).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_selection_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item != null) {
            if (id == R.id.open_drawer) {
                openSideMenu();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
