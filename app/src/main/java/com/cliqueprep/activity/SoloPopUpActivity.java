package com.cliqueprep.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.Model.Question;
import com.cliqueprep.R;
import com.cliqueprep.asynTask.CreateStudySessionTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by noufy on 22/9/16.
 */
public class SoloPopUpActivity extends AppCompatActivity {
    public static final String EXTRA_TEST_DURATION = "duration";

    private Context mContext;
    private TextView mSoloPractiseTimeText;
    private TextView mSoloPracticeTimeSetText;
    private TextView mSoloTimeIntervalText;
    private Button mSoloProceedButton;
    private int mPracticeTime = 20;
    private long mCourseId;
    private ArrayList<Integer> mSelectedTopicIds;
    private ProgressBar mLoadingProgressBar;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solo_practice_popup);
        mContext = this;

        mCourseId = getIntent().getLongExtra(SoloPracticeActivity.EXTRA_COURSE_ID, 0);
        mSelectedTopicIds = getIntent().getIntegerArrayListExtra(SoloPracticeActivity.EXTRA_TOPIC_IDS);
        System.out.println(mSelectedTopicIds);

        mSoloPractiseTimeText = (TextView) findViewById(R.id.practise_time_text);
        mSoloPracticeTimeSetText = (TextView) findViewById(R.id.practice_time_set_text);
        mSoloTimeIntervalText = (TextView) findViewById(R.id.timer_interval_text);
        mSoloProceedButton = (Button) findViewById(R.id.solo_proceed_button);
        mLoadingProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        setFonts();
        setTime(mPracticeTime);

        if (mSoloProceedButton != null) {
            mSoloProceedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createStudySession();
                }
            });
        }
    }

    public void setFonts() {
        FontUtils.setFiraSansRegular(mSoloPractiseTimeText);
        FontUtils.setFiraSansRegular(mSoloPracticeTimeSetText);
        FontUtils.setFiraSansRegular(mSoloTimeIntervalText);
        FontUtils.setFirasansBold(mSoloProceedButton);
    }

    public void onSoloTimeAddClicked(View view) {
        if (mPracticeTime < 60) {
            setTime(++mPracticeTime);
        } else {
            BFUtils.showToast(mContext, getString(R.string.error_max_time_exceeded));
        }
    }

    public void onSoloTimeDecreaseClicked(View view) {
        if (mPracticeTime > 5) {
            setTime(--mPracticeTime);
        } else {
            BFUtils.showToast(mContext, getString(R.string.error_min_time_exceeded));
        }
    }

    public void setTime(int practicetime) {
        if (practicetime >= 0) {
            if (mSoloPracticeTimeSetText != null && mSoloPractiseTimeText != null) {
                mSoloPracticeTimeSetText.setText(Integer.toString(practicetime));
                mSoloPractiseTimeText.setText(Html.fromHtml(getString(R.string.solo_timer_text, practicetime)));
            }
        }
    }

    private void createStudySession() {
        new CreateStudySessionTask(mContext, mCourseId, 1, mPracticeTime, mSelectedTopicIds, mLoadingProgressBar, new CreateStudySessionTask.Callback() {
            @Override
            public void onSuccess(long sessionId, Question[] questions) {
                Intent intent = new Intent(mContext, QuestionActivity.class);
                intent.putExtra(QuestionActivity.EXTRA_KEY_QUESTIONS, new Gson().toJson(questions));
                intent.putExtra(QuestionActivity.EXTRA_KEY_SESSION_ID, sessionId);
                intent.putExtra(SoloPracticeActivity.EXTRA_COURSE_ID, mCourseId);
                intent.putExtra(EXTRA_TEST_DURATION, mPracticeTime);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure() {
                new AlertDialog.Builder(mContext)
                        .setMessage(R.string.error_starting_session)
                        .create().show();
            }
        }).execute();
    }

    public void proceedToPractise(View view) {
        mPracticeTime = 1000;
        createStudySession();
    }
}