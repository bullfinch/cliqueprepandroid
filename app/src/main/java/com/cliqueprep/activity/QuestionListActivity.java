package com.cliqueprep.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.cliqueprep.R;
import com.cliqueprep.adapter.QuestionListAdapter;
import com.cliqueprep.framework.CliquePrepBaseActivity;

public class QuestionListActivity extends CliquePrepBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);

        mContext = this;

        QuestionListAdapter questionListAdapter = new QuestionListAdapter(getSupportFragmentManager(), mContext);
        ViewPager viewPager = (ViewPager) findViewById(R.id.question_list_container);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.question_list_tab);
        if (viewPager != null) {
            if (questionListAdapter != null) {
                viewPager.setAdapter(questionListAdapter);
            }

            if (tabLayout != null) {
                tabLayout.setupWithViewPager(viewPager);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_question_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            int id = item.getItemId();

            if (id == R.id.forward_arrow) {
                openSideMenu();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
