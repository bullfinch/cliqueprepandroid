package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;

public class SplashScreenActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mContext = this;

        new LoaderTask().execute();
    }

    private class LoaderTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                User user = User.load(mContext);
                if (user.isSignedIn()) {
                    Course course = user.getSelectedCourse();
                    if (course != null) {
                        Intent intent = new Intent(mContext, PracticeSelectionActivity.class);
                        intent.putExtra(PracticeSelectionActivity.EXTRA_COURSE, course);
                        startActivity(intent);
                    } else {
                        startActivity(new Intent(mContext, CourseSelectionActivity.class));
                    }
                } else {
                    startActivity(new Intent(mContext, IntroSliderActivity.class));
                }

                finish();
            }
        }
    }
}