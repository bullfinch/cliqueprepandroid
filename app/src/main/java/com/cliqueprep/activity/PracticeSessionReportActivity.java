package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.Model.Report;
import com.cliqueprep.R;
import com.cliqueprep.asynTask.PracticeReportTask;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.util.BFUtils;

public class PracticeSessionReportActivity extends CliquePrepBaseActivity {
    private TextView mSessionPointsText;
    private TextView mAttendedQuestionsText;
    private Context mContext;
    private TextView mCorrectedAnswerText;
    private TextView mWrongAnswerText;
    private TextView mAccuracyText;
    private TextView mAverageTimeText;
    private ProgressBar mLoaderProgressBar;
    private long mStudySessionId;
    private ProgressBar mAccuracyProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_session_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.practice_report_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(mContext, PracticeSelectionActivity.class));
                    finish();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.practise_session_report));
        }

        mContext = this;

        mStudySessionId = getIntent().getLongExtra(QuestionActivity.EXTRA_KEY_SESSION_ID, 0);

        mAccuracyProgressBar = (ProgressBar) findViewById(R.id.accuracy_progressbar);
        mSessionPointsText = (TextView) findViewById(R.id.session_point);
        mAttendedQuestionsText = (TextView) findViewById(R.id.attended_question_text);
        mCorrectedAnswerText = (TextView) findViewById(R.id.no_of_corrected_answer);
        mWrongAnswerText = (TextView) findViewById(R.id.no_of_wrong_answer);
        mAccuracyText = (TextView) findViewById(R.id.accuracy_text);
        mAverageTimeText = (TextView) findViewById(R.id.average_time_text);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progressBar);

        new PracticeReportTask(mContext, mStudySessionId, mLoaderProgressBar, new PracticeReportTask.Callback() {
            @Override
            public void onSuccess(Report report) {
                if (report != null) {
                    mSessionPointsText.setText(report.getPointEarned());
                    mAttendedQuestionsText.setText(report.getAttemptedQuestions());
                    mCorrectedAnswerText.setText(report.getCorrectQuestions());
                    mWrongAnswerText.setText(report.getWrongQuestions());
                    mAccuracyText.setText(String.valueOf(report.getAccuracy()) + "%");
                    mAccuracyProgressBar.setProgress(report.getAccuracy());
                    mAverageTimeText.setText(String.valueOf(report.getAverageTime()));
                }
            }
        }).execute();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, PracticeSelectionActivity.class));
        finish();
    }

    public void onReviewClicked(View view) {
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
    }
}