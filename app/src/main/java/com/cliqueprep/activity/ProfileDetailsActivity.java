package com.cliqueprep.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.cliqueprep.Model.Guardian;
import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.asynTask.UpdateProfileDetailsTask;
import com.cliqueprep.asynTask.UpdateProfilePicTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;

public class ProfileDetailsActivity extends AppCompatActivity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private final int RC_FROM_GALLERY = 102;
    private final int RC_FROM_CAMERA = 103;
    private final int RC_CROP_PICTURE = 104;
    private static final String LOG_TAG = "ProfilePicUpdate";

    public SimpleDraweeView mUserProfileView;
    public EditText mUserNameText;
    public EditText mUserEmailIdText;
    public Spinner mUserGenderSpinner;
    public EditText mUserPhoneText;
    public EditText mUserAddressText;
    public EditText mGuardianNameText;
    public EditText mUserCountryCodeText;
    public EditText mGuardianCountryCodeText;
    public EditText mGuardianEmailIdText;
    public EditText mGuardianPhoneText;
    public EditText mRelationshipText;
    public ImageView mEditUserDetailsView;
    public ImageView mEditGuardianDetailsView;
    public Button mUpdateDetailsButton;
    public Context mContext;
    public TextView mDateText;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        mContext = this;

        mUserCountryCodeText = (EditText) findViewById(R.id.user_country_code_text_field);
        mGuardianCountryCodeText = (EditText) findViewById(R.id.guardian_country_code_text_field);
        mUserProfileView = (SimpleDraweeView) findViewById(R.id.user_image_view);
        mUserNameText = (EditText) findViewById(R.id.user_name_text_field);
        mUserEmailIdText = (EditText) findViewById(R.id.email_id_text_field);
        mUserGenderSpinner = (Spinner) findViewById(R.id.user_gender_spinner);
        mUserPhoneText = (EditText) findViewById(R.id.user_phone_text_field);
        mUserAddressText = (EditText) findViewById(R.id.location_text_field);
        mGuardianNameText = (EditText) findViewById(R.id.guardian_name_text_field);
        mGuardianEmailIdText = (EditText) findViewById(R.id.guardian_email_id_text_field);
        mGuardianPhoneText = (EditText) findViewById(R.id.guardian_phone_text_field);
        mRelationshipText = (EditText) findViewById(R.id.guardian_relation_text_field);
        mEditUserDetailsView = (ImageView) findViewById(R.id.edit_user_details_view);
        mEditGuardianDetailsView = (ImageView) findViewById(R.id.edit_guardian_detail_view);
        mUpdateDetailsButton = (Button) findViewById(R.id.update_details_button);
        mDateText = (TextView) findViewById(R.id.date_picker_textview);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gender_array, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        if (mUserGenderSpinner != null) {
            mUserGenderSpinner.setEnabled(false);
            mUserGenderSpinner.setAdapter(adapter);
            mUserGenderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.v("item", (String) parent.getItemAtPosition(position));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

        setFonts();
        setDataInFields();
    }

    public void setDataInFields() {
        User userLoad = User.load(mContext);
        UserProfileAttributes profile = userLoad.getProfile();
        Guardian guardian = userLoad.getGurdian();

        setProfilePicture();

        if (!TextUtils.isEmpty(userLoad.getEmail()) && mUserEmailIdText != null) {
            mUserEmailIdText.setText(userLoad.getEmail());
        }

        if (!TextUtils.isEmpty(userLoad.getPhoneNo()) && mUserPhoneText != null) {
            mUserPhoneText.setText(userLoad.getPhoneNo());
        }

        if (!TextUtils.isEmpty(userLoad.getCountryCode()) && mUserCountryCodeText != null) {
            mUserCountryCodeText.setText(userLoad.getCountryCode());
        }

        if (profile != null) {
            if (!TextUtils.isEmpty(profile.getUserName()) && mUserNameText != null) {
                mUserNameText.setText(profile.getUserName());
            }

            if (!TextUtils.isEmpty(profile.getDateOfBirth()) && mDateText != null) {
                mDateText.setText(profile.getDateOfBirth());
            }

            if (!TextUtils.isEmpty(profile.getLocation()) && mUserAddressText != null) {
                mUserAddressText.setText(profile.getLocation());
            }

            if (!TextUtils.isEmpty(profile.getGender()) && mUserGenderSpinner != null) {
                String gender = profile.getGender();

                if (gender.equals("Male")) {
                    mUserGenderSpinner.setSelection(0);
                } else if (gender.equals("Female")) {
                    mUserGenderSpinner.setSelection(1);
                } else {
                    mUserGenderSpinner.setSelection(2);
                }
            }
        }

        if (guardian != null) {
            if (!TextUtils.isEmpty(guardian.getGuardianName()) && mGuardianNameText != null) {
                mGuardianNameText.setText(guardian.getGuardianName());
            }

            if (!TextUtils.isEmpty(guardian.getGuardianEmail()) && mGuardianEmailIdText != null) {
                mGuardianEmailIdText.setText(guardian.getGuardianEmail());
            }

            if (!TextUtils.isEmpty(guardian.getPhoneNumber()) && mGuardianPhoneText != null) {
                mGuardianPhoneText.setText(guardian.getPhoneNumber());
            }

            if (!TextUtils.isEmpty(guardian.getCountryCode()) && mGuardianCountryCodeText != null) {
                mGuardianCountryCodeText.setText(guardian.getCountryCode());
            }

            if (!TextUtils.isEmpty(guardian.getRelation()) && mRelationshipText != null) {
                mRelationshipText.setText(guardian.getRelation());
            }
        }
    }

    public void setProfilePicture() {
        User userLoad = User.load(mContext);
        if (userLoad.getPhoto() != null && mUserProfileView != null) {
            final Uri uri = Uri.parse(userLoad.getPhoto());
            ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {

                @Override
                public void onFailure(String id, Throwable throwable) {
                    //BFUtils.showToast(mContext, getString(R.string.error_loading_profile_pic));
                }
            };

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setControllerListener(controllerListener)
                    .build();
            mUserProfileView.setController(controller);
        }
    }

    public void editUserDetails(View view) {
        if (mUserNameText != null) {
            mUserNameText.setEnabled(true);
            mUserNameText.requestFocus();
        }

        if (mUserGenderSpinner != null) {
            mUserGenderSpinner.setEnabled(true);
        }

        if (mUserAddressText != null) {
            mUserAddressText.setEnabled(true);
        }

        if (mUpdateDetailsButton != null) {
            mUpdateDetailsButton.setVisibility(View.VISIBLE);
        }

        if (mDateText != null) {
            mDateText.setEnabled(true);
        }
    }

    public void editGuardianDetails(View view) {
        if (mGuardianNameText != null) {
            mGuardianNameText.setEnabled(true);
            mGuardianNameText.requestFocus();
        }

        if (mGuardianPhoneText != null) {
            mGuardianPhoneText.setEnabled(true);
        }

        if (mGuardianEmailIdText != null) {
            mGuardianEmailIdText.setEnabled(true);
        }

        if (mRelationshipText != null) {
            mRelationshipText.setEnabled(true);
        }

        if (mGuardianCountryCodeText != null) {
            mGuardianCountryCodeText.setEnabled(true);
        }

        if (mUpdateDetailsButton != null) {
            mUpdateDetailsButton.setVisibility(View.VISIBLE);
        }
    }

    public void setFonts() {
        FontUtils.setFiraSansRegular(findViewById(R.id.user_name_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.email_id_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.user_gender_spinner));
        FontUtils.setFiraSansRegular(findViewById(R.id.user_phone_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.date_picker_textview));
        FontUtils.setFiraSansRegular(findViewById(R.id.location_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.guardian_name_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.guardian_email_id_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.guardian_phone_text_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.guardian_relation_text_field));
    }

    public void onDatePickerClicked(View v) {
        if (v == mDateText) {
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            monthOfYear = monthOfYear + 1;
                            DecimalFormat formatter = new DecimalFormat("00");
                            String monthToShow = formatter.format(monthOfYear);
                            String dayToShow = formatter.format(dayOfMonth);
                            String dateToShow = year + "-" + monthToShow + "-" + dayToShow;
                            mDateText.setText(dateToShow);
                        }
                    }, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }

    public void updateDetails(View view) {
        BFUtils.hideSoftKeyboard(ProfileDetailsActivity.this);

        String name = BFUtils.getStringFromEditable(mUserNameText);
        String email = BFUtils.getStringFromEditable(mUserEmailIdText);
        String phone = BFUtils.getStringFromEditable(mUserPhoneText);
        String address = BFUtils.getStringFromEditable(mUserAddressText);
        String guardianName = BFUtils.getStringFromEditable(mGuardianNameText);
        String guardianPhone = BFUtils.getStringFromEditable(mGuardianPhoneText);
        String guardianEmail = BFUtils.getStringFromEditable(mGuardianEmailIdText);
        String relationship = BFUtils.getStringFromEditable(mRelationshipText);
        String userCountryCode = BFUtils.getStringFromEditable(mUserCountryCodeText);
        String guardianCountryCode = BFUtils.getStringFromEditable(mGuardianCountryCodeText);
        String birthday = null;
        String gender = null;

        if (mDateText != null) {
            birthday = mDateText.getText().toString();
        }

        if (mUserGenderSpinner != null) {
            gender = mUserGenderSpinner.getSelectedItem().toString();
        }

        User currentUser = User.load(mContext);
        Guardian guardian = currentUser.getGurdian();
        UserProfileAttributes profile = currentUser.getProfile();

        HashMap<String, String> userProfileAttributes = new HashMap<>();
        HashMap<String, String> guardianAttributes = new HashMap<>();

        if (guardian != null && profile != null) {
            if (isEqual(name, profile.getUserName()) && isEqual(birthday, profile.getDateOfBirth()) && isEqual(address, profile.getLocation()) &&
                    isEqual(guardianName, guardian.getGuardianName()) && isEqual(guardianPhone, guardian.getPhoneNumber()) &&
                    isEqual(guardianEmail, guardian.getGuardianEmail()) && isEqual(relationship, guardian.getRelation()) &&
                    isEqual(guardianCountryCode, guardian.getCountryCode()) && isEqual(gender, profile.getGender())) {
                disableGuardianDetails();
                disableUserDetails();
                BFUtils.showToast(mContext, getString(R.string.no_update_detected));
                return;
            }
        }

        if (mUserNameText.isEnabled() && mUserNameText != null) {
            if (TextUtils.isEmpty(name) || name.length() < 3) {
                BFUtils.setError(mUserNameText, getString(R.string.name_error));
                return;
            }

            if (TextUtils.isEmpty(email) || (!BFUtils.isValidEmailAddress(email))) {
                BFUtils.setError(mUserEmailIdText, getString(R.string.email_error));
                return;
            }

            if (TextUtils.isEmpty(birthday) && mDateText != null) {
                BFUtils.setErrorTextView(mDateText, getString(R.string.date_of_birth_error));
                return;
            }

            if (TextUtils.isEmpty(address) || address.length() < 3) {
                BFUtils.setError(mUserAddressText, getString(R.string.location_error));
                return;
            }

            if (userProfileAttributes != null) {
                userProfileAttributes.put("name", name);
                userProfileAttributes.put("location", address);
                userProfileAttributes.put("gender", gender);
                userProfileAttributes.put("dob", birthday);
            }
        }

        if (mGuardianNameText.isEnabled() && mGuardianNameText != null) {
            if (TextUtils.isEmpty(guardianName) || guardianName.length() < 3) {
                BFUtils.setError(mGuardianNameText, getString(R.string.name_error));
                return;
            }

            if (TextUtils.isEmpty(guardianEmail) || (!BFUtils.isValidEmailAddress(guardianEmail))) {
                BFUtils.setError(mGuardianEmailIdText, getString(R.string.email_error));
                return;
            }

            if (TextUtils.isEmpty(relationship) || relationship.length() < 3) {
                BFUtils.setError(mRelationshipText, getString(R.string.guardian_relationship_error));
                return;
            }

            if (TextUtils.isEmpty(guardianCountryCode)) {
                BFUtils.setError(mGuardianCountryCodeText, getString(R.string.country_code_error));
                return;
            }

            if (TextUtils.isEmpty(guardianPhone) || guardianPhone.length() != 10) {
                BFUtils.setError(mGuardianPhoneText, getString(R.string.phone_no_error));
                return;
            }

            if (guardianAttributes != null) {
                guardianAttributes.put("email", guardianEmail);
                guardianAttributes.put("relation", relationship);
                guardianAttributes.put("country_code", guardianCountryCode);
                guardianAttributes.put("phone_number", guardianPhone);
                guardianAttributes.put("name", guardianName);
            }
        }

        if (profile != null) {
            userProfileAttributes.put("id", profile.getUserId());
        }

        if (guardian != null) {
            guardianAttributes.put("id", guardian.getGuardianId());
        }

        new UpdateProfileDetailsTask(mContext, userProfileAttributes, email, guardianAttributes,
                new UpdateProfileDetailsTask.Callback() {
                    @Override
                    public void onTaskStart() {
                        onUpdateTaskStart();
                    }

                    @Override
                    public void onSuccessfulUpdate() {
                        onTaskSuccess();
                    }

                    @Override
                    public void onUpdateFailure(String error) {
                        onTaskFailure(error);
                    }
                }).execute();

        disableGuardianDetails();
        disableUserDetails();
    }

    public boolean isEqual(String str1, String str2) {
        if (!TextUtils.isEmpty(str1) && !TextUtils.isEmpty(str2)) {
            return str1.equals(str2);
        }

        return false;
    }

    public void disableUserDetails() {
        if (mUserNameText != null) {
            mUserNameText.setEnabled(false);
        }

        if (mUserEmailIdText != null) {
            mUserEmailIdText.setEnabled(false);
        }

        if (mUserGenderSpinner != null) {
            mUserGenderSpinner.setEnabled(false);
        }

        if (mUserAddressText != null) {
            mUserAddressText.setEnabled(false);
        }

        if (mUpdateDetailsButton != null) {
            mUpdateDetailsButton.setVisibility(View.INVISIBLE);
        }

        if (mDateText != null) {
            mDateText.setEnabled(false);
        }
    }

    public void disableGuardianDetails() {
        if (mGuardianNameText != null) {
            mGuardianNameText.setEnabled(false);
        }

        if (mGuardianPhoneText != null) {
            mGuardianPhoneText.setEnabled(false);
        }

        if (mGuardianEmailIdText != null) {
            mGuardianEmailIdText.setEnabled(false);
        }

        if (mRelationshipText != null) {
            mRelationshipText.setEnabled(false);
        }
        if (mGuardianCountryCodeText != null) {
            mGuardianCountryCodeText.setEnabled(false);
        }

        if (mUpdateDetailsButton != null) {
            mUpdateDetailsButton.setVisibility(View.INVISIBLE);
        }
    }

    public void changeProfilePicture(View view) {
        final Activity thisActivity = this;
        final Dialog dialog = new Dialog(mContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_upload_photo_location_selection_dialogue);
        dialog.show();

        final Button openCamera = (Button) dialog.findViewById(R.id.select_from_camera);
        Button openGallery = (Button) dialog.findViewById(R.id.select_from_gallery);
        TextView uploadPhotoText = (TextView) dialog.findViewById(R.id.upload_photo_text);
        TextView gallery = (TextView) dialog.findViewById(R.id.gallery_text);
        TextView camera = (TextView) dialog.findViewById(R.id.camera_text);

        FontUtils.setFirasansBold(uploadPhotoText);
        FontUtils.setFiraSansRegular(camera);
        FontUtils.setFiraSansRegular(gallery);

        if (openCamera != null) {
            openCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(mContext,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) mContext,
                                new String[]{Manifest.permission.CAMERA},
                                RC_FROM_CAMERA);
                    } else {
                        openCameraButtonClicked();
                    }
                }
            });
        }

        if (openGallery != null) {
            openGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(mContext,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions((Activity) mContext,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                RC_FROM_GALLERY);
                    } else {
                        openGalleryButtonClicked();
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        int camera = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE);

        if (camera == PackageManager.PERMISSION_GRANTED &&
                storage == PackageManager.PERMISSION_GRANTED && requestCode == RC_FROM_CAMERA) {
            openCameraButtonClicked();
        }

        if (camera == PackageManager.PERMISSION_GRANTED &&
                storage != PackageManager.PERMISSION_GRANTED && requestCode == RC_FROM_CAMERA) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) mContext,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        RC_FROM_CAMERA);
            }
        }

        if (storage == PackageManager.PERMISSION_GRANTED && requestCode == RC_FROM_GALLERY) {
            openGalleryButtonClicked();
        }
    }

    public void openCameraButtonClicked() {
        Intent takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri fileToSave = Uri.fromFile(getImageLocation());
        if (fileToSave != null) {
            takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, fileToSave);
            startActivityForResult(takePhoto, RC_FROM_CAMERA);
        }
    }

    public void openGalleryButtonClicked() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), RC_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri picUri;

        switch (requestCode) {

            case RC_FROM_GALLERY:
                if (data != null && resultCode == Activity.RESULT_OK) {
                    picUri = data.getData();
                    performCrop(picUri);
                }

                break;

            case RC_FROM_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    performCrop(Uri.fromFile(getImageLocation()));
                }

                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (result != null) {
                        Uri resultUri = result.getUri();
                        saveSelectedImage(resultUri);
                    }
                }

                break;
        }
    }

    private void performCrop(Uri picUri) {
        CropImage.activity(picUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void saveSelectedImage(Uri imageUri) {
        String sourceFilename = imageUri.getPath();
        String destinationFilename = getImageLocation().getPath();

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(sourceFilename));
            bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while (bis.read(buf) != -1);
        } catch (IOException e) {

        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {

            }
        }

        String email = BFUtils.getStringFromEditable(mUserEmailIdText);

        new UpdateProfilePicTask(mContext, email, new UpdateProfilePicTask.Callback() {
            @Override
            public void onTaskStart() {
                ProfileDetailsActivity.this.onUpdateTaskStart();
            }

            @Override
            public void onSuccessfulUpdate() {
                setDataInFields();
                onSuccessPhotoUpload();
            }

            @Override
            public void onUpdateFailure(String error) {
                onTaskFailure(error);
            }
        }).execute();
    }

    private File getImageLocation() {
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CLIQUEPREP");
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String path = directory.getPath() + File.separator + "PROFILE_PIC" + ".jpg";
        return new File(path);
    }

    public void onUpdateTaskStart() {
        if (mLoaderProgressBar != null) {
            mLoaderProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void onTaskSuccess() {
        if (mLoaderProgressBar != null) {
            mLoaderProgressBar.setVisibility(View.GONE);
        }

        BFUtils.showToast(mContext, getString(R.string.data_updates_successfully));
        startActivity(new Intent(mContext, PracticeSelectionActivity.class));
        finish();
    }

    public void onSuccessPhotoUpload() {
        if (mLoaderProgressBar != null) {
            mLoaderProgressBar.setVisibility(View.GONE);
        }

    }

    public void onTaskFailure(String error) {
        if (mLoaderProgressBar != null) {
            mLoaderProgressBar.setVisibility(View.GONE);
        }

        BFUtils.showToast(mContext, error);
    }

    public void onBackButtonClicked(View view) {
        finish();
    }
}