package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

import java.util.HashMap;

public class PayeeInfoActivity extends AppCompatActivity {
    private HashMap<String, String> mPayeeInfoMap;
    private Context mContext;
    private EditText mNameField;
    private EditText mEmailField;
    private EditText mMobileField;
    private EditText mAddressOneField;
    private EditText mAddressTwoField;
    private EditText mCityField;
    private EditText mStateField;
    private EditText mCountryField;
    private EditText mZipcodeField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payee_info);
        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mPayeeInfoMap = new HashMap<>();
        setFonts();
        setDataInFields();
    }

    private void setDataInFields() {
        mNameField = (EditText) findViewById(R.id.personal_info_name_field);
        mEmailField = (EditText) findViewById(R.id.personal_info_email_field);
        mMobileField = (EditText) findViewById(R.id.personal_info_phone_field);
        mAddressOneField = (EditText) findViewById(R.id.personal_info_address1_field);
        mAddressTwoField = (EditText) findViewById(R.id.personal_info_address2_field);
        mCityField = (EditText) findViewById(R.id.personal_info_city_field);
        mStateField = (EditText) findViewById(R.id.personal_info_state_field);
        mCountryField = (EditText) findViewById(R.id.personal_info_country_field);
        mZipcodeField = (EditText) findViewById(R.id.personal_info_zip_field);

        User currentUser = User.load(mContext);
        UserProfileAttributes profile = currentUser.getProfile();

        if (mEmailField != null && !TextUtils.isEmpty(currentUser.getEmail())) {
            mEmailField.setText(currentUser.getEmail());
            mPayeeInfoMap.put("email", currentUser.getEmail());
        }

        if (mMobileField != null && !TextUtils.isEmpty(currentUser.getPhoneNo())) {
            mMobileField.setText(currentUser.getPhoneNo());
            mPayeeInfoMap.put("phone", currentUser.getPhoneNo());
        }

        if (profile != null) {
            if (mNameField != null && !TextUtils.isEmpty(profile.getUserName())) {
                mNameField.setText(profile.getUserName());
                mPayeeInfoMap.put("firstname", profile.getUserName());
            }

            if (mAddressOneField != null && !TextUtils.isEmpty(profile.getAddressLineOne())) {
                mAddressOneField.setText(profile.getAddressLineOne());
            }

            if (mAddressTwoField != null && !TextUtils.isEmpty(profile.getAddressLineTwo())) {
                mAddressTwoField.setText(profile.getAddressLineTwo());
            }

            if (mCityField != null && !TextUtils.isEmpty(profile.getCity())) {
                mCityField.setText(profile.getCity());
            }

            if (mStateField != null && !TextUtils.isEmpty(profile.getState())) {
                mStateField.setText(profile.getState());
            }

            if (mCountryField != null && !TextUtils.isEmpty(profile.getCountry())) {
                mCountryField.setText(profile.getCountry());
            }
        }
    }

    private void setFonts() {
        FontUtils.setFiraSansMedium(findViewById(R.id.toolbar_title));
        FontUtils.setFiraSansMedium(findViewById(R.id.step_count));
        FontUtils.setFiraSansRegular(findViewById(R.id.payment_info_text));
        FontUtils.setFiraSansRegular(findViewById(R.id.address_info_text));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_name_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_email_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_phone_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_address1_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_address2_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_city_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_state_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_country_field));
        FontUtils.setFiraSansRegular(findViewById(R.id.personal_info_zip_field));
    }

    public void onGotoPaymentClicked(View view) {
        BFUtils.hideSoftKeyboard(PayeeInfoActivity.this);

        if (mAddressOneField != null) {
            String addressOne = BFUtils.getStringFromEditable(mAddressOneField);
            if (TextUtils.isEmpty(addressOne)) {
                BFUtils.setError(mAddressOneField, getString(R.string.address_empty_error));
                return;
            }

            mPayeeInfoMap.put("address1", addressOne);
        }

        if (mAddressTwoField != null) {
            String addressTwo = BFUtils.getStringFromEditable(mAddressTwoField);
            if (TextUtils.isEmpty(addressTwo)) {
                BFUtils.setError(mAddressTwoField, getString(R.string.address_empty_error));
                return;
            }

            mPayeeInfoMap.put("address2", addressTwo);
        }

        if (mCityField != null) {
            String city = BFUtils.getStringFromEditable(mCityField);
            if (TextUtils.isEmpty(city)) {
                BFUtils.setError(mCityField, getString(R.string.city_empty_error));
                return;
            }

            mPayeeInfoMap.put("city", city);
        }

        if (mStateField != null) {
            String state = BFUtils.getStringFromEditable(mStateField);
            if (TextUtils.isEmpty(state)) {
                BFUtils.setError(mStateField, getString(R.string.state_empty_error));
                return;
            }

            mPayeeInfoMap.put("state", state);
        }

        if (mCountryField != null) {
            String country = BFUtils.getStringFromEditable(mCountryField);
            if (TextUtils.isEmpty(country)) {
                BFUtils.setError(mCountryField, getString(R.string.country_empty_error));
                return;
            }

            mPayeeInfoMap.put("country", country);
        }

        if (mZipcodeField != null) {
            String zipcode = BFUtils.getStringFromEditable(mZipcodeField);
            if (TextUtils.isEmpty(zipcode) || zipcode.length() < 6) {
                BFUtils.setError(mZipcodeField, getString(R.string.invalid_zip_code_error));
                return;
            }

            mPayeeInfoMap.put("zipcode", zipcode);
        }

        Intent intent = new Intent(mContext, PaymentMethodActivity.class);
        intent.putExtra(PlanSelectionActivity.EXTRA_DETAILS, getIntent().getBundleExtra(PlanSelectionActivity.EXTRA_DETAILS));
        intent.putExtra(PaymentMethodActivity.EXTRA_PAYEE_INFO_MAP, mPayeeInfoMap);
        startActivity(intent);
    }
}
