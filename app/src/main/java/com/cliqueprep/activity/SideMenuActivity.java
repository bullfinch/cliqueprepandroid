package com.cliqueprep.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SideMenuActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView mCalendarImage;
    private TextView mDaysToText;
    private TextView mCourseYear;
    private TextView mCalenderCourseName;
    private TextView mUserName;
    private TextView mUserEmailId;
    private TextView mChangeCourseText;
    private TextView mProgressText;
    private TextView mNotificationsText;
    private TextView mInviteFriendsText;
    private TextView mContactsText;
    private TextView mLogoutText;
    private TextView mCourseNameText;
    private TextView mNumberOfDays;
    public Context mContext;
    private SimpleDraweeView mUserProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        View childView = LayoutInflater.from(this).inflate(layoutResID, null, false);
        if (childView != null) {
            View rootView = LayoutInflater.from(this).inflate(R.layout.activity_side_menu, null, false);
            if (rootView != null) {
                RelativeLayout mainViewContainer = (RelativeLayout) rootView.findViewById(R.id.main_view_container);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                mainViewContainer.addView(childView, params);
                super.setContentView(rootView);
            }

            mUserProfileImage = (SimpleDraweeView) rootView.findViewById(R.id.user_image_view);
            mCalendarImage = (ImageView) rootView.findViewById(R.id.calender_image);
            mDaysToText = (TextView) rootView.findViewById(R.id.days_to_course_text_view);
            mCourseNameText = (TextView) rootView.findViewById(R.id.course_name_text);
            mCalenderCourseName = (TextView) rootView.findViewById(R.id.name_of_course_textView);
            mCourseYear = (TextView) rootView.findViewById(R.id.year_of_course_text);
            mUserName = (TextView) rootView.findViewById(R.id.user_full_name_text);
            mUserEmailId = (TextView) rootView.findViewById(R.id.user_email_id_text);
            mChangeCourseText = (TextView) rootView.findViewById(R.id.change_course_text);
            mProgressText = (TextView) rootView.findViewById(R.id.side_menu_progress_text_view);
            mNotificationsText = (TextView) rootView.findViewById(R.id.side_menu_notifications_text_view);
            mInviteFriendsText = (TextView) rootView.findViewById(R.id.side_menu_invite_friends_text_view);
            mContactsText = (TextView) rootView.findViewById(R.id.side_menu_contact_text_view);
            mLogoutText = (TextView) rootView.findViewById(R.id.side_menu_logout_text_view);
            mNumberOfDays = (TextView) rootView.findViewById(R.id.number_of_remaining_text);
        }

        setFontText();
        setUpCustomActionBar();
//        setUserDataInDrawer();
    }

    public void setFontText() {
        FontUtils.setFiraSansRegular(mUserEmailId);
        FontUtils.setFiraSansRegular(mUserName);
        FontUtils.setFiraSansRegular(mChangeCourseText);
        FontUtils.setFirasansBold(mCourseNameText);
        FontUtils.setFirasansBold(mNumberOfDays);
        FontUtils.setFiraSansRegular(mProgressText);
        FontUtils.setFiraSansRegular(mNotificationsText);
        FontUtils.setFiraSansRegular(mInviteFriendsText);
        FontUtils.setFiraSansRegular(mContactsText);
        FontUtils.setFiraSansRegular(mLogoutText);
    }

    public void setUserDataInDrawer() {
        User currentUser = User.load(mContext);
        UserProfileAttributes profile = currentUser.getProfile();
        Course course = currentUser.getSelectedCourse();

        setProfilePicture();

        if (profile != null && !TextUtils.isEmpty(profile.getUserName()) && mUserName != null) {
            mUserName.setText(profile.getUserName());
        }

        if (!TextUtils.isEmpty(currentUser.getEmail()) && mUserEmailId != null) {
            mUserEmailId.setText(currentUser.getEmail());
        }

        if (course != null) {
            if (mCalendarImage != null) {
                mCalendarImage.setVisibility(View.VISIBLE);
            }

            if (mDaysToText != null) {
                mDaysToText.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(course.getCourseName())) {
                if (mCalenderCourseName != null) {
                    mCalenderCourseName.setText(course.getCourseName());
                }

                String courseName = "- " + course.getCourseName();
                if (mCourseNameText != null) {
                    mCourseNameText.setText(courseName);
                }
            }

            if (course.getValidity() != null) {
                Date date = course.getExamDate();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
                String examYear = dateFormat.format(date);

                if (mCourseYear != null) {
                    mCourseYear.setVisibility(View.VISIBLE);
                    mCourseYear.setText(examYear);
                }
            }

            Date examDate = course.getExamDate();
            int difference = calculateDaysToExam(examDate);

            if (mNumberOfDays != null) {
                mNumberOfDays.setText(String.valueOf(difference));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserDataInDrawer();
    }

    public void setProfilePicture() {
        User currentUser = User.load(mContext);
        if (mUserProfileImage != null && currentUser.getPhoto() != null) {
            final Uri uri = Uri.parse(currentUser.getPhoto());
            ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {

                @Override
                public void onFailure(String id, Throwable throwable) {
//                    BFUtils.showToast(mContext, getString(R.string.error_loading_profile_pic));
                }
            };

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(uri)
                    .setControllerListener(controllerListener)
                    .build();
            mUserProfileImage.setController(controller);
        }
    }


    public int calculateDaysToExam(Date dateOfExam) {
        Calendar calendar = Calendar.getInstance();
        Date currentDate = (Date) calendar.getTime();

        if (currentDate == null || dateOfExam == null) {
            return 0;
        }

        return (int) ((dateOfExam.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
    }

    private void setUpCustomActionBar() {
        View customView = getCustomActionBarView();
        if (customView != null) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                // enable the custom view visibility
                actionBar.setDisplayShowCustomEnabled(true);

                ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
                actionBar.setCustomView(customView, params);
                mToolbar = (Toolbar) customView.getParent();
                if (mToolbar != null) {
                    mToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
                    mToolbar.setContentInsetsAbsolute(0, 0);
                }
            }
        }
    }

    protected View getCustomActionBarView() {
        return null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }
}
