package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cliqueprep.R;
import com.cliqueprep.adapter.IntroSliderPagerAdapter;
import com.cliqueprep.util.FontUtils;
import com.viewpagerindicator.CirclePageIndicator;


public class IntroSliderActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_slider);

        mContext = this;

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        IntroSliderPagerAdapter pagerAdapter = new IntroSliderPagerAdapter(getSupportFragmentManager(), mContext);
        if (viewPager != null) {
            viewPager.setAdapter(pagerAdapter);
        }

        CirclePageIndicator dotIndicator = (CirclePageIndicator) findViewById(R.id.dots);
        if (dotIndicator != null && viewPager != null) {
            dotIndicator.setViewPager(viewPager);
        }

        customizeButtons();
    }

    public void customizeButtons() {
        Button signinButton = (Button) findViewById(R.id.signin_button);
        Button signupButton = (Button) findViewById(R.id.signup_button);
        if (signinButton != null) {
            signinButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SigninActivity.class);
                    startActivity(intent);
                }
            });
        }
        if (signupButton != null) {
            signupButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, SignUpActivity.class);
                    startActivity(intent);
                }
            });
        }

        FontUtils.setFirasansBold(signinButton);
        FontUtils.setFirasansBold(signupButton);
    }
}