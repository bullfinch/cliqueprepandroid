package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cliqueprep.R;
import com.cliqueprep.asynTask.SignUpDetailedTask;
import com.cliqueprep.asynTask.SocialSignUpTask;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.Model.User;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

public class SignUpDetailsActivity extends AppCompatActivity implements FacebookCallback<LoginResult> {
    public EditText mEmailAddressField;
    public EditText mPasswordField;
    public EditText mConfirmPasswordField;
    public Button mSignUpButton;
    public Button mFacebookSignUpButton;
    public Context mContext;
    private CallbackManager mCallbackManager;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_details);

        mContext = this;

        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, this);

        mEmailAddressField = (EditText) findViewById(R.id.email_field);
        mPasswordField = (EditText) findViewById(R.id.password_field);
        mConfirmPasswordField = (EditText) findViewById(R.id.confirm_password_field);
        mSignUpButton = (Button) findViewById(R.id.signup_button);
        mFacebookSignUpButton = (Button) findViewById(R.id.facebook_signup_button);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        setFont();
    }

    private void setFont() {
        FontUtils.setFiraSansRegular(mEmailAddressField);
        FontUtils.setFiraSansRegular(mPasswordField);
        FontUtils.setFiraSansRegular(mConfirmPasswordField);
        FontUtils.setFirasansBold(mFacebookSignUpButton);
        FontUtils.setFirasansBold(mSignUpButton);
    }

    public void onNavigationButtonClicked(View view) {
        finish();
    }

    public void onSignUpDetailClicked(View view) {
        BFUtils.hideSoftKeyboard(SignUpDetailsActivity.this);

        String email = BFUtils.getStringFromEditable(mEmailAddressField);
        if (!BFUtils.isValidEmailAddress(email)) {
            BFUtils.setError(mEmailAddressField, getString(R.string.email_error));
            return;
        }

        String password = BFUtils.getStringFromEditable(mPasswordField);
        if (TextUtils.isEmpty(password) || password.length() < 8) {
            BFUtils.setError(mPasswordField, getString(R.string.password_error));
            return;
        }

        String confirmpassword = BFUtils.getStringFromEditable(mConfirmPasswordField);
        if (TextUtils.isEmpty(confirmpassword)) {
            BFUtils.setError(mConfirmPasswordField, getString(R.string.password_error));
            return;
        }

        if (!confirmpassword.equals(password)) {
            BFUtils.setError(mConfirmPasswordField, getString(R.string.password_match_error));
            return;
        }

        new SignUpDetailedTask(mContext, email, password, confirmpassword, mLoaderProgressBar).execute();
    }

    public void onSocialSignInClicked(View view) {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_birthday"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        User user = User.load(mContext);
        new SocialSignUpTask(mContext, user.getPhoneNo(), loginResult.getAccessToken().getToken(), CliqueprepApplication.SIGN_UP_WITH_FB, mLoaderProgressBar).execute();
    }

    @Override
    public void onCancel() {
        BFUtils.showToast(mContext, getString(R.string.social_sign_up_error));
    }

    @Override
    public void onError(FacebookException error) {
        Log.e("fb error", String.valueOf(error));
    }
}
