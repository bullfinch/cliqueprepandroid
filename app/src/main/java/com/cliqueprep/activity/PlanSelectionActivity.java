package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.Model.CourePlan;
import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.CourseDetails;
import com.cliqueprep.R;
import com.cliqueprep.adapter.PlansFragmentAdapter;
import com.cliqueprep.asynTask.FreeTrialSubscriptionTask;
import com.cliqueprep.asynTask.GetCoursePlanTask;
import com.cliqueprep.asynTask.InitializeSubscriptionTask;
import com.cliqueprep.fragment.PlanDetailsFragment;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class PlanSelectionActivity extends AppCompatActivity implements PlanDetailsFragment.OnBuyNowClickListener {
    public static String EXTRA_COURSE = "course";
    public static String EXTRA_TRANSACTION_ID = "transactionId";
    public static String EXTRA_PLAN = "plan";
    public static String EXTRA_DETAILS = "details";

    private Course mCourse;
    private Context mContext;
    private ViewPager mViewPager;
    ProgressBar mProgressBar;
    private PlansFragmentAdapter mPlanSectionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_selection);
        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (getIntent() != null) {
            mCourse = (Course) getIntent().getSerializableExtra(EXTRA_COURSE);
        }

        mProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);
        new GetCoursePlanTask(this, mProgressBar, mCourse.getCourseId(), new GetCoursePlanTask.Callback() {
            @Override
            public void onSuccess(CourseDetails courseDetails) {
                initializeTab(courseDetails);
            }
        }).execute();

        initializeViews();
        setFonts();
    }

    private void initializeTab(CourseDetails courseDetails) {
        ArrayList<CourePlan> coursePlans = courseDetails.getPlans();
        if (coursePlans != null) {
            mPlanSectionAdapter = new PlansFragmentAdapter(getSupportFragmentManager(), mContext);
            mPlanSectionAdapter.addPlans(coursePlans);
            mViewPager = (ViewPager) findViewById(R.id.plans_view_pager);
            if (mViewPager != null) {
                if (mPlanSectionAdapter != null) {
                    mViewPager.setAdapter(mPlanSectionAdapter);
                }
                setupTabs(coursePlans);
            }
        }
    }

    private void setupTabs(ArrayList<CourePlan> coursePlans) {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.package_tab);
        if (tabLayout != null) {
            tabLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < coursePlans.size(); i++) {
                TabLayout.Tab tab = tabLayout.newTab();
                LinearLayout customTab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.plan_custom_tab, null);

                TextView title = (TextView) customTab.findViewById(R.id.tab_title);
                if (title != null) {
                    long amount = coursePlans.get(i).getAmount();
                    String titleText = getString(R.string.rupees, amount);
                    if (amount == 0) {
                        titleText = getString(R.string.free);
                    }
                    title.setText(titleText);
                }

                tab.setCustomView(customTab);
                tabLayout.addTab(tab);
            }

            if (mViewPager != null) {
                mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            }

            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mViewPager.setCurrentItem(tab.getPosition());

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    private void initializeViews() {
        if (mCourse != null) {
            TextView courseNameView = (TextView) findViewById(R.id.course_name);
            if (courseNameView != null && !TextUtils.isEmpty(mCourse.getCourseName())) {
                courseNameView.setText(mCourse.getCourseName());
            }

            SimpleDraweeView courseIcon = (SimpleDraweeView) findViewById(R.id.course_icon);
            if (courseIcon != null && !TextUtils.isEmpty(mCourse.getCourseUrl())) {
                courseIcon.setImageURI(Uri.parse(mCourse.getCourseUrl()));
            }
        }
    }

    public void setFonts() {
        FontUtils.setFirasansBold(findViewById(R.id.course_name));
        FontUtils.setFiraSansRegular(findViewById(R.id.toolbar_title));
    }

    @Override
    public void OnBuyNowClicked(final CourePlan plan) {
        if (plan != null) {
            if (plan.getAmount() == 0) {
                new FreeTrialSubscriptionTask(mContext, mProgressBar, plan.getId(), new FreeTrialSubscriptionTask.Callback() {
                    @Override
                    public void onSuccess() {
                        Intent intent = new Intent(mContext, PracticeSelectionActivity.class);
                        if (mCourse != null) {
                            intent.putExtra(PracticeSelectionActivity.EXTRA_COURSE, mCourse);
                        }

                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }).execute();
            } else {
                new InitializeSubscriptionTask(mContext, mProgressBar, plan.getId(), new InitializeSubscriptionTask.Callback() {
                    @Override
                    public void onSuccess(String transactionId) {
                        if (!TextUtils.isEmpty(transactionId)) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(EXTRA_COURSE, mCourse);
                            bundle.putSerializable(EXTRA_PLAN, plan);
                            bundle.putString(EXTRA_TRANSACTION_ID, transactionId);

                            Intent intent = new Intent(mContext, PayeeInfoActivity.class);
                            intent.putExtra(EXTRA_DETAILS, bundle);
                            startActivity(intent);
                        }
                    }
                }).execute();
            }
        }
    }
}