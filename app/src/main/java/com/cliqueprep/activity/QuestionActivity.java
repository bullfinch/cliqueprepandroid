package com.cliqueprep.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cliqueprep.Model.Attempt;
import com.cliqueprep.Model.Question;
import com.cliqueprep.Model.SubQuestion;
import com.cliqueprep.R;
import com.cliqueprep.adapter.SubQuestionsFragmentAdapter;
import com.cliqueprep.asynTask.GetNextQuestionSetTask;
import com.cliqueprep.asynTask.SubmitQuestionAttemptTask;
import com.cliqueprep.fragment.SubQuestionFragment;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.FontUtils;
import com.cliqueprep.view.NonSwipeableViewPager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import io.github.kexanie.library.MathView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by asif on 24/09/16.
 */
public class QuestionActivity extends AppCompatActivity {
    public static final String EXTRA_KEY_QUESTIONS = "questions";
    public static final String EXTRA_KEY_SESSION_ID = "session_id";

    private Context mContext;
    private ArrayList<Question> mQuestions;
    private long mStudySessionId;
    private Question mCurrentQuestion;
    ArrayList<Attempt> mUnsyncedAttempts;
    private SubQuestionFragment mSubQuestionFragment;
    private boolean mSyncInProgress;
    private long mCourseId;
    private TextView mSetTimerText;
    private ProgressBar mTimeProgress;
    private CardView mSyncingCard;
    private CardView mSyncFailedCard;

    private long mQuestionEndTime;
    private CountDownTimer mCountdownTimer;
    private boolean mPauseQuestionTimer;
    private boolean mIsFirstQuestion;
    private NonSwipeableViewPager mViewPager;
    private SubQuestionsFragmentAdapter mSubQuestionsFragmentAdapter;
    private TextView mSyncingMessageView;
    private int mSyncedAttemptsCount;
    private int mUnsyncedAttemptsCount;
    private int mTotalUnsyncedAttempts;
    private ProgressBar mSyncingProgress;
    private RelativeLayout mExplanationLayout;
    private MathView mExplanationView;
    private Animation mBottomUp;
    private Animation mBottomDown;
    private TextView mTestExpiredMessage;
    private TextView mContinueOrNotText;
    private Button mQuitButton;
    private Button mContinueButton;
    private boolean mContinueTest = false;
    private ImageButton mTestQuitButton;
    private SubQuestion mFirstSubQuestion;
    private boolean mHelpVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        mContext = this;

        mQuestions = new ArrayList<>();
        mUnsyncedAttempts = new ArrayList<>();
        mSyncInProgress = false;
        mPauseQuestionTimer = false;
        mIsFirstQuestion = true;

        mSetTimerText = (TextView) findViewById(R.id.timer_text);
        mTimeProgress = (ProgressBar) findViewById(R.id.time_progress);
        mViewPager = (NonSwipeableViewPager) findViewById(R.id.sub_question_viewpager);
        mSubQuestionsFragmentAdapter = new SubQuestionsFragmentAdapter(getSupportFragmentManager(), mContext);
        mSyncingProgress = (ProgressBar) findViewById(R.id.progress);
        mSyncingMessageView = (TextView) findViewById(R.id.message);
        mSyncingCard = (CardView) findViewById(R.id.sync_attempts_card);
        mSyncFailedCard = (CardView) findViewById(R.id.sync_failed_card);
        mBottomUp = AnimationUtils.loadAnimation(mContext, R.anim.bottom_up);
        mBottomDown = AnimationUtils.loadAnimation(mContext, R.anim.bottom_down);
        mTestQuitButton = (ImageButton) findViewById(R.id.test_quit_view);

        mStudySessionId = getIntent().getLongExtra(EXTRA_KEY_SESSION_ID, 0);
        mCourseId = getIntent().getLongExtra(SoloPracticeActivity.EXTRA_COURSE_ID, 0);
        String questionsJson = getIntent().getStringExtra(EXTRA_KEY_QUESTIONS);
        Gson gson = new Gson();
        Question[] questions = gson.fromJson(questionsJson, Question[].class);
        Collections.addAll(mQuestions, questions);

        if (mSubQuestionsFragmentAdapter != null) {
            if (mQuestions != null) {
                mSubQuestionsFragmentAdapter.addQuestions(mQuestions);
            }

            if (mViewPager != null) {
                mViewPager.setAdapter(mSubQuestionsFragmentAdapter);
                mViewPager.setOffscreenPageLimit(2);
            }
        }

        if (mSetTimerText != null) {
            mSetTimerText.setText(R.string._00_00);
        }

        FontUtils.setFiraSansRegular(mSetTimerText);
        FontUtils.setFiraSansRegular(mSyncingMessageView);

        initializeExplanation();
        nextQuestion();
        showHelpPages();
    }

    public void nextQuestion() {
        hideHelp();

        if (mQuestions.size() > 0) {
            mCurrentQuestion = mQuestions.get(0);
            mQuestions.remove(0);
            setExplanation();

            if (mViewPager != null && !mIsFirstQuestion) {
                int currentItem = mViewPager.getCurrentItem();
                mViewPager.setCurrentItem(++currentItem);
            }

            mIsFirstQuestion = false;
            mPauseQuestionTimer = false;
            mQuestionEndTime = System.currentTimeMillis() + mCurrentQuestion.getTime() * 1000;

            if (mTimeProgress != null) {
                mTimeProgress.setProgress(mCurrentQuestion.getTime());
                mTimeProgress.setMax(mCurrentQuestion.getTime());
            }
        }

        if (mQuestions.size() < 5) {
            new GetNextQuestionSetTask(mContext, mStudySessionId, new GetNextQuestionSetTask.Callback() {
                @Override
                public void onSuccess(Question[] questions) {
                    Collections.addAll(mQuestions, questions);
                    if (mSubQuestionsFragmentAdapter != null) {
                        mSubQuestionsFragmentAdapter.addQuestions(questions);
                    }
                }

                @Override
                public void onFailure() {
                }
            }).execute();
        }
    }

    @Override
    protected void onDestroy() {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
        }
        super.onDestroy();
    }

    private void setExplanation() {
        if (mExplanationView != null && mCurrentQuestion != null) {
            SubQuestion subQuestion = mCurrentQuestion.getSubQuestion();
            String explanation;
            if (subQuestion != null && !TextUtils.isEmpty(subQuestion.getExplanation())) {
                explanation = "<div style = 'text-align: center; font-size: 15px'>" + subQuestion.getExplanation() + "</div>";
            } else {
                explanation = "<div style = 'text-align: center; color: #AAB3B0'>" + getString(R.string.no_explanation_message) + "</div>";
            }

            mExplanationView.setText(explanation);
        }
    }

    public void submitAttempt(Attempt attempt) {
        mPauseQuestionTimer = true;

        attempt.setQuestionId(mCurrentQuestion.getId());
        mUnsyncedAttempts.add(attempt);

        mSyncInProgress = true;
        new SubmitQuestionAttemptTask(mContext, attempt, mStudySessionId, new SubmitQuestionAttemptTask.Callback() {
            @Override
            public void onSuccess(Attempt attempt) {
                mUnsyncedAttempts.remove(attempt);
                mSyncInProgress = false;
            }

            @Override
            public void onFailure(Attempt attempt) {
                mSyncInProgress = false;
            }
        }).execute();
    }

    public void setCurrentSubQuestion(SubQuestion subQuestion) {
        if (mCurrentQuestion != null) {
            SubQuestion firstSubQuestion = mCurrentQuestion.getSubQuestion();
            if (firstSubQuestion != null && subQuestion != null
                    && firstSubQuestion.getId() == subQuestion.getId()) {
                /**
                 * Storing the first sub question. This is required by the setTimer() method.
                 */
                mFirstSubQuestion = subQuestion;
            }
        }
    }

    public void startSoloTestTimer() {
        if (!isHelpVisible()) {
            setTimer();
        }
    }

    public void setTimer() {
        /*
         * This function should be called only once after the first question is loaded, we are
         * passing the subquestion to check that.
         */

        if (mFirstSubQuestion != null && mCurrentQuestion != null
                && mCurrentQuestion.hasSubQuestion()
                && mCurrentQuestion.getSubQuestion().getId() == mFirstSubQuestion.getId()
                && mCountdownTimer == null) {

            long sessionDuration;
            if (mContinueTest) {
                sessionDuration = 1000;
            } else {
                sessionDuration = getIntent().getIntExtra(SoloPopUpActivity.EXTRA_TEST_DURATION, 0);
            }

            if (sessionDuration == 1000) {
                LinearLayout timerLayout = (LinearLayout) findViewById(R.id.timer_layout);
                if (timerLayout != null) {
                    timerLayout.setVisibility(View.GONE);
                }
            }

            sessionDuration = sessionDuration * 60 * 1000;
            mQuestionEndTime = System.currentTimeMillis() + mCurrentQuestion.getTime() * 1000;

            mCountdownTimer = new CountDownTimer(sessionDuration, 1000) {
                public void onTick(long millisUntilFinished) {
                    long questionTimeLeft = (mQuestionEndTime - System.currentTimeMillis()) / 1000;
                    if (!mPauseQuestionTimer) {
                        mTimeProgress.setProgress((int) questionTimeLeft);
                    }

                    if (questionTimeLeft <= 0 && !mPauseQuestionTimer) {
                        sendTimeExpiredBroadcast();
                    }

                    if (millisUntilFinished <= 0) {
                        mSetTimerText.setText(R.string._00_00);
                        onEndSessionClick(null);
                    } else {
                        long minutesLeft = (millisUntilFinished / 1000) / 60;
                        long secondsLeft = (millisUntilFinished / 1000) % 60;
                        String timeString = String.format(Locale.getDefault(), "%2d:%02d", minutesLeft, secondsLeft);
                        mSetTimerText.setText(timeString);
                    }
                }

                public void onFinish() {
                    mSetTimerText.setText(getResources().getText(R.string.timer));
                    onEndSessionClick(null);
                }

            }.start();
        }
    }

    private void sendTimeExpiredBroadcast() {
        Intent intent = new Intent();
        intent.setAction(CliqueprepApplication.ACTION_TIME_EXPIRED(mContext));
        if (mCurrentQuestion != null) {
            SubQuestion subQuestion = mCurrentQuestion.getSubQuestion();
            if (subQuestion != null) {
                intent.putExtra(CliqueprepApplication.EXTRA_SUBQUESTION_ID, subQuestion.getId());
            }
        }
        sendBroadcast(intent);
    }

    public void syncAttemptsAndEndSession(final View view) {
        LinearLayout syncQuestionsLayout = (LinearLayout) findViewById(R.id.sync_attempts_layout);
        if (syncQuestionsLayout != null) {
            syncQuestionsLayout.setVisibility(View.VISIBLE);
        }

        if (mSyncingCard != null) {
            mSyncingCard.setVisibility(View.VISIBLE);
        }

        if (mSyncFailedCard != null) {
            mSyncFailedCard.setVisibility(View.GONE);
        }

        if (mUnsyncedAttempts != null && mUnsyncedAttempts.size() > 0) {
            mUnsyncedAttemptsCount = mUnsyncedAttempts.size();
            mTotalUnsyncedAttempts = mUnsyncedAttemptsCount;
        }

        if (mSyncingProgress != null && mUnsyncedAttemptsCount != 0) {
            mSyncingProgress.setMax(mUnsyncedAttemptsCount);
        }

        mSyncedAttemptsCount = 0;

        syncAttempts();
    }

    public void syncAttempts() {
        if (mUnsyncedAttempts != null && mUnsyncedAttemptsCount > 0 && mUnsyncedAttempts.size() > 0) {
            Attempt attempt = mUnsyncedAttempts.get(0);
            mUnsyncedAttempts.remove(attempt);
            mUnsyncedAttemptsCount--;

            if (mSyncingMessageView != null) {
                mSyncingMessageView.setText(getString(R.string.syncing_message, ++mSyncedAttemptsCount, mTotalUnsyncedAttempts));
            }

            if (mSyncingProgress != null) {
                mSyncingProgress.setProgress(mSyncedAttemptsCount);
            }

            if (attempt != null) {
                mSyncInProgress = true;
                new SubmitQuestionAttemptTask(mContext, attempt, mStudySessionId, new SubmitQuestionAttemptTask.Callback() {
                    @Override
                    public void onSuccess(Attempt attempt) {
                        mSyncInProgress = false;
                        syncAttempts();
                    }

                    @Override
                    public void onFailure(Attempt attempt) {
                        mSyncInProgress = false;
                        mUnsyncedAttempts.add(attempt);
                        syncAttempts();
                    }
                }).execute();
            }
        } else if (mUnsyncedAttempts != null && mUnsyncedAttempts.size() > 0) {
            syncFailed();
        } else {
            Intent intent = new Intent(mContext, PracticeSessionReportActivity.class);
            intent.putExtra(EXTRA_KEY_SESSION_ID, mStudySessionId);
            startActivity(intent);
            finish();
        }
    }

    private void syncFailed() {
        if (mSyncingCard != null) {
            mSyncingCard.setVisibility(View.GONE);
        }

        if (mSyncFailedCard != null) {
            mSyncFailedCard.setVisibility(View.VISIBLE);
        }

        TextView syncFailedMessageView = (TextView) findViewById(R.id.sync_failed_message);
        if (syncFailedMessageView != null) {
            FontUtils.setFiraSansRegular(syncFailedMessageView);
            if (mUnsyncedAttempts != null) {
                syncFailedMessageView.setText(getString(R.string.sync_failed_message, mUnsyncedAttempts.size()));
            }
        }

        FontUtils.setFiraSansRegular(findViewById(R.id.try_again_button));
    }

    public void onEndSessionClick(View view) {
        final Dialog dialog = new Dialog(mContext);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_continue_question);
        dialog.show();

        mTestExpiredMessage = (TextView) dialog.findViewById(R.id.time_expired_message);
        mContinueOrNotText = (TextView) dialog.findViewById(R.id.continue_or_not_test);
        mQuitButton = (Button) dialog.findViewById(R.id.quit_test_button);
        mContinueButton = (Button) dialog.findViewById(R.id.continue_test_button);

        FontUtils.setFiraSansRegular(mTestExpiredMessage);
        FontUtils.setFiraSansRegular(mContinueOrNotText);
        FontUtils.setFirasansBold(mQuitButton);
        FontUtils.setFirasansBold(mContinueButton);

        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                syncAttemptsAndEndSession(null);
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                initializeExplanation();
                mCountdownTimer = null;
                mContinueTest = true;
                setCurrentSubQuestion(mCurrentQuestion.getSubQuestion());
                startSoloTestTimer();
            }
        });
    }

    @Override
    public void onBackPressed() {
        hideHelp();
    }

    private void initializeExplanation() {
        mExplanationLayout = (RelativeLayout) findViewById(R.id.explanation_layout);
        mExplanationView = (MathView) findViewById(R.id.explanation);
        if (mExplanationView != null) {
            mExplanationView.setEngine(MathView.Engine.MATHJAX);
            mExplanationView.config("MathJax.Hub.Config({\n" +
                    " tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']]}});");
        }

        TextView hideExplanationView = (TextView) findViewById(R.id.hide_explanation);
        if (hideExplanationView != null) {
            hideExplanationView.setPaintFlags(hideExplanationView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            hideExplanationView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideHelp();
                }
            });
        }
    }

    public void showHelp() {
        if (mExplanationLayout != null) {
            mExplanationLayout.startAnimation(mBottomUp);
            mExplanationLayout.setVisibility(View.VISIBLE);
        }
    }

    private void hideHelp() {
        if (mExplanationLayout != null && mExplanationLayout.getVisibility() == View.VISIBLE) {
            mExplanationLayout.startAnimation(mBottomDown);
            mExplanationLayout.setVisibility(View.GONE);
        }
    }

    private void showHelpPages() {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);

        MaterialShowcaseSequence showcaseSequence = new MaterialShowcaseSequence(this, "solo_help");
        showcaseSequence.setConfig(config);

        String gotIt = getString(R.string.help_got_it);

        showcaseSequence.addSequenceItem(
                mSetTimerText,
                getString(R.string.help_time_remaining),
                gotIt);

        showcaseSequence.addSequenceItem(
                mTestQuitButton,
                getString(R.string.help_quit_test),
                gotIt);

        showcaseSequence.addSequenceItem(
                mTimeProgress,
                getString(R.string.help_questin_remaining),
                gotIt);

        showcaseSequence.setOnItemDismissedListener(new MaterialShowcaseSequence.OnSequenceItemDismissedListener() {
            @Override
            public void onDismiss(MaterialShowcaseView materialShowcaseView, int i) {
                if (i == 2) {
                    // closed the last help screen
                    setHelpVisible(false);

                    startSoloTestTimer();
                }
            }
        });

        showcaseSequence.start();

        // hadFired returns false means we're showing the help
        setHelpVisible(!showcaseSequence.hasFired());
    }

    private boolean isHelpVisible() {
        return mHelpVisible;
    }

    private void setHelpVisible(boolean helpStatus) {
        mHelpVisible = helpStatus;
    }
}