package com.cliqueprep.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.asynTask.OtpVerificationTask;
import com.cliqueprep.asynTask.SignUpTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

public class OtpVerificationActivity extends AppCompatActivity {
    private TextView mTimeTextView;
    private ProgressBar mProgressBar;
    private int mOtpTimeCount;
    private EditText mOtpField;
    private Button mNextButton;
    private Context mContext;
    private ImageView mTitleImageView;
    private TextView mOtpPageText;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        mContext = this;

        mTitleImageView = (ImageView) findViewById(R.id.title_image_view);
        mOtpPageText = (TextView) findViewById(R.id.otp_page_text);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        mTimeTextView = (TextView) findViewById(R.id.otp_time_text);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        mOtpTimeCount = 0;
        if (mProgressBar != null) {
            mProgressBar.setProgress(mOtpTimeCount);
        }

        setFonts();
        startTimer();

        mNextButton = (Button) findViewById(R.id.next_button);
        mOtpField = (EditText) findViewById(R.id.otp_field);
        if (mOtpField != null) {
            mOtpField.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (!s.equals("")) {
                        if (mNextButton != null) {
                            mNextButton.setText(getResources().getString(R.string.next));
                        }
                    }
                }
            });
        }
    }

    public void setFonts() {
        FontUtils.setFiraSansMedium(mTitleImageView);
        FontUtils.setFiraSansLight(mOtpPageText);
        FontUtils.setFiraSansRegular(mTimeTextView);
        FontUtils.setFiraSansRegular(mOtpField);
        FontUtils.setFirasansBold(mNextButton);
    }

    public void startTimer() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTimeTextView.setText(String.valueOf(millisUntilFinished / 1000));
                mOtpTimeCount++;
                mProgressBar.setProgress(mOtpTimeCount);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                mTimeTextView.setText("0");
                mNextButton.setText(getResources().getString(R.string.re_generate_otp));
                mNextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User user = User.load(mContext);
                        UserProfileAttributes profile = user.getProfile();
                        if (profile != null) {
                            //same api for resending otp
                            new SignUpTask(mContext, profile.getUserName(), user.getCountryCode(), user.getPhoneNo(), mLoaderProgressBar).execute();
                        }

                        startTimer();
                    }
                });
            }
        }.start();
    }

    public void onNextClicked(View view) {
        String otp = BFUtils.getStringFromEditable(mOtpField);
        if (otp == null || otp.length() < 1) {
            BFUtils.setError(mOtpField, getString(R.string.otp_error));
            return;
        }

        new OtpVerificationTask(mContext, otp, mLoaderProgressBar).execute();
    }
}

