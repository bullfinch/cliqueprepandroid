package com.cliqueprep.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cliqueprep.Model.Course;
import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;

public class PaymentResultActivity extends AppCompatActivity {
    public static final String EXTRA_PAYMENT_STATUS = "extra_payment_status";
    public static final String EXTRA_COURSE = "extra_course";

    public static final String STATUS_SUCCESS = "payment_success";
    public static final String STATUS_FAILURE = "payment_failure";

    private Course mCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String paymentStatus = getIntent().getStringExtra(EXTRA_PAYMENT_STATUS);
        Course mCourse = (Course) getIntent().getSerializableExtra(EXTRA_COURSE);

        // set content view based on status.
        if (STATUS_SUCCESS.equals(paymentStatus)) {
            setContentView(R.layout.activity_payment_success);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.payment_success);
            }

            TextView successMessageView = (TextView) findViewById(R.id.payment_success_message);
            if (successMessageView != null && mCourse != null) {
                successMessageView.setText(getString(R.string.course_subscribed_success_fully, mCourse.getCourseName()));
            }

            FontUtils.setFiraSansRegular(findViewById(R.id.payment_contact_info_message));
            FontUtils.setFiraSansMedium(successMessageView);
            FontUtils.setFiraSansRegular(findViewById(R.id.practice_now_button));

        } else {
            setContentView(R.layout.activity_payment_failure);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(R.string.payment_failed);
            }

            FontUtils.setFiraSansRegular(findViewById(R.id.payment_contact_info_message));
            FontUtils.setFiraSansMedium(findViewById(R.id.payment_failure_message));
            FontUtils.setFiraSansRegular(findViewById(R.id.try_again_button));
        }
    }

    public void onTryAgainClicked(View view) {
        Intent intent = new Intent(this, CourseSelectionActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void onPracticeNowClicked(View view) {
        Intent intent = new Intent(this, PracticeSelectionActivity.class);
        if (mCourse != null) {
            intent.putExtra(PracticeSelectionActivity.EXTRA_COURSE, mCourse);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
