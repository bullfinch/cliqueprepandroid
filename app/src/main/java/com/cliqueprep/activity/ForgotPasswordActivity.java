package com.cliqueprep.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

public class ForgotPasswordActivity extends AppCompatActivity {
    private static String LOG_TAG = "ForgotPassword";

    private EditText mResetPasswordFiled;
    private Button mButton;
    private Context mContext;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        mResetPasswordFiled = (EditText) findViewById(R.id.reset_password_field);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);
        mButton = (Button) findViewById(R.id.forgot_submit_button);

        setFont();
    }

    public void initializeNavigation(View view) {
        finish();
    }

    public void setFont() {
        FontUtils.setFiraSansRegular(findViewById(R.id.reset_page_text));
    }

    public void onResetSubmitClicked(View view) {
        String email = BFUtils.getStringFromEditable(mResetPasswordFiled);
        if (!BFUtils.isValidEmailAddress(email)) {
            BFUtils.setError(mResetPasswordFiled, getString(R.string.email_error));
            return;
        }
        new PasswordResetTask(email).execute();
    }

    private class PasswordResetTask extends AsyncTask<Void, Void, Response> {
        private String mEmail;

        public PasswordResetTask(String email) {
            mEmail = email;
        }

        @Override
        protected void onPreExecute() {

            BFUtils.hideSoftKeyboard(ForgotPasswordActivity.this);

            if (mLoaderProgressBar != null && mContext != null
                    && mContext instanceof Activity
                    && !((Activity) mContext).isFinishing()) {
                mLoaderProgressBar.setVisibility(View.VISIBLE);
            }
        }

        protected Response doInBackground(Void... voids) {
            Gson gson = new Gson();
            HashMap<String, String> params = new HashMap<>();
            params.put("email", mEmail);

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("user", params);

            return new HTTPManager(CliqueprepApplication.RESET_URL).post(gson.toJson(parameters));
        }

        public void onPostExecute(Response response) {
            JSONObject jsonObject;
            if (mLoaderProgressBar != null && mContext != null
                    && mContext instanceof Activity
                    && !((Activity) mContext).isFinishing()) {
                mLoaderProgressBar.setVisibility(View.GONE);
            }

            if (response != null) {
                try {
                    if (response.getStatusCode() == 200) {
                        jsonObject = new JSONObject(response.getResponseBody());
                        String status = jsonObject.getString("status");
                        BFUtils.showToast(mContext, status);
                        mContext.startActivity(new Intent(mContext, SigninActivity.class));

                    } else {
                        String error;

                        if (response.getStatusCode() == 0) {
                            error = mContext.getString(R.string.internet_connection_error);
                        } else {
                            jsonObject = new JSONObject(response.getResponseBody());
                            error = jsonObject.getString("errors");
                        }

                        BFUtils.showToast(mContext, error);
                    }
                } catch (JSONException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
    }
}
