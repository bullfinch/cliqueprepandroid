package com.cliqueprep.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.cliqueprep.Model.SubscriptionDetails;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;

public class PayUPaymentActivity extends Activity {
    private static final String LOG_TAG = PayUPaymentActivity.class.getSimpleName();
    public static String EXTRA_SUBSCRIPTION_DETAILS = "subscription_details";


    private WebView mWebView;
    private StringBuilder mParamsStringBuilder;
    private SubscriptionDetails mSubscriptionDetails;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        setContentView(R.layout.activity_pay_u_payment);


        mWebView = (WebView) findViewById(R.id.payment_web_view);
        if (getIntent() != null) {
            mSubscriptionDetails = (SubscriptionDetails) getIntent().getSerializableExtra(EXTRA_SUBSCRIPTION_DETAILS);
        }

        mWebView.setVisibility(View.VISIBLE);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.clearHistory();
        mWebView.clearCache(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setUseWideViewPort(false);
        mWebView.getSettings().setLoadWithOverviewMode(false);
        mWebView.getSettings().setDisplayZoomControls(false);

        String productInfo = "cliqueprep";
        String serviceProvider = "payu_paisa";
        User user = User.load(mContext);

        if (mSubscriptionDetails != null) {
            Log.i(LOG_TAG, "Transaction Id : " + mSubscriptionDetails.getTransactionId());

            mParamsStringBuilder = new StringBuilder();
            mParamsStringBuilder.append("key=");
            mParamsStringBuilder.append(mSubscriptionDetails.getKey());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("txnid=");
            mParamsStringBuilder.append(mSubscriptionDetails.getTransactionId());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("amount=");
            mParamsStringBuilder.append(mSubscriptionDetails.getAmount());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("productinfo=");
            mParamsStringBuilder.append(productInfo);
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("firstname=");
            mParamsStringBuilder.append(user.getProfile().getUserName());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("phone=");
            mParamsStringBuilder.append(user.getPhoneNo());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("email=");
            mParamsStringBuilder.append(user.getEmail());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("surl=");
            mParamsStringBuilder.append(mSubscriptionDetails.getSuccessUrl());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("furl=");
            mParamsStringBuilder.append(mSubscriptionDetails.getFailureUrl());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("hash=");
            mParamsStringBuilder.append(mSubscriptionDetails.getHash());
            mParamsStringBuilder.append("&");
            mParamsStringBuilder.append("service_provider=");
            mParamsStringBuilder.append(serviceProvider);
        }

        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.i(LOG_TAG, "Redirection URL: " + url);

                if (url.equals(mSubscriptionDetails.getSuccessUrl())) {
                    launchPaymentResultPage(PaymentResultActivity.STATUS_SUCCESS);
                } else if (url.equals(mSubscriptionDetails.getFailureUrl())) {
                    launchPaymentResultPage(PaymentResultActivity.STATUS_FAILURE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                mWebView.loadUrl("javascript:(function() { " +
                        "$('a.logo').css('display', 'none'); })()");
                mWebView.loadUrl("javascript:(function() { " +
                        "$('.cancel_url_p a').text('CliquePrep App'); })()");
                mWebView.loadUrl("javascript:(function() { " +
                        "var i = document.querySelectorAll('.result_div p');" +
                        "i[1].innerHTML= 'You will be redirected to CliquePrep App'; })()");
            }
        });

        if (mSubscriptionDetails != null && mParamsStringBuilder != null) {
            mWebView.postUrl(mSubscriptionDetails.getSubmitUrl(), mParamsStringBuilder.toString().getBytes());
        }

    }

    @Override
    public void onBackPressed() {
        mWebView.destroy();

        launchPaymentResultPage(PaymentResultActivity.STATUS_FAILURE);
    }

    @Override
    public void finish() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        super.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void launchPaymentResultPage(String paymentStatus) {
        Log.i(LOG_TAG, paymentStatus);

        Intent intent = new Intent(mContext, PaymentResultActivity.class);
        intent.putExtra(PaymentResultActivity.EXTRA_PAYMENT_STATUS, paymentStatus);
        intent.putExtra(PaymentResultActivity.EXTRA_COURSE, mSubscriptionDetails.getCourse());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}

