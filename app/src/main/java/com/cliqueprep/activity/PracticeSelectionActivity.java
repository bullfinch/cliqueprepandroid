package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.mocktest.activity.MockTestListActivity;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

public class PracticeSelectionActivity extends CliquePrepBaseActivity {
    public static final String EXTRA_COURSE = "course";
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_selection);

        mContext = this;

        Course course = (Course) getIntent().getSerializableExtra(EXTRA_COURSE);
        if (course != null) {
            // save this one as selected course
            User currentUser = User.load(mContext);
            currentUser.setSelectedCourse(course);
            currentUser.save(mContext);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.practice_selection_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        setFonts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.practice_selection_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            int id = item.getItemId();

            switch (id) {
                case R.id.bell_notification:
                    BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
                    break;
                case R.id.friends_notification:
                    BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
                    break;
                case R.id.open_drawer:
                    openSideMenu();
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSocialPracticeCardViewClicked(View view) {
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
        /*
        Intent intent = new Intent(mContext, FriendRequestsActivity.class);
        startActivity(intent);
        */
    }

    public void onMockTestCardViewClicked(View view) {
        Intent intent = new Intent(mContext, MockTestListActivity.class);
        startActivity(intent);
    }

    public void onQuizCardViewClicked(View view) {
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
    }

    public void setFonts() {
        FontUtils.setFirasansBold(findViewById(R.id.clat_text));
        FontUtils.setFiraSansLight(findViewById(R.id.clat_sub_text));
        FontUtils.setFiraSansMedium(findViewById(R.id.social_practice_text));
        FontUtils.setFiraSansMedium(findViewById(R.id.quiz_practice_text));
    }

    public void onSoloPracticeViewClicked(View view) {
        User user = User.load(mContext);
        Course course = user.getSelectedCourse();

        if (course != null) {
            Intent intent = new Intent(mContext, SoloPracticeActivity.class);
            intent.putExtra(SoloPracticeActivity.EXTRA_COURSE_ID, course.getCourseId());
            startActivity(intent);
        }
    }
}
