package com.cliqueprep.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.mocktest.activity.MockTestQuestionActivity;
import com.cliqueprep.mocktest.activity.RollNumberActivity;
import com.cliqueprep.util.FontUtils;

/**
 * Created by sreejith on 13/10/16.
 */

public class InstructionsActivity extends CliquePrepBaseActivity {
    private TextView mInstructionsTextView;
    MockTest mMockTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        mContext = this;
        mMockTest = (MockTest) getIntent().getSerializableExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST);
        if (mMockTest == null) {
            finish();
        }

        mInstructionsTextView = (TextView) findViewById(R.id.instruction_text);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        if (mInstructionsTextView != null) {
            String mtInstructions = mMockTest.getInstructions();

            // this fix should have been taken care at the backend.
            if (TextUtils.isEmpty(mtInstructions)) {
                mtInstructions = getString(R.string.mock_test_default_instruction);
            }

            mInstructionsTextView.setText(Html.fromHtml(mtInstructions));
        }

        setFonts();
    }

    public void setFonts() {
        FontUtils.setFirasansBold(findViewById(R.id.toolbar_title));
        FontUtils.setFiraSansMedium(findViewById(R.id.instruction_header));
        FontUtils.setFiraSansRegular(mInstructionsTextView);
        FontUtils.setFiraSansMedium(findViewById(R.id.accept_button));
    }

    public void onAcceptClicked(View view) {
        Intent intent = new Intent(mContext, RollNumberActivity.class);
        intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST, mMockTest);
        startActivity(intent);
        finish();
    }
}
