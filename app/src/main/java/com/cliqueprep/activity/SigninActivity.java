package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.asynTask.SignInTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

public class SigninActivity extends AppCompatActivity {
    public EditText mUserNameField;
    public EditText mPasswordField;
    public Button mSignInButton;
    private Context mContext;
    private TextView mResetPasswordText;
    private ProgressBar mLoaderProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        mContext = this;

        mUserNameField = (EditText) findViewById(R.id.username_field);
        mPasswordField = (EditText) findViewById(R.id.password_field);
        mSignInButton = (Button) findViewById(R.id.signin_button);
        mResetPasswordText = (TextView) findViewById(R.id.forgot_password_text);
        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        setFonts();
    }

    public void setFonts() {
        FontUtils.setFiraSansMedium(mResetPasswordText);
        FontUtils.setFiraSansRegular(mUserNameField);
        FontUtils.setFiraSansRegular(mPasswordField);
        FontUtils.setFirasansBold(mSignInButton);
    }

    public void onResetPasswordClicked(View view) {
        mContext.startActivity(new Intent(mContext, ForgotPasswordActivity.class));
    }

    public void initializeNavigation(View view) {
        finish();
    }

    public void onSignInClicked(View view) {
        BFUtils.hideSoftKeyboard(SigninActivity.this);

        String username = BFUtils.getStringFromEditable(mUserNameField);
        if (TextUtils.isEmpty(username)) {
            BFUtils.setError(mUserNameField, getString(R.string.username_error));
            return;
        }
        String password = BFUtils.getStringFromEditable(mPasswordField);
        if (TextUtils.isEmpty(password)) {
            BFUtils.setError(mPasswordField, getString(R.string.password_error));
            return;
        }

        new SignInTask(mContext, username, password, mLoaderProgressBar).execute();
    }
}
