package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cliqueprep.Model.CourePlan;
import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.SubscriptionDetails;
import com.cliqueprep.R;
import com.cliqueprep.asynTask.ApplyCouponsTask;
import com.cliqueprep.asynTask.GetPaymentDigestTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

import java.util.HashMap;

public class PaymentMethodActivity extends AppCompatActivity {
    public static String EXTRA_PAYEE_INFO_MAP = "payee_info_map";

    private HashMap<String, String> mPayeeInfoMap;
    private CourePlan mPlan;
    private Course mCourse;
    private String mTransactionId;
    private double mDiscount;
    private double mDiscountedAmount;
    private String mAppliedCouponCode;

    private RelativeLayout mCouponsContainerLayout;
    private TextView mCouponAppliedMessageView;
    private ProgressBar mProgressBar;
    private Context mContext;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (getIntent() != null) {
            mPayeeInfoMap = (HashMap<String, String>) getIntent().getSerializableExtra(EXTRA_PAYEE_INFO_MAP);
            Bundle bundle = getIntent().getBundleExtra(PlanSelectionActivity.EXTRA_DETAILS);
            if (bundle != null) {
                mTransactionId = bundle.getString(PlanSelectionActivity.EXTRA_TRANSACTION_ID);
                mPlan = (CourePlan) bundle.getSerializable(PlanSelectionActivity.EXTRA_PLAN);
                mCourse = (Course) bundle.getSerializable(PlanSelectionActivity.EXTRA_COURSE);
            }
        }

        mContext = this;
        setFonts();
        initializeViews();
    }

    private void initializeViews() {
        mCouponAppliedMessageView = (TextView) findViewById(R.id.coupon_applied_view);
        mCouponsContainerLayout = (RelativeLayout) findViewById(R.id.coupon_code_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        TextView packageNameView = (TextView) findViewById(R.id.course_package_name);
        TextView packageAmountView = (TextView) findViewById(R.id.payment_price_text);
        CheckBox checkbox = (CheckBox) findViewById(R.id.use_coupon_checkbox);

        if (checkbox != null) {
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mCouponsContainerLayout != null && mCouponAppliedMessageView != null) {
                        if (isChecked) {
                            mCouponsContainerLayout.setVisibility(View.VISIBLE);
                        } else {
                            mCouponsContainerLayout.setVisibility(View.GONE);
                            mCouponAppliedMessageView.setVisibility(View.GONE);
                            mAppliedCouponCode = "";
                        }
                    }
                }
            });
        }
        if (mPlan != null && mCourse != null) {
            if (!TextUtils.isEmpty(mPlan.getName()) && !TextUtils.isEmpty(mCourse.getCourseName())) {
                String packageName = getString(R.string.package_name, mCourse.getCourseName(), mPlan.getName());
                if (packageNameView != null) {
                    packageNameView.setText(packageName);
                }
            }

            if (packageAmountView != null) {
                packageAmountView.setText(getString(R.string.rupee, mPlan.getAmount()));
            }

        }
    }

    public void setFonts() {
        FontUtils.setFiraSansRegular(findViewById(R.id.toolbar_title));
        FontUtils.setFiraSansRegular(findViewById(R.id.step_count));
        FontUtils.setFiraSansRegular(findViewById(R.id.course_package_name));
        FontUtils.setFiraSansRegular(findViewById(R.id.payment_price_text));
        FontUtils.setFiraSansRegular(findViewById(R.id.coupon_text));
        FontUtils.setFirasansItalic(findViewById(R.id.payment_coupon_sub_text));
        FontUtils.setFiraSansRegular(findViewById(R.id.apply_button));
        FontUtils.setFiraSansRegular(findViewById(R.id.coupon_applied_view));
        FontUtils.setFiraSansRegular(findViewById(R.id.coupon_code_field));
    }

    public void OnApplyClicked(View view) {
        EditText couponCodeField = (EditText) findViewById(R.id.coupon_code_field);
        if (couponCodeField != null) {
            final String couponCode = BFUtils.getStringFromEditable(couponCodeField);
            if (TextUtils.isEmpty(couponCode)) {
                BFUtils.setError(couponCodeField, getString(R.string.error_invalid_coupon_code));
                return;
            }

            HashMap<String, String> parameters = new HashMap<>();
            if (mCourse != null) {
                parameters.put("course", String.valueOf(mCourse.getCourseId()));
            }

            if (mPlan != null) {
                parameters.put("plan", String.valueOf(mPlan.getId()));
            }

            parameters.put("coupon_code", couponCode);
            new ApplyCouponsTask(mContext, mProgressBar, parameters, new ApplyCouponsTask.Callback() {
                @Override
                public void onSuccess(double discount, double discountedAmount) {
                    mDiscount = discount;
                    mDiscountedAmount = discountedAmount;
                    mAppliedCouponCode = couponCode;

                    if (mCouponAppliedMessageView != null) {
                        mCouponAppliedMessageView.setText(Html.fromHtml(getString(R.string.coupon_applied_message, couponCode)));
                        mCouponAppliedMessageView.setVisibility(View.VISIBLE);
                        mCouponsContainerLayout.setVisibility(View.GONE);
                    }
                }
            }).execute();
        }
    }

    public void onPayNowClicked(View view) {
        if (mPayeeInfoMap != null) {
            if (!TextUtils.isEmpty(mTransactionId)) {
                mPayeeInfoMap.put("txnid", mTransactionId);
            }

            if (mPlan != null) {
                mPayeeInfoMap.put("plan", String.valueOf(mPlan.getId()));
            }

            if (!TextUtils.isEmpty(mAppliedCouponCode)) {
                mPayeeInfoMap.put("coupon_code", mAppliedCouponCode);
            }

            new GetPaymentDigestTask(mContext, mProgressBar, mPayeeInfoMap, new GetPaymentDigestTask.Callback() {
                @Override
                public void onSuccess(SubscriptionDetails subscriptionDetails) {
                    if (subscriptionDetails != null) {
                        if (!TextUtils.isEmpty(mTransactionId)) {
                            subscriptionDetails.setTransactionId(mTransactionId);
                        }

                        if (mPlan != null) {
                            subscriptionDetails.setPlan(mPlan);
                        }

                        if (mCourse != null) {
                            subscriptionDetails.setCourse(mCourse);
                        }

                        Intent intent = new Intent(mContext, PayUPaymentActivity.class);
                        intent.putExtra(PayUPaymentActivity.EXTRA_SUBSCRIPTION_DETAILS, subscriptionDetails);
                        startActivity(intent);
                    }
                }
            }).execute();
        }
    }
}
