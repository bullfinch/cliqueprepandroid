package com.cliqueprep.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.cliqueprep.R;
import com.cliqueprep.asynTask.SignUpTask;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

public class SignUpActivity extends AppCompatActivity {
    public EditText mNameField;
    public EditText mPhoneCodeField;
    public EditText mPhoneNoField;
    public Button mSignUpButton;
    public Context mContext;
    private ProgressBar mLoadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mContext = this;

        mNameField = (EditText) findViewById(R.id.name_field);
        mPhoneCodeField = (EditText) findViewById(R.id.phone_code_field);
        mPhoneNoField = (EditText) findViewById(R.id.phone_no_field);
        mSignUpButton = (Button) findViewById(R.id.submit_button);
        mLoadingProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        setFonts();
    }

    public void setFonts() {
        FontUtils.setFiraSansMedium(findViewById(R.id.title));
        FontUtils.setFiraSansRegular(findViewById(R.id.signup_page_text));
        FontUtils.setFiraSansRegular(mNameField);
        FontUtils.setFiraSansRegular(mPhoneNoField);
        FontUtils.setFiraSansRegular(mPhoneCodeField);
        FontUtils.setFirasansBold(mSignUpButton);
    }

    public void onNavigationButtonClicked(View view) {
        finish();
    }

    public void onSignUpClicked(View view) {
        BFUtils.hideSoftKeyboard(SignUpActivity.this);

        String name = BFUtils.getStringFromEditable(mNameField);
        if (TextUtils.isEmpty(name) || name.length() < 3) {
            BFUtils.setError(mNameField, getString(R.string.name_error));
            return;
        }

        String phonecode = BFUtils.getStringFromEditable(mPhoneCodeField);
        if (TextUtils.isEmpty(phonecode) || phonecode.length() <= 1) {
            BFUtils.setError(mPhoneCodeField, getString(R.string.phone_code_error));
            return;
        }

        String phoneno = BFUtils.getStringFromEditable(mPhoneNoField);
        if (TextUtils.isEmpty(phoneno) || phoneno.length() != 10) {
            BFUtils.setError(mPhoneNoField, getString(R.string.phone_no_error));
            return;
        }

        new SignUpTask(mContext, name, phonecode, phoneno, mLoadingProgressBar).execute();
    }
}

