package com.cliqueprep.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import com.cliqueprep.Model.Subjects;
import com.cliqueprep.R;
import com.cliqueprep.adapter.SoloExpandableAdapter;
import com.cliqueprep.asynTask.SubjectTask;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;

public class SoloPracticeActivity extends CliquePrepBaseActivity {
    public static final String EXTRA_COURSE_ID = "course_id";
    public static final String EXTRA_TOPIC_IDS = "topic_ids";

    private ExpandableListView mSubjectList;
    private Context mContext;
    private SoloExpandableAdapter mSubjectExpListAdapter;
    private long mCourseId;
    private ProgressBar mLoaderProgressBar;
    private ToggleButton mSelectAllToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solo_practice);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.solo_practice_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitleTextColor(0xFFFFFFFF);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        mSelectAllToggle = (ToggleButton) findViewById(R.id.select_subject_button);
        enableSelectAllToggle();

        mLoaderProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);

        mSubjectList = (ExpandableListView) findViewById(R.id.solo_subject_list);

        if (mSubjectList != null) {
            mSubjectExpListAdapter = new SoloExpandableAdapter(new SoloExpandableAdapter.SelectionChangeListener() {
                @Override
                public void onSelectionChange(boolean isAllSelected) {
                    disableSelectAllToggle();

                    if (mSelectAllToggle != null) {
                        mSelectAllToggle.setChecked(isAllSelected);
                    }

                    enableSelectAllToggle();
                }
            });

            mSubjectList.setAdapter(mSubjectExpListAdapter);
        }

        mCourseId = getIntent().getLongExtra(EXTRA_COURSE_ID, 0);

        new SubjectTask(mContext, mCourseId, mLoaderProgressBar, new SubjectTask.Callback() {
            @Override
            public void onSuccess(Subjects subjects) {
                if (subjects != null && mSubjectExpListAdapter != null) {
                    mSubjectExpListAdapter.setSubjects(subjects.getSubjectGroup());
                }
            }
        }).execute();


        FontUtils.setFiraSansRegular(findViewById(R.id.select_subject_text));
    }

    private void enableSelectAllToggle() {
        if (mSelectAllToggle != null) {
            mSelectAllToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (mSubjectExpListAdapter != null) {
                        if (isChecked) {
                            mSubjectExpListAdapter.selectAllTopics();
                        } else {
                            mSubjectExpListAdapter.deselectAllTopics();
                        }
                    }
                }
            });
        }
    }

    private void disableSelectAllToggle() {
        if (mSelectAllToggle != null) {
            mSelectAllToggle.setOnCheckedChangeListener(null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.solo_practice_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null) {
            int id = item.getItemId();
            switch (id) {
                case R.id.bell_notification:
                    BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
                    break;
                case R.id.friends_notification:
                    BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
                    break;
                case R.id.open_drawer:
                    openSideMenu();
                    break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSoloSubmitClicked(View view) {
        if (mSubjectExpListAdapter.getAllSelectedTopics().size() != 0) {
            Intent intent = new Intent(mContext, SoloPopUpActivity.class);
            intent.putExtra(EXTRA_TOPIC_IDS, mSubjectExpListAdapter.getAllSelectedTopics());
            intent.putExtra(SoloPracticeActivity.EXTRA_COURSE_ID, mCourseId);
            startActivity(intent);
        } else {
            BFUtils.showToast(mContext, getString(R.string.selection_faild));
        }
    }
}