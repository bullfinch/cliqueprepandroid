package com.cliqueprep.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.Model.Courses;
import com.cliqueprep.R;
import com.cliqueprep.adapter.SubscribedRecyclerViewAdapter;

public class SubscribedCourseFragment extends Fragment {
    private static final String EXTRA_COURSE = "course";

    private RecyclerView mSubCourseRecyclerView;
    private SubscribedRecyclerViewAdapter mSubscribedCourseAdapter;
    private Courses mCourse;
    private TextView mNoResultView;

    public SubscribedCourseFragment() {
    }

    public static SubscribedCourseFragment newInstance(Courses course) {
        SubscribedCourseFragment fragment = new SubscribedCourseFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_COURSE, course);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mCourse = (Courses) getArguments().getSerializable(EXTRA_COURSE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_subscribed_list, container, false);
        Context context = rootView.getContext();
        mSubCourseRecyclerView = (RecyclerView) rootView.findViewById(R.id.subscribed_course_recycleriew);
        mNoResultView = (TextView) rootView.findViewById(R.id.subscribed_course_error);
        mSubscribedCourseAdapter = new SubscribedRecyclerViewAdapter(context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        if (mSubCourseRecyclerView != null) {
            mSubCourseRecyclerView.setLayoutManager(linearLayoutManager);
            mSubCourseRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mSubCourseRecyclerView.setAdapter(mSubscribedCourseAdapter);

        }

        if (mCourse != null && mSubscribedCourseAdapter != null) {
            mSubscribedCourseAdapter.addCourse(mCourse.getSubscribedCourse());
        }

        setNoResultViewVisibility();
        return rootView;
    }

    public void setNoResultViewVisibility() {
        if (mSubscribedCourseAdapter != null) {
            int visibility = mSubscribedCourseAdapter.getItemCount() == 0 ? TextView.VISIBLE : TextView.GONE;
            if (mNoResultView != null) {
                mNoResultView.setVisibility(visibility);
            }
        }
    }
}
