package com.cliqueprep.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cliqueprep.R;
import com.cliqueprep.adapter.AllQuestionRecyclerViewAdapter;

/**
 * Created by tony on 6/11/16.
 */

public class ReviewAllQuestionFragment extends Fragment {

    public ReviewAllQuestionFragment() {
    }

    public static ReviewAllQuestionFragment newInstance() {
        ReviewAllQuestionFragment reviewAllFragment = new ReviewAllQuestionFragment();
        return reviewAllFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question_list, container, false);
        if (rootView != null) {
            Context context = rootView.getContext();

            RecyclerView allQuestionRecyclerView = (RecyclerView) rootView.findViewById(R.id.question_list_recycler_view);
            if (allQuestionRecyclerView != null) {
                allQuestionRecyclerView.setHasFixedSize(true);
                allQuestionRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                allQuestionRecyclerView.setAdapter(new AllQuestionRecyclerViewAdapter(context));
            }

            return rootView;
        }
        return null;
    }
}
