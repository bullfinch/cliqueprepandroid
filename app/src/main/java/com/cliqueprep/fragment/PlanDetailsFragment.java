package com.cliqueprep.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cliqueprep.Model.CourePlan;
import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;

public class PlanDetailsFragment extends Fragment {
    private static final String EXTRA_PLAN = "plan";
    private CourePlan mCoursePlan;
    private OnBuyNowClickListener mListener;

    public PlanDetailsFragment() {
        // Required empty public constructor
    }

    public static PlanDetailsFragment newInstance(CourePlan couresPlan) {
        PlanDetailsFragment fragment = new PlanDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_PLAN, couresPlan);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCoursePlan = (CourePlan) getArguments().getSerializable(EXTRA_PLAN);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_plan_details, container, false);

        TextView nameView = (TextView) view.findViewById(R.id.name_field);
        TextView descriptionOneView = (TextView) view.findViewById(R.id.description_one);
        TextView descriptionTwoView = (TextView) view.findViewById(R.id.description_two);
        TextView descriptionThreeView = (TextView) view.findViewById(R.id.description_three);

        if (nameView != null && !TextUtils.isEmpty(mCoursePlan.getName())) {
            nameView.setText(mCoursePlan.getName());
            FontUtils.setFiraSansMedium(nameView);
        }

        if (descriptionOneView != null && !TextUtils.isEmpty(mCoursePlan.getFirstDescription())) {
            descriptionOneView.setText(mCoursePlan.getFirstDescription());
            FontUtils.setFiraSansMedium(descriptionOneView);
        }

        if (descriptionTwoView != null && !TextUtils.isEmpty(mCoursePlan.getSecondDescription())) {
            descriptionTwoView.setText(mCoursePlan.getSecondDescription());
            FontUtils.setFiraSansMedium(descriptionTwoView);
        }

        if (descriptionThreeView != null && !TextUtils.isEmpty(mCoursePlan.getThirdDescription())) {
            descriptionThreeView.setText(mCoursePlan.getThirdDescription());
            FontUtils.setFiraSansMedium(descriptionThreeView);
        }

        Button buyNowButton = (Button) view.findViewById(R.id.buy_button);
        if (buyNowButton != null) {
            FontUtils.setFirasansBold(buyNowButton);
            buyNowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.OnBuyNowClicked(mCoursePlan);
                }
            });

            if (getString(R.string.expired).equals(mCoursePlan.getValidity())) {
                buyNowButton.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.ash_color));
                buyNowButton.setText(R.string.expired);
                buyNowButton.setOnClickListener(null);
            }
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBuyNowClickListener) {
            mListener = (OnBuyNowClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMentionsListItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This listener needs to be implemented by the activity.
     * This will tell when a mention item is being clicked.
     * Based on the mention, we need to launch the corresponding
     * chat room fragment, with the chat portion loaded.
     */
    public interface OnBuyNowClickListener {
        void OnBuyNowClicked(CourePlan plan);
    }
}
