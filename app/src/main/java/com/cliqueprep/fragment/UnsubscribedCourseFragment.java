package com.cliqueprep.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.Model.Courses;
import com.cliqueprep.R;
import com.cliqueprep.adapter.UnSubscribedRecyclerViewAdapter;

public class UnsubscribedCourseFragment extends Fragment {
    private static final String EXTRA_COURSE = "course";

    private RecyclerView mCourseRecyclerView;
    private Courses mCourse;
    private UnSubscribedRecyclerViewAdapter mCourseRecyclerViewAdapter;
    private TextView mNoResultView;

    public UnsubscribedCourseFragment() {
    }

    public static UnsubscribedCourseFragment newInstance(Courses course) {
        UnsubscribedCourseFragment fragment = new UnsubscribedCourseFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_COURSE, course);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mCourse = (Courses) getArguments().getSerializable(EXTRA_COURSE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_course_list, container, false);
        Context context = rootView.getContext();
        mCourseRecyclerView = (RecyclerView) rootView.findViewById(R.id.course_recycleriew);
        mNoResultView = (TextView) rootView.findViewById(R.id.unsubscibed_course_error);
        mCourseRecyclerViewAdapter = new UnSubscribedRecyclerViewAdapter(context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);

        if (mCourseRecyclerView != null) {
            mCourseRecyclerView.setLayoutManager(linearLayoutManager);
            mCourseRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mCourseRecyclerView.setAdapter(mCourseRecyclerViewAdapter);
        }

        if (mCourse != null && mCourseRecyclerViewAdapter != null) {
            mCourseRecyclerViewAdapter.addCourse(mCourse.getUnSubascribedCourse());
        }

        setNoResultViewVisibility();
        return rootView;
    }

    public void setNoResultViewVisibility() {
        if (mCourseRecyclerViewAdapter != null) {
            int visibility = mCourseRecyclerViewAdapter.getItemCount() == 0 ? TextView.VISIBLE : TextView.GONE;
            if (mNoResultView != null) {
                mNoResultView.setVisibility(visibility);
            }
        }
    }
}
