package com.cliqueprep.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cliqueprep.Model.Attempt;
import com.cliqueprep.Model.SubQuestion;
import com.cliqueprep.R;
import com.cliqueprep.activity.QuestionActivity;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.FontUtils;
import com.cliqueprep.view.OptionView;
import com.google.gson.Gson;

import java.util.HashMap;

import io.github.kexanie.library.MathView;

public class SubQuestionFragment extends Fragment {
    private static final String ARG_SUB_QUESTION = "sub_question";

    private SubQuestion mSubQuestion;
    private SubQuestion.Option mSelectedOption;

    private RelativeLayout mActionLayout;
    private LinearLayout mAnswerLayout;
    private TextView mAnswer;
    private Button mSubmitButton;
    private long mStartTime;
    private LinearLayout mProgressLayout;
    private ProgressBar mProgressBar;
    private TextView mProgressMessage;

    HashMap<SubQuestion.Option, OptionView> mOptionViews;
    OptionView mSelectedOptionView;

    BroadcastReceiver mTimeExpiredEventsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long subQuestionId = intent.getLongExtra(CliqueprepApplication.EXTRA_SUBQUESTION_ID, 0);
            if (mSubQuestion != null && mSubQuestion.getId() == subQuestionId) {
                onTimeExpired();
            }
        }
    };

    public SubQuestionFragment() {
        // Required empty public constructor
    }

    public static SubQuestionFragment newInstance(SubQuestion subQuestion) {
        SubQuestionFragment fragment = new SubQuestionFragment();
        Bundle args = new Bundle();
        if (subQuestion != null) {
            args.putSerializable(ARG_SUB_QUESTION, subQuestion);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mOptionViews = new HashMap<>();

        if (getArguments() != null) {
            mSubQuestion = (SubQuestion) getArguments().getSerializable(ARG_SUB_QUESTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_question, container, false);

        if (view != null) {
            mActionLayout = (RelativeLayout) view.findViewById(R.id.action_layout);
            mAnswerLayout = (LinearLayout) view.findViewById(R.id.answer_layout);
            mAnswer = (TextView) view.findViewById(R.id.answer);
            mProgressLayout = (LinearLayout) view.findViewById(R.id.progress_layout);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
            mProgressMessage = (TextView) view.findViewById(R.id.message);

            setFonts();
            initializeQuestion(view);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(mTimeExpiredEventsReceiver,
                new IntentFilter(CliqueprepApplication.ACTION_TIME_EXPIRED(getContext())));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mStartTime = System.currentTimeMillis();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(mTimeExpiredEventsReceiver);
    }

    private void initializeQuestion(View view) {
        if (mSubQuestion != null) {
            showProgressDialog();

            MathView questionView = (MathView) view.findViewById(R.id.question);
            if (questionView != null) {
                questionView.setEngine(MathView.Engine.MATHJAX);
                questionView.config("MathJax.Hub.Config({\n" +
                        " tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']]}});");

                String description = "<div style = 'text-align : center'>" + mSubQuestion.getDescription() + "</div>";
                questionView.setText(description);
                questionView.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onProgressChanged(WebView view, int newProgress) {
                        if (mProgressBar != null) {
                            mProgressBar.setProgress(newProgress);
                        }

                        if (newProgress == 100) {
                            hideProgressDialog();
                            if (getActivity() != null && getActivity() instanceof QuestionActivity) {
                                ((QuestionActivity) getActivity()).setCurrentSubQuestion(mSubQuestion);
                                ((QuestionActivity) getActivity()).startSoloTestTimer();
                            }
                        }
                    }
                });
            }

            LinearLayout optionContainer = (LinearLayout) view.findViewById(R.id.option_container);
            if (optionContainer != null) {
                addOptions(optionContainer);
            }

            mSubmitButton = (Button) view.findViewById(R.id.confirm_button);
            if (mSubmitButton != null) {
                mSubmitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        submitAnswer();
                    }
                });
            }

            Button tryNextButton = (Button) view.findViewById(R.id.try_next_button);
            if (tryNextButton != null) {
                tryNextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        moveToNextQuestion();
                    }
                });
            }

            FontUtils.setFiraSansRegular(tryNextButton);
            FontUtils.setFiraSansRegular(questionView);
        }
    }

    private void addOptions(LinearLayout container) {
        for (final SubQuestion.Option option : mSubQuestion.getOptions()) {
            final OptionView optionView = new OptionView(getActivity());
            optionView.setContent(option.getValue());
            optionView.setCallback(new OptionView.Callback() {
                @Override
                public void helpClicked() {
                    showHelp();
                }

                @Override
                public void onSelected() {
                    clearOptionSelections();
                    mSelectedOption = option;
                    mSelectedOptionView = optionView;
                    showConfirmButton();
                }
            });

            container.addView(optionView);
            mOptionViews.put(option, optionView);
        }
    }

    private void showConfirmButton() {
        mAnswerLayout.setVisibility(View.GONE);
        mSubmitButton.setVisibility(View.VISIBLE);
        mActionLayout.setVisibility(View.VISIBLE);
    }

    public void onTimeExpired() {
        submitAnswer();
    }

    private void submitAnswer() {
        for (OptionView optionView : mOptionViews.values()) {
            optionView.setInactive();
        }

        if (mSelectedOption != null) {
            if (mSelectedOption.isCorrect()) {
                showCorrectAnswer();
            } else {
                showWrongAnswer();
            }

            updateOptionViews();

            Attempt attempt = new Attempt();
            attempt.setTime((System.currentTimeMillis() - mStartTime) / 1000);
            attempt.setShuffledOptions(new Gson().toJson(mSubQuestion.getOptions()));

            HashMap<Integer, Integer> optionsHash = new HashMap<>();
            optionsHash.put((int) mSubQuestion.getId(), (int) mSelectedOption.getId());
            attempt.setOptionsHash(optionsHash);

            ((QuestionActivity) getActivity()).submitAttempt(attempt);
        } else {
            showSkipped();
            updateOptionViews();
        }
    }

    private void showCorrectAnswer() {
        mSubmitButton.setVisibility(View.GONE);

        mAnswerLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.text_button_color));
        mAnswer.setText(getResources().getText(R.string.correct_answer));
        mAnswerLayout.setVisibility(View.VISIBLE);

        mActionLayout.setVisibility(View.VISIBLE);
    }

    private void showWrongAnswer() {
        mSubmitButton.setVisibility(View.GONE);

        mAnswerLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_primary_dark));
        mAnswer.setText(getResources().getText(R.string.wrong_answer));
        mAnswerLayout.setVisibility(View.VISIBLE);

        mActionLayout.setVisibility(View.VISIBLE);
    }

    private void showSkipped() {
        mSubmitButton.setVisibility(View.GONE);

        mAnswerLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_primary));
        mAnswer.setText(getResources().getText(R.string.skip_answer));
        mAnswerLayout.setVisibility(View.VISIBLE);

        mActionLayout.setVisibility(View.VISIBLE);
    }

    private void clearOptionSelections() {
        for (OptionView optionView : mOptionViews.values()) {
            optionView.clearSelection();
        }
    }

    private void updateOptionViews() {
        for (SubQuestion.Option option : mOptionViews.keySet()) {
            OptionView optionView = mOptionViews.get(option);
            optionView.clearSelection();

            if (option.isCorrect()) {
                optionView.setCorrect();
            }

            if (option == mSelectedOption) {
                optionView.setSelection(option.isCorrect());
            }
        }
    }

    private void moveToNextQuestion() {
        ((QuestionActivity) getActivity()).nextQuestion();
    }

    private void showHelp() {
        ((QuestionActivity) getActivity()).showHelp();
    }

    public void setFonts() {
        FontUtils.setFiraSansRegular(mAnswer);
        FontUtils.setFirasansBold(mSubmitButton);
        FontUtils.setFiraSansRegular(mProgressMessage);
    }

    protected void showProgressDialog() {
        if (mProgressLayout != null) {
            mProgressLayout.setVisibility(View.VISIBLE);
        }

        if (mProgressMessage != null) {
            mProgressMessage.setText(R.string.loading_question);
        }

        if (mProgressBar != null) {
            mProgressBar.setMax(100);
        }
    }

    protected void hideProgressDialog() {
        if (mProgressLayout != null) {
            mProgressLayout.setVisibility(View.GONE);
        }
    }
}
