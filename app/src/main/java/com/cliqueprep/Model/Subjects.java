package com.cliqueprep.Model;

import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by noufy on 5/9/16.
 */
public class Subjects {
    private static final String LOG_TAG = "SoloSubjectModel";

    public Subject[] subjects;

    public static Subjects parse(String object) {
        Subjects subjects = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                subjects = gson.fromJson(object, Subjects.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error: " + e.getMessage());
            }
        }
        return subjects;
    }

    public Subject[] getSubjectGroup() {
        return subjects;
    }
}
