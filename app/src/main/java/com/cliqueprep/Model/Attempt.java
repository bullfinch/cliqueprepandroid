package com.cliqueprep.Model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by asif on 26/09/16.
 */
public class Attempt implements Serializable {
    private HashMap<Integer, Integer> optionsHash;
    private long questionId;
    private long time;
    private String shuffledOptions;

    public HashMap<Integer, Integer> getOptionsHash() {
        return optionsHash;
    }

    public String getOptionHashJson() {
        String json = "{";
        if (optionsHash != null) {
            for (Integer key : optionsHash.keySet()) {
                if (json.length() > 3) { json += ","; }
                json += String.valueOf(key) + " => " + String.valueOf(optionsHash.get(key));
            }
        }

        json += "}";

        return json;
    }

    public void setOptionsHash(HashMap<Integer, Integer> optionsHash) {
        this.optionsHash = optionsHash;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getShuffledOptions() {
        return shuffledOptions;
    }

    public void setShuffledOptions(String shuffledOptions) {
        this.shuffledOptions = shuffledOptions;
    }
}
