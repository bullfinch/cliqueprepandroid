package com.cliqueprep.Model;

import java.io.Serializable;

/**
 * Created by asif on 25/09/16.
 */
public class SubQuestion implements Serializable {
    private long id;
    private String description;
    private String explanation;
    private float score;
    private Option[] options;

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getExplanation() {
        return explanation;
    }

    public float getScore() {
        return score;
    }

    public Option[] getOptions() {
        return options;
    }

    public class Option implements Serializable {
        private long id;
        private String value;
        private boolean correct;

        public long getId() {
            return id;
        }

        public String getValue() {
            return value;
        }

        public boolean isCorrect() {
            return correct;
        }
    }
}
