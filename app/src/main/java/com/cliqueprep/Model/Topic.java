package com.cliqueprep.Model;

/**
 * Created by noufy on 4/9/16.
 */
public class Topic {
    private String name;
    private int id;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getTopicName() {
        return name;
    }

    public void setTopicName(String name) {
        this.name = name;
    }

    public int getTopicId() {
        return id;
    }

    public void setTopicId(int id) {
        this.id = id;
    }
}
