package com.cliqueprep.Model;

import java.io.Serializable;

/**
 * Created by asif on 24/09/16.
 */
public class Question implements Serializable {
    private long id;
    private QuestionType question_type;
    private int time;
    private int prescribed_time;
    private int difficulty;
    private String passage;
    private String explanation;
    private SubQuestion[] sub_questions;

    public boolean isPassage() {
        return question_type == QuestionType.Passage;
    }

    public long getId() {
        return id;
    }

    public QuestionType getQuestionType() {
        return question_type;
    }

    public int getTime() {
        return time;
    }

    public int getPrescribedTime() {
        return prescribed_time;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public String getPassage() {
        return passage;
    }

    public String getExplanation() {
        return explanation;
    }

    public SubQuestion[] getSubQuestions() {
        return sub_questions;
    }

    public enum QuestionType {
        Normal,
        Passage
    }

    public boolean hasSubQuestion() {
       return sub_questions != null && sub_questions.length > 0 && sub_questions[0] != null;
    }

    public SubQuestion getSubQuestion() {
        if (hasSubQuestion()) {
            return sub_questions[0];
        }

        return null;
    }
}
