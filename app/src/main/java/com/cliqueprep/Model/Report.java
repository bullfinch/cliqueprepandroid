package com.cliqueprep.Model;

import android.util.Log;

import com.google.gson.Gson;

import java.io.Serializable;
import java.net.URL;

/**
 * Created by noufy on 29/9/16.
 */

public class Report implements Serializable {
    private static String LOG_TAG = "reportModel";

    private String points_earned;
    private String total_attempted_questions;
    private String total_correct_questions;
    private String total_wrong_questions;
    private int accuracy;
    private int average_time;
    private String review_url;

    public static Report parse(String object) {
        Report reportList = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                reportList = gson.fromJson(object, Report.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return reportList;
    }

    public String getPointEarned() {
        return points_earned;
    }

    public void setPointsEarned(String points_earned) {
        this.points_earned = points_earned;
    }

    public String getAttemptedQuestions() {
        return total_attempted_questions;
    }

    public void setAttemptedQuestions(String total_attempted_questions) {
        this.total_attempted_questions = total_attempted_questions;
    }

    public String getCorrectQuestions() {
        return total_correct_questions;
    }

    public void setCorrectQuestions(String total_correct_questions) {
        this.total_correct_questions = total_correct_questions;
    }

    public String getWrongQuestions() {
        return total_wrong_questions;
    }

    public void setWrongQuestions(String points_earned) {
        this.total_wrong_questions = total_wrong_questions;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getAverageTime() {
        return average_time;
    }

    public void setAverageTime(int average_time) {
        this.average_time = average_time;
    }

    public String getReviewUrl() {
        return review_url;
    }

    public void setReviewUrl(String review_url) {
        this.review_url = review_url;
    }
}
