package com.cliqueprep.Model;

/**
 * Created by tony on 31/8/16.
 */
public class UserProfileAttributes {
    private String id;
    private String name;
    private String dob;
    private String location;
    private String gender;
    private String address_line1;
    private String address_line2;
    private String state;
    private String city;
    private String country;

    public String getUserId() {
        return id;
    }

    public void setUserId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return name;
    }

    public void setUserName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dob;
    }

    public void setDateOfBirth(String dob) {
        this.dob = dob;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddressLineOne() {
        return address_line1;
    }

    public void setAddressLineOne(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddressLineTwo() {
        return address_line2;
    }

    public void setAddressLineTwo(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}