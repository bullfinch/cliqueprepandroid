package com.cliqueprep.Model;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.cliqueprep.framework.CliqueprepApplication;
import com.google.gson.Gson;

import java.io.Serializable;

public class User implements Serializable {
    private static final String PREF_KEY_USER = "User";
    private static User instance = null;
    private static final String LOG_TAG = "UserModel";

    private Long id;
    private String country_code;
    private String phone_number;
    private String email;
    private String auth_token;
    private Guardian guardian;
    private UserProfileAttributes profile;
    private Course selected_course;
    private String photo_url;
    private String release = "app";

    public static synchronized User load(Context context) {
        if (instance == null) {
            String userJson = context.getSharedPreferences(CliqueprepApplication.PREF_FILE_CONFIG, Context.MODE_PRIVATE).getString(PREF_KEY_USER, null);
            if (userJson != null) {
                instance = new Gson().fromJson(userJson, User.class);
            }

            if (instance == null) {
                instance = new User();
            }
        }

        return instance;
    }

    public void save(Context context) {
        context.getSharedPreferences(CliqueprepApplication.PREF_FILE_CONFIG, Context.MODE_PRIVATE)
                .edit()
                .putString(PREF_KEY_USER, new Gson().toJson(this))
                .apply();
    }

    public void copyAndSave(Context context, User user) {
        email = user.getEmail();
        auth_token = user.getAuthenticationToken();
        phone_number = user.getPhoneNo();
        country_code = user.getCountryCode();
        id = user.getId();
        guardian = user.getGurdian();
        profile = user.getProfile();
        photo_url = user.getPhoto();

        save(context);
    }

    public static User parse(String object) {
        User userList = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                userList = gson.fromJson(object, User.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error: " + e.getMessage());
            }
        }
        return userList;
    }

    public void clearUser(Context context) {
        email = null;
        auth_token = null;
        id = null;
        guardian = null;
        country_code = null;
        phone_number = null;
        profile = null;
        guardian = null;
        profile = null;
        selected_course = null;
        photo_url = null;

        save(context);
    }

    public boolean isSignedIn() {
        return !TextUtils.isEmpty(auth_token);
    }

    public String getRelease() {
        return release;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void setCountryCode(String country_code) {
        this.country_code = country_code;
    }

    public void setPhoneNo(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPhoneNo() {
        return phone_number;
    }

    public String getAuthenticationToken() {
        return auth_token;
    }

    public void setAuthenticationToken(String authentication_token) {
        this.auth_token = authentication_token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo_url;
    }

    public void setPhoto(String photoUrl) {
        this.photo_url = photoUrl;
    }

    public Guardian getGurdian() {
        return guardian;
    }

    public void setGurdian(Guardian guardian) {
        this.guardian = guardian;
    }

    public UserProfileAttributes getProfile() {
        return profile;
    }

    public void setProfile(UserProfileAttributes profile) {
        this.profile = profile;
    }

    public Course getSelectedCourse() {
        return selected_course;
    }

    public void setSelectedCourse(Course selected_course) {
        this.selected_course = selected_course;
    }

    public long getSelectedCourseId() {
        return selected_course != null ? selected_course.getCourseId() : 0;
    }
}
