package com.cliqueprep.Model;

import android.util.Log;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by meera on 25/9/16.
 */

public class SubscriptionDetails implements Serializable {
    private static final String LOG_TAG = "InitSubRespModel";
    private String txid;
    private String surl;
    private String furl;
    private String curl;
    private Course course;
    private CourePlan plan;
    private int amount;
    private String hash;
    private String key;
    private String submit_url;

    public static SubscriptionDetails parse(String object) {
        SubscriptionDetails subscriptionDetails = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                subscriptionDetails = gson.fromJson(object, SubscriptionDetails.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return subscriptionDetails;
    }

    public String getTransactionId() {
        return txid;
    }

    public void setTransactionId(String txid) {
        this.txid = txid;
    }

    public String getSuccessUrl() {
        return surl;
    }

    public void setSuccessUrl(String surl) {
        this.surl = surl;
    }

    public String getFailureUrl() {
        return furl;
    }

    public void setFailureUrl(String furl) {
        this.furl = furl;
    }

    public String getCurl() {
        return curl;
    }

    public void setCurl(String curl) {
        this.curl = curl;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public CourePlan getPlan() {
        return plan;
    }

    public void setPlan(CourePlan plan) {
        this.plan = plan;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSubmitUrl() {
        return submit_url;
    }

    public void setSubmitUrl(String submit_url) {
        this.submit_url = submit_url;
    }
}
