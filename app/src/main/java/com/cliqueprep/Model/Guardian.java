package com.cliqueprep.Model;

public class Guardian {
    private String id;
    private String name;
    private String email;
    private String relation;
    private String phone_number;
    private String country_code;

    public String getGuardianId() {
        return id;
    }

    public void setGuardianId(String id) {
        this.id = id;
    }

    public String getGuardianName() {
        return name;
    }

    public void setGuardianName(String name) {
        this.name = name;
    }

    public String getGuardianEmail() {
        return email;
    }

    public void setGuardianEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void setCountryCode(String country_code) {
        this.country_code = country_code;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
}