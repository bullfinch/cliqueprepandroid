package com.cliqueprep.Model;

import java.io.Serializable;

/**
 * Created by meera on 24/9/16.
 */

public class CourePlan implements Serializable {
    private long id;
    private String name;
    private long amount;
    private String desc1;
    private String desc2;
    private String desc3;
    private String validity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getFirstDescription() {
        return desc1;
    }

    public void setFirstDescription(String desc1) {
        this.desc1 = desc1;
    }

    public String getSecondDescription() {
        return desc2;
    }

    public void setSecondDescription(String desc2) {
        this.desc2 = desc2;
    }

    public String getThirdDescription() {
        return desc3;
    }

    public void setThirdDescription(String desc3) {
        this.desc3 = desc3;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
