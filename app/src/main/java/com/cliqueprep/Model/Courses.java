package com.cliqueprep.Model;

import android.util.Log;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by noufy on 29/8/16.
 */
public class Courses implements Serializable {
    private static String LOG_TAG = "coursesModel";
    private Course[] not_subscribed;
    private Course[] subscribed;

    public static Courses parse(String object) {
        Courses courseList = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                courseList = gson.fromJson(object, Courses.class);
            } catch (Exception e) {
                Log.e(LOG_TAG,e.getMessage());
            }
        }
        return courseList;
    }

    public Course[] getUnSubascribedCourse() {
        return not_subscribed;
    }

    public Course[] getSubscribedCourse() {
        return subscribed;
    }
}
