package com.cliqueprep.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noufy on 4/9/16.
 */
public class Subject {
    private String name;
    private ArrayList<Topic> topics;
    private String image;

    private boolean isAllTopicSelected;

    public boolean isAllTopicSelected() {
        return isAllTopicSelected;
    }

    public void setAllTopicSelected(boolean allTopicSelected) {
        isAllTopicSelected = allTopicSelected;
    }

    public String getSubjectName() {
        return name;
    }

    public void setSubjectName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return image;
    }

    public void setImageUrl(String image) {
        this.image = image;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<Topic> topics) {
        this.topics = topics;
    }

    public void markAll(boolean isSelected) {
        if (topics != null) {
            for (Topic topic : topics) {
                if (topic != null) {
                    topic.setSelected(isSelected);
                }
            }
        }

        setAllTopicSelected(isSelected);
    }

    public List<Integer> getSelectedTopicIds() {
        List<Integer> idList = new ArrayList<>();

        if (topics != null) {
            for (Topic topic : topics) {
                if (topic != null && topic.isSelected()) {
                    idList.add(topic.getTopicId());
                }
            }
        }

        return idList;
    }

    public int getSelectedTopicCount() {
        int count = 0;

        if (topics != null) {
            for (Topic topic : topics) {
                if (topic != null && topic.isSelected()) {
                    count++;
                }
            }
        }

        return count;
    }
}

