package com.cliqueprep.Model;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by meera on 24/9/16.
 */

public class CourseDetails {
    private static final String LOG_TAG = "CourseDetailsModel";
    private String name;
    private ArrayList<CourePlan> plans;

    public static CourseDetails parse(String object) {
        CourseDetails courseDetails = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                courseDetails = gson.fromJson(object, CourseDetails.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return courseDetails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CourePlan> getPlans() {
        return plans;
    }

    public void setPlans(ArrayList<CourePlan> plans) {
        this.plans = plans;
    }
}
