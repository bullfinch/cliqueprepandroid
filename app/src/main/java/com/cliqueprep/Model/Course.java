package com.cliqueprep.Model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by noufy on 29/8/16.
 */
public class Course implements Serializable {
    private long course_id;
    private String course;
    private String plan;
    private String icon_url;
    private Date validity;
    private Date exam_date;

    public long getCourseId() {
        return course_id;
    }

    public void setCourseId(long id) {
        this.course_id = id;
    }

    public String getCourseName() {
        return course;
    }

    public String getCoursePlan() {
        return plan;
    }

    public void setCoursePlan(String plan) {
        this.plan = plan;
    }

    public void setCourseName(String course) {
        this.course = course;
    }

    public void setValidity(Date validity) {
        this.validity = validity;
    }

    public Date getValidity() {
        return validity;
    }

    public Date getExamDate() {
        return exam_date;
    }

    public void setExamDate(Date exam_date) {
        this.exam_date = exam_date;
    }

    public String getCourseUrl() {
        return icon_url;
    }

    public void setCourseUrl(String mViewUrl) {
        this.icon_url = mViewUrl;
    }
}
