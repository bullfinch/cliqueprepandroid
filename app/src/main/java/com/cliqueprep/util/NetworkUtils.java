package com.cliqueprep.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author Rahul Raveendran V P
 *         Created on 09/12/16 @ 4:04 PM
 *         https://github.com/rahulrvp
 */

public class NetworkUtils {

    public static boolean isConnectedToNetwork(Context context) {
        boolean isConnected = false;

        if (context != null) {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }

        return isConnected;
    }
}
