package com.cliqueprep.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cliqueprep.Model.User;
import com.cliqueprep.activity.IntroSliderActivity;

/**
 * Created by noufy on 24/8/16.
 */
public class BFUtils {

    public static String getStringFromEditable(EditText editText) {
        String value = null;
        if (editText != null) {
            Editable editable = editText.getText();
            if (editable != null) {
                value = editable.toString();
                if (value.equals("")) {
                    value = null;
                }
            }
        }

        return value;
    }

    public static boolean isValidEmailAddress(String emailID) {
        return emailID != null && android.util.Patterns.EMAIL_ADDRESS.matcher(emailID).matches();
    }

    public static void setError(EditText editText, String errorMessage) {
        if (editText != null) {
            editText.setError(errorMessage);
        }
    }

    public static void setErrorTextView(TextView textView, String errorMessage) {
        if (textView != null) {
            textView.setError(errorMessage);
        }
    }

    public static void logoutUser(Context context) {
        User user = User.load(context);
        user.clearUser(context);
        Intent intent = new Intent(context, IntroSliderActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    public static void showToast(Context context, String message) {
        if (!TextUtils.isEmpty(message)) {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);

        View view = activity.getCurrentFocus();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

