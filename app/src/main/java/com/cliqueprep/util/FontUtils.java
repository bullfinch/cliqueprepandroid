package com.cliqueprep.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

/**
 * Created by anand on 29/6/16.
 */
public class FontUtils {
    private static final String FONT_AMMONIOSO_TRIAL = "font/Armonioso-trial.otf";
    private static final String FONT_FIRASANS_BOLD = "font/FiraSans-Bold.otf";
    private static final String FONT_FIRASANS_BOLD_ITALIC = "font/FiraSans-BoldItalic.otf";
    private static final String FONT_FIRASANS_ITALIC = "font/FiraSans-Italic.otf";
    private static final String FONT_FIRASANS_LIGHT = "font/FiraSans-Light.otf";
    private static final String FONT_FIRASANS_LIGHT_ITALIC = "font/FiraSans-LightItalic.otf";
    private static final String FONT_FIRASANS_MEDIUM = "font/FiraSans-Medium.otf";
    private static final String FONT_FIRASANS_MEDIUM_ITALIC = "font/FiraSans-MediumItalic.otf";
    private static final String FONT_FIRASANS_REGULAR = "font/FiraSans-Regular.otf";

    // Font cache variables
    private static Typeface sArmonioso;
    private static Typeface sFirasansBold;
    private static Typeface sFirasansBoldItalic;
    private static Typeface sFirasansItalic;
    private static Typeface sFirasansLight;
    private static Typeface sFirasansLightItalic;
    private static Typeface sFirasansMedium;
    private static Typeface sFirasansMediumItalic;
    private static Typeface sFirasansRegular;

    public static void setFirasansBold(View view) {
        if (view != null) {
            if (sFirasansBold == null) {
                sFirasansBold = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_BOLD);
            }

            setTypeface(view, sFirasansBold);
        }
    }

    public static void setArmonioso(View view) {
        if (view != null) {
            if (sArmonioso == null) {
                sArmonioso = getTypefaceFromAsset(view.getContext(), FONT_AMMONIOSO_TRIAL);
            }

            setTypeface(view, sArmonioso);
        }
    }

    public static void setFiraSansBoldItalic(View view) {
        if (view != null) {
            if (sFirasansBoldItalic == null) {
                sFirasansBoldItalic = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_BOLD_ITALIC);
            }

            setTypeface(view, sFirasansBoldItalic);
        }
    }

    public static void setFirasansItalic(View view) {
        if (view != null) {
            if (sFirasansItalic == null) {
                sFirasansItalic = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_ITALIC);
            }

            setTypeface(view, sFirasansItalic);
        }
    }

    public static void setFiraSansLight(View view) {
        if (view != null) {
            if (sFirasansLight == null) {
                sFirasansLight = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_LIGHT);
            }

            setTypeface(view, sFirasansLight);
        }
    }

    public static void setFirasansLightItalic(View view) {
        if (view != null) {
            if (sFirasansLightItalic == null) {
                sFirasansLightItalic = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_LIGHT_ITALIC);
            }

            setTypeface(view, sFirasansLightItalic);
        }
    }

    public static void setFiraSansMedium(View view) {
        if (view != null) {
            if (sFirasansMedium == null) {
                sFirasansMedium = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_MEDIUM);
            }

            setTypeface(view, sFirasansMedium);
        }
    }

    public static void setFiraSansMediumItalic(View view) {
        if (view != null) {
            if (sFirasansMediumItalic == null) {
                sFirasansMediumItalic = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_MEDIUM_ITALIC);
            }

            setTypeface(view, sFirasansMediumItalic);
        }
    }

    public static void setFiraSansRegular(View view) {
        if (view != null) {
            if (sFirasansRegular == null) {
                sFirasansRegular = getTypefaceFromAsset(view.getContext(), FONT_FIRASANS_REGULAR);
            }

            setTypeface(view, sFirasansRegular);
        }
    }

    public static void setTypeface(View view, Typeface typeface) {
        if (view != null && view instanceof TextView && typeface != null) {
            ((TextView) view).setTypeface(typeface);
        }
    }

    public static Typeface getTypefaceFromAsset(Context context, String filename) {
        Typeface typeface = null;
        if (context != null && !TextUtils.isEmpty(filename)) {
            typeface = Typeface.createFromAsset(context.getAssets(), filename);
        }

        return typeface;
    }
}
