package com.cliqueprep.framework;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;

import com.cliqueprep.R;
import com.cliqueprep.activity.CourseSelectionActivity;
import com.cliqueprep.activity.ProfileDetailsActivity;
import com.cliqueprep.activity.QuestionListActivity;
import com.cliqueprep.activity.SideMenuActivity;
import com.cliqueprep.util.BFUtils;

/**
 * Created by tony on 28/9/16.
 */

public class CliquePrepBaseActivity extends SideMenuActivity {
    protected Context mContext;
    private DrawerLayout mSideDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void hidingNavigationBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void closeSideMenu() {
        if (mSideDrawer != null) {
            mSideDrawer.closeDrawer(Gravity.RIGHT);
        }
    }

    public void onForwardButtonClicked(View view) {
        closeSideMenu();
    }

    public void openSideMenu() {
        mSideDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mSideDrawer != null) {
            mSideDrawer.openDrawer(Gravity.RIGHT);
        }
    }

    public void onProfileViewClicked(View view) {
        startActivity(new Intent(mContext, ProfileDetailsActivity.class));
        closeSideMenu();
    }

    public void changeCourseCardViewClicked(View view) {
        startActivity(new Intent(mContext, CourseSelectionActivity.class));
        closeSideMenu();
    }

    public void onProgressCardViewClicked(View view) {
        startActivity(new Intent(mContext, QuestionListActivity.class));
        closeSideMenu();
    }

    public void onNotificationsCardViewClicked(View view) {
        //drawer options are to be completed later
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
    }

    public void onInviteFriendsCardViewClicked(View view) {
        //drawer options are to be completed later
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
    }

    public void onContactsCardViewClicked(View view) {
        //drawer options are to be completed later
        BFUtils.showToast(mContext, getString(R.string.coming_soon_string));
    }

    public void onLogoutCardViewClicked(View view) {
        BFUtils.logoutUser(mContext);
        closeSideMenu();
    }
}
