package com.cliqueprep.framework;

import android.app.Application;
import android.content.Context;

import com.cliqueprep.BuildConfig;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by noufy on 24/8/16.
 */
public class CliqueprepApplication extends Application {
    public static final String PREF_FILE_CONFIG = "Config";
    public static final String EXTRA_SUBQUESTION_ID = "subquestionId";

    public static final String BASE_URL = BuildConfig.API_SERVER;

    public static final String SIGNIN_URL = BASE_URL + "/users/sign_in.json";
    public static final String SIGNUP_URL = BASE_URL + "/sign_up_logs/send_tpin.json";
    public static final String OTPVERIFICATION_URL = BASE_URL + "/sign_up_logs/tpin_verification.json";
    public static final String SIGNUPDETAILED_URL = BASE_URL + "/users.json";
    public static final String SIGN_UP_WITH_FB = BASE_URL + "/users/fb_auth_android.json";
    public static final String RESET_URL = BASE_URL + "/users/password.json";
    public static final String COURSE_URL = BASE_URL + "/courses.json";
    public static final String SUBJECT_URL = BASE_URL + "/study_sessions/new.json";
    public static final String STUDY_SESSION_URL = BASE_URL + "/study_sessions.json";
    public static final String GET_COURSE_PLAN_URL = BASE_URL + "/courses/get_course_plans.json";

    public static final String SUBMIT_QUESTION_ATTEMPT_URL(long sessionId) {
        return BASE_URL + "/study_sessions/" + sessionId + "/attempts.json";
    }

    public static final String GET_MOCK_TEST_LIST_URL(long courseId) {
        return BASE_URL + "/courses/" + courseId + "/mock_tests.json";
    }

    public static final String MOCK_TEST_ATTEMPT_URL = BASE_URL + "/mock_test_attempts.json";

    public static final String MOCK_TEST_RESUME_URL(long mockTestId) {
        return BASE_URL + "/mock_tests/" + mockTestId + "/resume.json";
    }

    public static final String SAVE_MOCK_TEST_ATTEMPT_URL(long attemptId) {
        return BASE_URL + "/mock_test_attempts/" + attemptId + ".json";
    }

    public static final String APPLY_COUPON_URL = BASE_URL + "/payments/amount_after_coupon_code";
    public static final String GET_PAYMENT_DIGEST_URL = BASE_URL + "/payments/digest.json";
    public static final String PRACTICE_REPORT_URL = BASE_URL + "/study_sessions/get_report.json";

    public static final String NEXT_QUESTION_BATCH_URL(long id) {
        return BASE_URL + "/study_sessions/" + id + "/get_next_batch.json";
    }

    public static String INITIALIZE_SUBSCRIPTION_URL(long user_id) {
        return BASE_URL + "/users/" + user_id + "/payments/new.json";
    }

    public static String FREE_TRIAL_SUBSCRIPTION_URL(long user_id) {
        return BASE_URL + "/users/" + user_id + "/free_trial_subscription.json";
    }

    public static String ACTION_TIME_EXPIRED(Context context) {
        return context != null ? context.getPackageName() + ".TIME_EXPIRED" : null;
    }

    public static String MOCK_TEST_REVIEW_URL(String test_attempt_id) {
        return BASE_URL + "/mock_test_attempts/" + test_attempt_id + "/review.json";
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Fresco.initialize(this);
    }

    public static String PROFILE_URL(long id) {
        return BASE_URL + "/users/" + id + ".json";
    }
}