package com.cliqueprep.asynTask;

import android.content.Context;
import android.os.AsyncTask;

import com.cliqueprep.Model.Attempt;
import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.google.gson.Gson;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by asif on 25/8/16.
 */
public class SubmitQuestionAttemptTask extends AsyncTask<Void, Void, Boolean> {
    private static String LOG_TAG = "study session create";

    private Context mContext;
    private Callback mCallback;

    private Attempt mAttempt;
    private long mSessionId;

    public SubmitQuestionAttemptTask(Context context, Attempt attempt, long sessionId, Callback callback) {
        mContext = context;
        mCallback = callback;
        mAttempt = attempt;
        mSessionId = sessionId;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());
        params.put("course_id", 1);
        params.put("question_id", mAttempt.getQuestionId());
        params.put("time", mAttempt.getTime());
        params.put("shuffled_options", mAttempt.getShuffledOptions());
        params.put("sub_question_option_hash", mAttempt.getOptionHashJson());

        Response response = new HTTPManager(CliqueprepApplication.SUBMIT_QUESTION_ATTEMPT_URL(mSessionId)).post(new Gson().toJson(params));

        return response.getStatusCode() == 200;
    }

    public void onPostExecute(Boolean success) {
        if (mCallback != null) {
            if (success) {
                mCallback.onSuccess(mAttempt);
            } else {
                mCallback.onFailure(mAttempt);
            }
        }
    }

    public interface Callback {
        void onSuccess(Attempt attempt);

        void onFailure(Attempt attempt);
    }
}