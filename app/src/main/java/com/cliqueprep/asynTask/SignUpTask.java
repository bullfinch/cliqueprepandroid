package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.activity.OtpVerificationActivity;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 24/8/16.
 */
public class SignUpTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "signUpTask";

    private String mName;
    private String mPhoneCode;
    private String mPhoneNo;
    public Context mContext;
    private String mRelease = "app";
    private ProgressBar mProgressBar;

    public SignUpTask(Context context, String name, String phonecode, String phoneno, ProgressBar progressBar) {
        mName = name;
        mPhoneNo = phoneno;
        mPhoneCode = phonecode;
        this.mContext = context;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        HashMap<String, String> params = new HashMap<>();
        params.put("phone_number", mPhoneNo);
        params.put("release", mRelease);

        return new HTTPManager(CliqueprepApplication.SIGNUP_URL).post(gson.toJson(params));
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            if (response.getStatusCode() == 200) {
                User user = User.load(mContext);
                UserProfileAttributes profile = new UserProfileAttributes();
                profile.setUserName(mName);
                user.setCountryCode(mPhoneCode);
                user.setPhoneNo(mPhoneNo);
                user.setProfile(profile);
                user.save(mContext);

                mContext.startActivity(new Intent(mContext, OtpVerificationActivity.class));
                ((Activity) mContext).finish();
            } else {
                try {
                    String error = null;
                    JSONObject jsonObject;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token null
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                } catch (JSONException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
    }
}