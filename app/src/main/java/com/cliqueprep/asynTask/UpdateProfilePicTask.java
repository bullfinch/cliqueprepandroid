package com.cliqueprep.asynTask;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Part;
import ch.bullfin.httpmanager.Response;

/**
 * Created by tony on 26/10/16.
 */

public class UpdateProfilePicTask extends AsyncTask<Void, Void, Response> {
    private static final String LOG_TAG = "UpdateProfilePicTask";
    private String mEmail;
    public Context mContext;
    private UpdateProfilePicTask.Callback mCallback;

    public interface Callback {
        void onSuccessfulUpdate();

        void onTaskStart();

        void onUpdateFailure(String errorString);
    }

    public UpdateProfilePicTask(Context context, String email,
                                UpdateProfilePicTask.Callback callback) {
        mEmail = email;
        mContext = context;
        mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onTaskStart();
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User userLoad = User.load(mContext);
        UserProfileAttributes userProfileAttributes = userLoad.getProfile();

        File imageFile = getImageLocation();
        if (imageFile != null) {
            // extract mime from file url
            String mimeType = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(imageFile.getPath());
            if (extension != null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }

            if (TextUtils.isEmpty(mimeType)) {
                mimeType = "image/*";
            }

            ArrayList<Part> params = new ArrayList<>();

            params.add(new Part("email", userLoad.getEmail()));
            params.add(new Part("auth_token", userLoad.getAuthenticationToken()));
            params.add(new Part("user[user_profile_attributes][id]", userProfileAttributes.getUserId()));
            params.add(new Part("user[user_profile_attributes][photo]", imageFile, mimeType));

            return new HTTPManager(CliqueprepApplication.PROFILE_URL(userLoad.getId())).putMultiPart(params);
        }

        return null;
    }

    public void onPostExecute(Response response) {
        JSONObject jsonObject;

        if (response != null) {
            try {
                if (response.getStatusCode() == 200) {
                    jsonObject = new JSONObject(response.getResponseBody());

                    if (jsonObject.has("user")) {
                        User user = User.parse(jsonObject.getString("user"));
                        User currentUser = User.load(mContext);
                        currentUser.copyAndSave(mContext, user);

                        if (mCallback != null) {
                            mCallback.onSuccessfulUpdate();
                        }
                    }
                } else {
                    String error = null;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token expired
                        error = mContext.getString(R.string.session_expired);
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    if (mCallback != null) {
                        mCallback.onUpdateFailure(error);
                    }
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        } else {
            if (mCallback != null && mContext != null) {
                mCallback.onUpdateFailure(mContext.getString(R.string.no_image_found));
            }
        }
    }

    private File getImageLocation() {
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Cliqueprep");
        if (!directory.exists()) {
            directory.mkdirs();
        }

        if (directory.exists() && directory.isDirectory() && directory != null) {
            String path = directory.getPath() + File.separator + "PROFILE_PIC" + ".jpg";
            return new File(path);
        }

        return null;
    }
}
