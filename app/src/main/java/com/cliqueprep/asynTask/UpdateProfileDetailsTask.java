package com.cliqueprep.asynTask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

public class UpdateProfileDetailsTask extends AsyncTask<Void, Void, Response> {
    private static final String LOG_TAG = "UpdateProfileDetailTask";
    private String mEmail;
    private HashMap mUserAttributes;
    private HashMap mGuardianAttributes;
    public Context mContext;
    private Callback mCallback;

    public interface Callback {
        void onSuccessfulUpdate();

        void onTaskStart();

        void onUpdateFailure(String errorString);
    }

    public UpdateProfileDetailsTask(Context context, HashMap userProfileAttributes,
                                    String email, HashMap guardianAttributes, Callback callback) {
        mEmail = email;
        mGuardianAttributes = guardianAttributes;
        mUserAttributes = userProfileAttributes;
        mContext = context;
        mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onTaskStart();
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        User userObject = new User();
        User userLoad = User.load(mContext);

        HashMap<String, Object> user = new HashMap<>();
        Map<String, Object> params = new HashMap<>();

        user.put("id", userObject.getId());
        user.put("user_profile_attributes", mUserAttributes);
        user.put("guardian_attributes", mGuardianAttributes);

        params.put("email", userLoad.getEmail());
        params.put("auth_token", userLoad.getAuthenticationToken());
        params.put("user", user);

        return new HTTPManager(CliqueprepApplication.PROFILE_URL(userLoad.getId())).put(gson.toJson(params));
    }

    public void onPostExecute(Response response) {
        JSONObject jsonObject;

        if (response != null) {
            String error = null;
            try {
                if (response.getStatusCode() == 200) {
                    jsonObject = new JSONObject(response.getResponseBody());
                    if (jsonObject.has("user")) {
                        User user = User.parse(jsonObject.getString("user"));
                        User currentUser = User.load(mContext);
                        currentUser.copyAndSave(mContext, user);

                        if (mCallback != null) {
                            mCallback.onSuccessfulUpdate();
                        }
                    } else {
                        if (mCallback != null) {
                            mCallback.onUpdateFailure(mContext.getString
                                    (R.string.error_loading_user_data));
                        }
                    }
                } else {
                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token expired
                        error = mContext.getString(R.string.session_expired);
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    if (mCallback != null) {
                        mCallback.onUpdateFailure(error);
                    }
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}