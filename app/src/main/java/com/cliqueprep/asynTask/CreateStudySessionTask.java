package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Question;
import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by asif on 25/8/16.
 */
public class CreateStudySessionTask extends AsyncTask<Void, Void, Question[]> {
    private static String LOG_TAG = "study session create";

    private Context mContext;
    private ProgressBar mProgressBar;
    private Callback mCallback;

    private int mButtonNo;
    private int mDuration;
    private ArrayList<Integer> mTopicIds;
    private long mCourseId;
    private long mSessionId;

    public CreateStudySessionTask(Context context, long courseId, int buttonNo, int duration, ArrayList<Integer> topicIds, ProgressBar progressBar, Callback callback) {
        mContext = context;
        mProgressBar = progressBar;
        mCallback = callback;

        mButtonNo = buttonNo;
        mDuration = duration;
        mTopicIds = topicIds;
        mCourseId = courseId;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Question[] doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());

        HashMap<String, Object> studySessionParams = new HashMap<>();
        studySessionParams.put("button_no", String.valueOf(mButtonNo));
        studySessionParams.put("duration", mDuration);
        studySessionParams.put("topic_ids", mTopicIds);
        studySessionParams.put("course_id", mCourseId);

        params.put("study_session", studySessionParams);

        Response response = new HTTPManager(CliqueprepApplication.STUDY_SESSION_URL).post(new Gson().toJson(params));

        if (response.getStatusCode() == 200) {
            String json = response.getResponseBody();
            try {
                JSONObject jsonObject = new JSONObject(json);
                mSessionId = jsonObject.getJSONObject("study_session").getLong("id");

                String questionsJson = jsonObject.getString("questions");

                return new Gson().fromJson(questionsJson, Question[].class);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    public void onPostExecute(Question[] questions) {
        if (mCallback != null) {
            if (questions != null) {
                mCallback.onSuccess(mSessionId, questions);
            } else {
                mCallback.onFailure();
            }
        }
    }

    public interface Callback {
        void onSuccess(long sessionId, Question[] questions);

        void onFailure();
    }
}