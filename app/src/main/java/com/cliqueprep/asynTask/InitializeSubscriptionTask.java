package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 27/8/16.
 */
public class InitializeSubscriptionTask extends AsyncTask<Void, Void, Response> {
    private static final String LOG_TAG = "InitializeSubscription";
    private Context mContext;
    private Callback mCallback;
    private long mPlanId;
    private ProgressBar mProgressBar;

    public interface Callback {
        void onSuccess(String txid);
    }

    public InitializeSubscriptionTask(Context context, ProgressBar progressBar, long planId, Callback callback) {
        mContext = context;
        mCallback = callback;
        mPlanId = planId;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());
        params.put("plan_id", String.valueOf(mPlanId));

        return new HTTPManager(CliqueprepApplication.INITIALIZE_SUBSCRIPTION_URL(User.load(mContext).getId())).get(params);
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            try {
                if (response.getStatusCode() == 200) {
                    JSONObject jsonObject = new JSONObject(response.getResponseBody());
                    if (mCallback != null) {
                        String txid = jsonObject.getString("txid");
                        mCallback.onSuccess(txid);
                    }
                } else {
                    String error;
                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token expired
                        error = mContext.getString(R.string.session_expired);
                        BFUtils.logoutUser(mContext);
                    } else {
                        error = new JSONObject(response.getResponseBody()).getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}