package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Question;
import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by asif on 25/8/16.
 */
public class GetNextQuestionSetTask extends AsyncTask<Void, Void, Question[]> {
    private static String LOG_TAG = "study session create";

    private Context mContext;
    private Callback mCallback;

    private long mStudySessionId;

    public GetNextQuestionSetTask(Context context, long studySessionId, Callback callback) {
        mContext = context;
        mCallback = callback;
        mStudySessionId = studySessionId;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Question[] doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());

        Response response = new HTTPManager(CliqueprepApplication.NEXT_QUESTION_BATCH_URL(mStudySessionId)).get(params);

        if (response.getStatusCode() == 200) {
            String json = response.getResponseBody();
            try {
                JSONObject jsonObject = new JSONObject(json);
                String questionsJson = jsonObject.getString("questions");

                return new Gson().fromJson(questionsJson, Question[].class);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    public void onPostExecute(Question[] questions) {
        if (mCallback != null) {
            if (questions != null) {
                mCallback.onSuccess(questions);
            } else {
                mCallback.onFailure();
            }
        }
    }

    public interface Callback {
        void onSuccess(Question[] questions);
        void onFailure();
    }
}