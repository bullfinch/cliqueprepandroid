package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Courses;
import com.cliqueprep.Model.Report;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 29/9/16.
 */
public class PracticeReportTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "PracticeReportTask";

    private Context mContext;
    private Callback mCallback;
    private ProgressBar mProgressBar;
    private Report report;
    private long mSessionId;

    public interface Callback {
        void onSuccess(Report report);
    }

    public PracticeReportTask(Context context, long sessionid, ProgressBar progressBar, Callback callback) {
        mContext = context;
        mCallback = callback;
        mSessionId = sessionid;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());
        params.put("id", String.valueOf(mSessionId));

        return new HTTPManager(CliqueprepApplication.PRACTICE_REPORT_URL).get(params);
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        try {
            JSONObject jsonObject = new JSONObject(response.getResponseBody());

            if (response.getStatusCode() == 200) {
                report = Report.parse(response.getResponseBody());
                if (mCallback != null) {
                    mCallback.onSuccess(report);
                }
            } else {
                String error;

                if (response.getStatusCode() == 0) {
                    error = mContext.getString(R.string.internet_connection_error);
                } else if (response.getStatusCode() == 401) {
                    //authentication token expired
                    error = mContext.getString(R.string.session_expired);
                    BFUtils.logoutUser(mContext);
                } else {
                    error = jsonObject.getString("errors");
                }

                BFUtils.showToast(mContext, error);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
}