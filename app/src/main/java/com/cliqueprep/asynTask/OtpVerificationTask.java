package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.R;
import com.cliqueprep.activity.SignUpDetailsActivity;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.Model.User;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 24/8/16.
 */
public class OtpVerificationTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "otpVerificationTask";

    private String mOtpField;
    private Context mContext;
    private ProgressBar mProgressBar;

    public OtpVerificationTask(Context mContext, String otp, ProgressBar progressBar) {
        this.mContext = mContext;
        mOtpField = otp;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        User user = User.load(mContext);
        Map<String, String> params = new HashMap<>();
        params.put("release", user.getRelease());
        params.put("phone_number", user.getPhoneNo());
        params.put("tpin", mOtpField);

        return new HTTPManager(CliqueprepApplication.OTPVERIFICATION_URL).post(gson.toJson(params));
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            if (response.getStatusCode() == 200) {
                mContext.startActivity(new Intent(mContext, SignUpDetailsActivity.class));
                ((Activity) mContext).finish();
            } else {
                try {
                    String error = null;
                    JSONObject jsonObject;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token null
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                } catch (JSONException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
    }
}