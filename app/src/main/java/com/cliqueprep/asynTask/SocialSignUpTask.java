package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.activity.CourseSelectionActivity;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 1/9/16.
 */
public class SocialSignUpTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "socialSignUpTask";

    private String mAuthenticationToken;
    private String mFacebookUrl;
    private Gson gson;
    private Context mContext;
    private String mPhone;
    private ProgressBar mProgressBar;

    public SocialSignUpTask(Context context, String phoneNo, String token, String signUpWithFb, ProgressBar progressBar) {
        mAuthenticationToken = token;
        mFacebookUrl = signUpWithFb;
        mContext = context;
        mPhone = phoneNo;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        gson = new Gson();
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("token", mAuthenticationToken);
        params.put("phone_number", user.getPhoneNo());

        return new HTTPManager(mFacebookUrl).post(gson.toJson(params));
    }

    protected void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            JSONObject jsonObject;
            try {
                if (response.getStatusCode() == 200) {
                    jsonObject = new JSONObject(response.getResponseBody());
                    User user = User.parse(jsonObject.getString("user"));
                    User currentUser = User.load(mContext);
                    currentUser.copyAndSave(mContext, user);

                    Intent intent = new Intent(mContext, CourseSelectionActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                } else {
                    String error = null;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token null
                        BFUtils.logoutUser(mContext);
                    } else if (response.getStatusCode() == 400) {
                        error = mContext.getString(R.string.social_sign_up_error);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}