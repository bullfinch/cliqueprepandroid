package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cliqueprep.Model.CourseDetails;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 27/8/16.
 */
public class ApplyCouponsTask extends AsyncTask<Void, Void, Response> {
    private static final String LOG_TAG = "GetCoursePlanTask";
    private Context mContext;
    private Callback mCallback;
    private HashMap<String, String> mParameters;
    private ProgressBar mProgressBar;

    public interface Callback {
        void onSuccess(double discount, double discountedAmount);
    }

    public ApplyCouponsTask(Context context, ProgressBar progressBar, HashMap<String, String> parameters, Callback callback) {
        mContext = context;
        mCallback = callback;
        mParameters = parameters;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        return new HTTPManager(CliqueprepApplication.APPLY_COUPON_URL).get(mParameters);
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            try {
                if (response.getStatusCode() == 200) {
                    JSONObject jsonObject = new JSONObject(response.getResponseBody());
                    double discount = jsonObject.getDouble("discount");
                    double discountedAmount = jsonObject.getDouble("discounted_amount");

                    if (mCallback != null) {
                        mCallback.onSuccess(discount, discountedAmount);
                    }
                } else {
                    String error = null;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token expired
                        error = mContext.getString(R.string.session_expired);
                        BFUtils.logoutUser(mContext);
                    } else if (response.getStatusCode() == 404) {
                        error = mContext.getString(R.string.error_invaid_coupon);
                    } else {
                        JSONObject jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}
