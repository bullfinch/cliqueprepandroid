package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.SubscriptionDetails;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by meera on 28/9/16.
 */
public class GetPaymentDigestTask extends AsyncTask<Void, Void, Response> {
    private static final String LOG_TAG = "GetCoursePlanTask";
    private Context mContext;
    private Callback mCallback;
    private HashMap<String, String> mParameters;
    private ProgressBar mProgressBar;

    public interface Callback {
        void onSuccess(SubscriptionDetails subscriptionDetails);
    }

    public GetPaymentDigestTask(Context context, ProgressBar progressBar, HashMap<String, String> parameters, Callback callback) {
        mContext = context;
        mCallback = callback;
        mParameters = parameters;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        User user = User.load(mContext);
        mParameters.put("email", user.getEmail());
        mParameters.put("auth_token", user.getAuthenticationToken());

        return new HTTPManager(CliqueprepApplication.GET_PAYMENT_DIGEST_URL).post(gson.toJson(mParameters));
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            try {
                if (response.getStatusCode() == 200) {
                    if (mCallback != null) {
                        SubscriptionDetails subscriptionDetails = SubscriptionDetails.parse(response.getResponseBody());
                        mCallback.onSuccess(subscriptionDetails);
                    }
                } else {
                    String error;
                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token expired
                        error = mContext.getString(R.string.session_expired);
                        BFUtils.logoutUser(mContext);
                    } else {
                        error = new JSONObject(response.getResponseBody()).getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}