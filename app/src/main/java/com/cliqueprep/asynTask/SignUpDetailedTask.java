package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Guardian;
import com.cliqueprep.Model.User;
import com.cliqueprep.Model.UserProfileAttributes;
import com.cliqueprep.R;
import com.cliqueprep.activity.CourseSelectionActivity;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 24/8/16.
 */
public class SignUpDetailedTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "signUpDetailedTask";

    private String mEmail;
    private String mPassword;
    private String mConfirmPassword;
    private Context mContext;
    private ProgressBar mProgressBar;

    public SignUpDetailedTask(Context context, String email, String password, String confirmpassword, ProgressBar progressBar) {
        mEmail = email;
        mPassword = password;
        mConfirmPassword = confirmpassword;
        this.mContext = context;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        User user = User.load(mContext);
        UserProfileAttributes profile = user.getProfile();
        Guardian guardian = user.getGurdian();

        Map<String, Object> parameters = new HashMap<>();
        if (profile != null || guardian != null) {
            Map<String, String> attributes = new HashMap<>();
            attributes.put("name", profile.getUserName());

            Map<String, Object> params = new HashMap<>();
            params.put("release", user.getRelease());
            params.put("email", mEmail);
            params.put("password", mPassword);
            params.put("password_confirmation", mConfirmPassword);
            params.put("country_code", user.getCountryCode());
            params.put("phone_number", user.getPhoneNo());
            params.put("user_profile_attributes", attributes);

            parameters.put("user", params);
        }
        return new HTTPManager(CliqueprepApplication.SIGNUPDETAILED_URL).post(gson.toJson(parameters));
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            JSONObject jsonObject;
            try {
                if (response.getStatusCode() == 200) {
                    jsonObject = new JSONObject(response.getResponseBody());
                    User user = User.parse(jsonObject.getString("user"));
                    User currentUser = User.load(mContext);
                    currentUser.copyAndSave(mContext, user);

                    Intent intent = new Intent(mContext, CourseSelectionActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                } else {
                    String error = null;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token null
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}