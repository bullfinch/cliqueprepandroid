package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cliqueprep.Model.Subjects;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.Model.User;
import com.cliqueprep.util.BFUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 5/9/16.
 */
public class SubjectTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "subjectTask";

    private Context mContext;
    private Subjects mSubjects;
    private Callback mCallback;
    private long mCourseId;
    private ProgressBar mProgressBar;

    public interface Callback {
        void onSuccess(Subjects mSubjects);
    }

    public SubjectTask(Context context, long courseid, ProgressBar progressBar, Callback callback) {
        mContext = context;
        mCallback = callback;
        mCourseId = courseid;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());
        params.put("course_id", String.valueOf(mCourseId));

        return new HTTPManager(CliqueprepApplication.SUBJECT_URL).get(params);
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            if (response.getStatusCode() == 200) {
                mSubjects = Subjects.parse(response.getResponseBody());
                if (mCallback != null) {
                    mCallback.onSuccess(mSubjects);
                }
            } else {
                String error = null;
                JSONObject jsonObject;

                if (response.getStatusCode() == 0) {
                    error = mContext.getString(R.string.internet_connection_error);
                } else if (response.getStatusCode() == 401) {
                    //authentication token null
                    BFUtils.logoutUser(mContext);
                } else {
                    try {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    } catch (JSONException e) {
                        Log.e(LOG_TAG, e.getMessage());
                    }
                }

                BFUtils.showToast(mContext, error);
            }
        }
    }
}