package com.cliqueprep.asynTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.activity.CourseSelectionActivity;
import com.cliqueprep.activity.ProfileDetailsActivity;
import com.cliqueprep.framework.CliqueprepApplication;

import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by noufy on 22/8/16.
 */
public class SignInTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "signInTask";

    private String mUsername;
    private String mPassword;
    private Context mContext;
    private ProgressBar mProgressBar;

    public SignInTask(Context context, String username, String password, ProgressBar progressBar) {
        mUsername = username;
        mPassword = password;
        mContext = context;
        mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        Gson gson = new Gson();
        HashMap<String, String> params = new HashMap<>();
        params.put("login", mUsername);
        params.put("password", mPassword);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("user", params);

        return new HTTPManager(CliqueprepApplication.SIGNIN_URL).post(gson.toJson(parameters));
    }

    public void onPostExecute(Response response) {
        JSONObject jsonObject;
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        if (response != null) {
            try {
                if (response.getStatusCode() == 200) {
                    jsonObject = new JSONObject(response.getResponseBody());
                    User user = User.parse(jsonObject.getString("user"));
                    User currentUser = User.load(mContext);
                    currentUser.copyAndSave(mContext, user);

                    Intent intent = new Intent(mContext, CourseSelectionActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mContext.startActivity(intent);
                } else {
                    String error = null;

                    if (response.getStatusCode() == 0) {
                        error = mContext.getString(R.string.internet_connection_error);
                    } else if (response.getStatusCode() == 401) {
                        //authentication token null
                        BFUtils.logoutUser(mContext);
                    } else {
                        jsonObject = new JSONObject(response.getResponseBody());
                        error = jsonObject.getString("errors");
                    }

                    BFUtils.showToast(mContext, error);
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
    }
}