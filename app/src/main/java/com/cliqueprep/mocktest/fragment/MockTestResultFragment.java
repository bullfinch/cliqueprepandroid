package com.cliqueprep.mocktest.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cliqueprep.R;
import com.cliqueprep.mocktest.adapter.MockTestResultRecyclerAdapter;
import com.cliqueprep.mocktest.model.SubjectWiseSummary;

/**
 * Created by sreejith on 20/10/16.
 */

public class MockTestResultFragment extends Fragment {
    private static final String KEY_SUMMARY = "summary";

    public static MockTestResultFragment newInstance(SubjectWiseSummary subjectSummary) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_SUMMARY, subjectSummary);

        MockTestResultFragment fragment = new MockTestResultFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_mock_test_result, container, false);

        RecyclerView resultRecyclerView = (RecyclerView) rootView.findViewById(R.id.mock_test_result_recycleriew);
        MockTestResultRecyclerAdapter mockTestResultAdapter = new MockTestResultRecyclerAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

        if (resultRecyclerView != null) {
            resultRecyclerView.setAdapter(mockTestResultAdapter);
            resultRecyclerView.setLayoutManager(linearLayoutManager);
            resultRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        Bundle bundle = getArguments();
        SubjectWiseSummary subjectSummary = (SubjectWiseSummary) bundle.getSerializable(KEY_SUMMARY);
        mockTestResultAdapter.setData(subjectSummary);

        return rootView;
    }
}
