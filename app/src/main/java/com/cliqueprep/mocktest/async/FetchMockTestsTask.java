package com.cliqueprep.mocktest.async;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.Report;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.util.BFUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by sreejithk on 19/10/16.
 */
public class FetchMockTestsTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = FetchMockTestsTask.class.getSimpleName();

    private Context mContext;
    private Callback mCallback;
    private ProgressBar mProgressBar;
    private Report report;
    private long mCourseId;

    public interface Callback {
        void onSuccess(MockTest[] mockTests);
    }

    public FetchMockTestsTask(Context context, long courseId, ProgressBar progressBar, Callback callback) {
        mContext = context;
        mCallback = callback;
        mCourseId = courseId;
        mProgressBar = progressBar;
    }

    protected void onPreExecute() {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());

        return new HTTPManager(CliqueprepApplication.GET_MOCK_TEST_LIST_URL(mCourseId)).get(params);
    }

    public void onPostExecute(Response response) {
        if (mProgressBar != null && mContext != null
                && mContext instanceof Activity
                && !((Activity) mContext).isFinishing()) {
            mProgressBar.setVisibility(View.GONE);
        }

        try {
            JSONObject jsonObject = new JSONObject(response.getResponseBody());

            if (response.getStatusCode() == 200) {
                MockTest[] mockTests = new Gson().fromJson(jsonObject.getString("mock_tests"), MockTest[].class);
                if (mCallback != null) {
                    mCallback.onSuccess(mockTests);
                }
            } else {
                String error;

                if (response.getStatusCode() == 0) {
                    error = mContext.getString(R.string.internet_connection_error);
                } else if (response.getStatusCode() == 401) {
                    //authentication token expired
                    error = mContext.getString(R.string.session_expired);
                    BFUtils.logoutUser(mContext);
                } else {
                    error = jsonObject.getString("errors");
                }

                BFUtils.showToast(mContext, error);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
}