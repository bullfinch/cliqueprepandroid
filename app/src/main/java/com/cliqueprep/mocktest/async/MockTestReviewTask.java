package com.cliqueprep.mocktest.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.mocktest.model.MockTestReview;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by tony on 10/1/17.
 */

public class MockTestReviewTask extends AsyncTask<Void, Void, Response> {
    private static String LOG_TAG = "MockTestReviewTask";
    private Context mContext;
    private long mAttemptId;
    private Callback mCallback;

    public interface Callback {
        void onSuccess(MockTestReview mockTest);

        void onFailure();

        void onTaskStart();
    }

    public MockTestReviewTask(Context context, long attemptId, Callback callback) {
        mContext = context;
        mAttemptId = attemptId;
        mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (mCallback != null) {
            mCallback.onTaskStart();
        }
    }

    @Override
    protected Response doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("email", user.getEmail());
        parameters.put("auth_token", user.getAuthenticationToken());

        return new HTTPManager(CliqueprepApplication.MOCK_TEST_REVIEW_URL("13806")).get(parameters);
        //TODO:id is hardcoded because previous activity was not created
        //TODO: it would be the next PR
    }

    @Override
    protected void onPostExecute(Response response) {
        try {
            if (response.getStatusCode() == 200) {

                MockTestReview mockTest = MockTestReview.parse(response.getResponseBody());
                if (mCallback != null) {
                    mCallback.onSuccess(mockTest);
                }
            } else {
                if (mCallback != null) {
                    mCallback.onFailure();
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }
}