package com.cliqueprep.mocktest.async;

import android.content.Context;
import android.os.AsyncTask;

import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.mocktest.model.MTQuestion;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.mocktest.model.MockTestAttempt;
import com.google.gson.Gson;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by asif on 25/8/16.
 */
public class SaveAnswersTask extends AsyncTask<Void, Void, Boolean> {
    private static String LOG_TAG = "start mock test";

    private Context mContext;
    private Callback mCallback;
    private boolean mIsFinalSubmit;

    private MockTestAttempt mMockTestAttempt;

    public SaveAnswersTask(Context context, MockTestAttempt mockTestAttempt, boolean isFinalSubmit, Callback callback) {
        mContext = context;
        mCallback = callback;
        mIsFinalSubmit = isFinalSubmit;
        mMockTestAttempt = mockTestAttempt;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        User user = User.load(mContext);
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", user.getEmail());
        params.put("auth_token", user.getAuthenticationToken());

        HashMap<String, Object> mockTestParams = new HashMap<>();

        HashMap<String, Object> subjects = new HashMap<>();

        for (String subject : mMockTestAttempt.getSubjects()) {
            HashMap<String, Object> responses = new HashMap<>();
            for (MTQuestion question : mMockTestAttempt.getSubject(subject).getQuestions()) {
                HashMap<String, Object> questionReeponse = new HashMap<>();
                questionReeponse.put("answer", question.getAnswer());
                questionReeponse.put("time_spent", question.getTimeSpent());
                questionReeponse.put("visited", question.isVisited());
                questionReeponse.put("marked", question.isMarked());

                responses.put(String.valueOf(question.getId()), questionReeponse);
            }
            subjects.put(subject, responses);
        }
        mockTestParams.put("answers", subjects);

        if (mIsFinalSubmit) {
            mockTestParams.put("submit", true);
        }

        params.put("mock_test_attempt", mockTestParams);

        Response response = new HTTPManager(CliqueprepApplication.SAVE_MOCK_TEST_ATTEMPT_URL(mMockTestAttempt.getId())).put(new Gson().toJson(params));

        return response.getStatusCode() == 200;
    }

    public void onPostExecute(Boolean success) {
        if (mCallback != null) {
            if (success) {
                mCallback.onSuccess();
            } else {
                mCallback.onFailure();
            }
        }
    }

    public interface Callback {
        void onSuccess();
        void onFailure();
    }
}