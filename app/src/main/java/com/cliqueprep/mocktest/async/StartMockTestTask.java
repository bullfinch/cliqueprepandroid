package com.cliqueprep.mocktest.async;

import android.content.Context;
import android.os.AsyncTask;

import com.cliqueprep.Model.User;
import com.cliqueprep.framework.CliqueprepApplication;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.mocktest.model.MockTestAttempt;
import com.google.gson.Gson;

import java.util.HashMap;

import ch.bullfin.httpmanager.HTTPManager;
import ch.bullfin.httpmanager.Response;

/**
 * Created by asif on 25/8/16.
 */
public class StartMockTestTask extends AsyncTask<Void, Void, MockTestAttempt> {
    private static String LOG_TAG = "start mock test";

    private Context mContext;
    private Callback mCallback;

    private String mRollNo;
    private MockTest mMockTest;
    private boolean mResuming;

    public StartMockTestTask(Context context, boolean isResuming, String rollNo, MockTest mockTest, Callback callback) {
        mContext = context;
        mCallback = callback;
        mRollNo = rollNo;
        mMockTest = mockTest;
        mResuming = isResuming;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected MockTestAttempt doInBackground(Void... voids) {
        User user = User.load(mContext);

        Response response;

        if (mResuming) {
            HashMap<String, String> params = new HashMap<>();
            params.put("email", user.getEmail());
            params.put("auth_token", user.getAuthenticationToken());

            response = new HTTPManager(CliqueprepApplication.MOCK_TEST_RESUME_URL(mMockTest.getId())).get(params);
        } else {
            HashMap<String, Object> params = new HashMap<>();
            params.put("email", user.getEmail());
            params.put("auth_token", user.getAuthenticationToken());

            HashMap<String, Object> mockTestParams = new HashMap<>();
            mockTestParams.put("roll_number", mRollNo);
            mockTestParams.put("mock_test_id", mMockTest.getId());

            params.put("mock_test_attempt", mockTestParams);

            response = new HTTPManager(CliqueprepApplication.MOCK_TEST_ATTEMPT_URL).post(new Gson().toJson(params));
        }

        if (response.getStatusCode() == 200 || response.getStatusCode() == 403) {
            return new Gson().fromJson(response.getResponseBody(), MockTestAttempt.class);
        }

        return null;
    }

    public void onPostExecute(MockTestAttempt mockTestAttempt) {
        if (mCallback != null) {
            if (mockTestAttempt != null) {
                mCallback.onSuccess(mockTestAttempt);
            } else {
                mCallback.onFailure();
            }
        }
    }

    public interface Callback {
        void onSuccess(MockTestAttempt attempt);
        void onFailure();
    }
}