package com.cliqueprep.mocktest;

import com.cliqueprep.mocktest.model.MockTestAttempt;

/**
 * This class is created as a work around for an issue found in mock test summary page.
 * The intent was not able to take huge json of mock test attempt. Hence we are storing it
 * in this singleton class and clearing it as soon as the submit is success.
 * <p>
 * Yea, I know it isn't a proper fix. But had no choice at this point of time :)
 *
 * @author Rahul Raveendran V P
 *         Created on 2/2/17 @ 7:47 PM
 *         https://github.com/rahulrvp
 */

public class MockTestAttemptCache {
    private static MockTestAttemptCache instance;
    private MockTestAttempt attempt;

    public static MockTestAttemptCache getInstance() {
        if (instance == null) {
            instance = new MockTestAttemptCache();
        }

        return instance;
    }

    public MockTestAttempt getAttempt() {
        return this.attempt;
    }

    public void setAttempt(MockTestAttempt attempt) {
        this.attempt = attempt;
    }

    public void clear() {
        this.attempt = null;
    }
}
