package com.cliqueprep.mocktest.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * Created by sreejith on 19/10/16.
 */

public class MockTest implements Serializable {

    private long id;
    private String title;
    private long release_on;
    private boolean is_free;
    private boolean visibility;
    private boolean has_mock_test_in_progress;
    private boolean has_attempted_mock_test;
    private boolean is_open;
    private boolean will_open_soon;
    private boolean is_a_paid_user;
    private int test_time_min;
    private String instructions;
    private long attempted_on;
    private long seconds_remaining;

    public long getId() {
        return id;
    }

    public int getTestTimeMin() {
        return test_time_min;
    }

    public boolean isPaidUser() {
        return is_a_paid_user;
    }

    public boolean isWillOpenSoon() {
        return will_open_soon;
    }

    public boolean isOpen() {
        return is_open;
    }

    public boolean hasAttemptedMockTest() {
        return has_attempted_mock_test;
    }

    public boolean isInProgress() {
        return has_mock_test_in_progress;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public boolean isFree() {
        return is_free;
    }

    public String getReleaseOn() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");
        return formatter.format(release_on * 1000);
    }

    public String getTitle() {
        return title;
    }

    public String getInstructions() {
        return instructions == null ? "" : instructions;
    }

    public long getRemainingTimeInMillis() {
        return seconds_remaining * 1000;
    }

}
