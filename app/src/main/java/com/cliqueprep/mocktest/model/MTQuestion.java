package com.cliqueprep.mocktest.model;

import java.util.ArrayList;

/**
 * Created by asif on 22/11/16.
 */
public class MTQuestion {
    private long id;
    private String type;
    private String instructions;
    private String question_text;
    private ArrayList<String> options;
    private boolean visited;
    private String answered;
    private boolean marked;
    private int time_spent;

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getInstructions() {
        return instructions;
    }

    public String getQuestion_text() {
        return question_text;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public boolean isVisited() {
        return visited;
    }

    public boolean isAnswered() {
        return answered != null;
    }

    public String getAnswer() {
        return answered;
    }

    public boolean isAnswered(String option) {
        return answered != null && answered.equals(option);
    }

    public boolean isMarked() {
        return marked;
    }

    public int getTimeSpent() {
        return time_spent;
    }

    public void setVisited() {
        this.visited = true;
    }

    public void setAnswered(String option) {
        this.answered = option;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public void incrementTimeSpent() {
        time_spent++;
    }
}
