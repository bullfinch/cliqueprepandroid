package com.cliqueprep.mocktest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by asif on 22/11/16.
 */
public class MTSubject extends HashMap<String, MTQuestion> {
    public MTQuestion get(int key) {
        return super.get(String.valueOf(key));
    }

    public Collection<MTQuestion> getQuestions() {
        return values();
    }
}
