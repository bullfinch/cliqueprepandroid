package com.cliqueprep.mocktest.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by asif on 22/11/16.
 */
public class MockTestAttempt implements Serializable {
    private long id;
    private MockTest mock_test;
    private HashMap<String, MTSubject> questions;
    private ArrayList<String> subjects;

    public int getQuestionCount(String subject) {
        return questions.get(subject).size();
    }

    public long getId() {
        return id;
    }

    public MockTest getMock_test() {
        return mock_test;
    }

    public MTSubject getSubject(String subject) {
        return questions.get(subject);
    }

    public MTQuestion getQuestion(String subject, int number) {
        return questions.get(subject).get(number);
    }

    public ArrayList<String> getSubjects() {
        subjects = new ArrayList<>();

        if (questions != null) {
            Set<String> subjectNames = questions.keySet();
            for (String subjectName : subjectNames) {
                MTSubject mtSubject = questions.get(subjectName);
                if (mtSubject != null && mtSubject.size() > 0) {
                    subjects.add(subjectName);
                }
            }
        }

        return subjects;
    }

}
