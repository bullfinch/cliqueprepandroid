package com.cliqueprep.mocktest.model;

import java.util.HashMap;

/**
 * This class holds the subject wise summary of a mock test.
 * The question wise summary is saved against a question type name.
 *
 * @author Rahul Raveendran V P
 *         Created on 8/12/16 @ 11:53 AM
 *         https://github.com/rahulrvp
 */

public class SubjectWiseSummary extends HashMap<String, QuestionTypeWiseSummary> {

    void addNotVisited(String questionType, long id) {
        QuestionTypeWiseSummary questionTypeWiseSummary = get(questionType);
        if (questionTypeWiseSummary == null) {
            questionTypeWiseSummary = new QuestionTypeWiseSummary();
        }

        questionTypeWiseSummary.addNotVisited(id);

        put(questionType, questionTypeWiseSummary);
    }

    void addNotAnswered(String questionType, long id) {
        QuestionTypeWiseSummary questionTypeWiseSummary = get(questionType);
        if (questionTypeWiseSummary == null) {
            questionTypeWiseSummary = new QuestionTypeWiseSummary();
        }

        questionTypeWiseSummary.addNotAnswered(id);

        put(questionType, questionTypeWiseSummary);
    }

    void addAnswered(String questionType, long id) {
        QuestionTypeWiseSummary questionTypeWiseSummary = get(questionType);
        if (questionTypeWiseSummary == null) {
            questionTypeWiseSummary = new QuestionTypeWiseSummary();
        }

        questionTypeWiseSummary.addAnswered(id);

        put(questionType, questionTypeWiseSummary);
    }

    void addMarked(String questionType, long id) {
        QuestionTypeWiseSummary questionTypeWiseSummary = get(questionType);
        if (questionTypeWiseSummary == null) {
            questionTypeWiseSummary = new QuestionTypeWiseSummary();
        }

        questionTypeWiseSummary.addMarked(id);

        put(questionType, questionTypeWiseSummary);
    }
}
