package com.cliqueprep.mocktest.model;

import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;

/**
 * Created by tony on 11/1/17.
 */

public class MockTestReview {
    private static String LOG_TAG = "mockTestReviewModel";
    private MockTestAttempt mock_test_attempt;
    private HashMap<String, String> answers;

    public static MockTestReview parse(String object) {
        MockTestReview mockTest = null;
        if (object != null) {
            Gson gson = new Gson();
            try {
                mockTest = gson.fromJson(object, MockTestReview.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return mockTest;
    }

    public MockTestAttempt getMockTestAttempt() {
        return mock_test_attempt;
    }

    public HashMap getAnswers() {
        return answers;
    }
}
