package com.cliqueprep.mocktest.model;

import java.util.HashMap;

/**
 * The overall summary of a mock test.
 * This class holds subject wise summary of a mock test
 * against a subject name.
 *
 * @author Rahul Raveendran V P
 *         Created on 8/12/16 @ 12:22 PM
 *         https://github.com/rahulrvp
 */

public class MockTestSummary extends HashMap<String, SubjectWiseSummary> {

    public void addNotVisited(String subject, String questionType, long id) {
        SubjectWiseSummary subjectSummary = get(subject);
        if (subjectSummary == null) {
            subjectSummary = new SubjectWiseSummary();
        }

        subjectSummary.addNotVisited(questionType, id);

        put(subject, subjectSummary);
    }

    public void addNotAnswered(String subject, String questionType, long id) {
        SubjectWiseSummary subjectSummary = get(subject);
        if (subjectSummary == null) {
            subjectSummary = new SubjectWiseSummary();
        }

        subjectSummary.addNotAnswered(questionType, id);

        put(subject, subjectSummary);
    }

    public void addAnswered(String subject, String questionType, long id) {
        SubjectWiseSummary subjectSummary = get(subject);
        if (subjectSummary == null) {
            subjectSummary = new SubjectWiseSummary();
        }

        subjectSummary.addAnswered(questionType, id);

        put(subject, subjectSummary);
    }

    public void addMarked(String subject, String questionType, long id) {
        SubjectWiseSummary subjectSummary = get(subject);
        if (subjectSummary == null) {
            subjectSummary = new SubjectWiseSummary();
        }

        subjectSummary.addMarked(questionType, id);

        put(subject, subjectSummary);
    }
}
