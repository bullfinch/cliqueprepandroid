package com.cliqueprep.mocktest.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class holds subject wise summary of the mock test.
 * We are tracking the questions using ids in 4 categories.
 * We are using 4 different array lists for this purpose.
 * When a new id is being added to a category, we will remove
 * the same id from all other categories (if present).
 *
 * @author Rahul Raveendran V P
 *         Created on 8/12/16 @ 4:51 PM
 *         https://github.com/rahulrvp
 */

public class QuestionTypeWiseSummary implements Serializable {
    private ArrayList<Long> mNotVisited;
    private ArrayList<Long> mNotAnswered;
    private ArrayList<Long> mAnswered;
    private ArrayList<Long> mMarked;

    void addNotVisited(long id) {
        removeFormAll(id);

        if (mNotVisited == null) {
            mNotVisited = new ArrayList<>();
        }

        mNotVisited.add(id);
    }

    void addNotAnswered(long id) {
        removeFormAll(id);

        if (mNotAnswered == null) {
            mNotAnswered = new ArrayList<>();
        }

        mNotAnswered.add(id);
    }

    void addAnswered(long id) {
        removeFormAll(id);

        if (mAnswered == null) {
            mAnswered = new ArrayList<>();
        }

        mAnswered.add(id);
    }

    void addMarked(long id) {
        removeFormAll(id);

        if (mMarked == null) {
            mMarked = new ArrayList<>();
        }

        mMarked.add(id);
    }

    private void removeFormAll(long id) {
        if (mNotVisited != null) {
            mNotVisited.remove(id);
        }

        if (mNotAnswered != null) {
            mNotAnswered.remove(id);
        }

        if (mAnswered != null) {
            mAnswered.remove(id);
        }

        if (mMarked != null) {
            mMarked.remove(id);
        }
    }

    public int getNotVisitedCount() {
        return mNotVisited != null ? mNotVisited.size() : 0;
    }

    public int getNotAnsweredCount() {
        return mNotAnswered != null ? mNotAnswered.size() : 0;
    }

    public int getAnsweredCount() {
        return mAnswered != null ? mAnswered.size() : 0;
    }

    public int getMarkedCount() {
        return mMarked != null ? mMarked.size() : 0;
    }

    public int getQuestionsCount() {
        return getNotVisitedCount() + getNotAnsweredCount() + getAnsweredCount() + getMarkedCount();
    }
}
