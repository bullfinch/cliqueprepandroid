package com.cliqueprep.mocktest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.activity.InstructionsActivity;
import com.cliqueprep.mocktest.activity.MockTestQuestionActivity;
import com.cliqueprep.mocktest.activity.MockTestReviewActivity;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.util.FontUtils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by sreejith on 12/10/16.
 */
public class MockTestRecyclerAdapter extends RecyclerView.Adapter<MockTestRecyclerAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MockTest> mMockTestList;

    public MockTestRecyclerAdapter(Context context) {
        mContext = context;
        mMockTestList = new ArrayList<>();
    }

    public void setData(MockTest[] mockTests) {
        if (mockTests != null && mockTests.length > 0) {
            mMockTestList.clear();
            Collections.addAll(mMockTestList, mockTests);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mock_test_list_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        return mMockTestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final Button mAttendButton;
        private final Button mReviewButton;
        private final Button mResumeButton;
        private final TextView mMockTestName;
        private final TextView mMockTestDate;
        private final TextView mMockTestDay;
        private final TextView mMockTestMonth;
        private final TextView mMockTestYear;
        private final LinearLayout mDateLayout;
        private final ImageView mBackgroundImage;
        private final ImageView mLockImage;

        public ViewHolder(View view) {
            super(view);

            mAttendButton = (Button) view.findViewById(R.id.attend_button);
            mReviewButton = (Button) view.findViewById(R.id.review_button);
            mResumeButton = (Button) view.findViewById(R.id.resume_button);
            mMockTestName = (TextView) view.findViewById(R.id.mock_test_name);
            mMockTestDate = (TextView) view.findViewById(R.id.mock_test_date);
            mMockTestDay = (TextView) view.findViewById(R.id.mock_test_day);
            mMockTestMonth = (TextView) view.findViewById(R.id.mock_test_month);
            mMockTestYear = (TextView) view.findViewById(R.id.mock_test_year);
            mDateLayout = (LinearLayout) view.findViewById(R.id.date_layout);
            mBackgroundImage = (ImageView) view.findViewById(R.id.background_image);
            mLockImage = (ImageView) view.findViewById(R.id.lock_image);

            FontUtils.setFiraSansMedium(mMockTestName);
            FontUtils.setFiraSansMedium(mMockTestDate);
            FontUtils.setFiraSansMedium(mMockTestDay);
            FontUtils.setFiraSansMedium(mMockTestMonth);
            FontUtils.setFiraSansMedium(mMockTestYear);
            FontUtils.setFiraSansMedium(mReviewButton);
            FontUtils.setFiraSansMedium(mAttendButton);
        }

        public void fillValues(int position) {

            final MockTest mockTest = mMockTestList.get(position);

            setText(mMockTestName, mockTest.getTitle());
            setText(mMockTestDate, mockTest.getReleaseOn());

            hideView(mReviewButton);
            hideView(mResumeButton);
            hideView(mAttendButton);

            if (mockTest.isOpen()) {
                showView(mBackgroundImage);
                hideView(mDateLayout);
                hideView(mLockImage);

                if (mockTest.isInProgress()) {
                    showView(mResumeButton);
                } else if (mockTest.hasAttemptedMockTest()) {
                    showView(mReviewButton);
                } else {
                    showView(mAttendButton);
                }
            } else {
                showView(mLockImage);
                showView(mDateLayout);
                hideView(mBackgroundImage);
                showView(mLockImage);
            }

            mAttendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, InstructionsActivity.class);
                    intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST, mockTest);
                    mContext.startActivity(intent);
                }
            });

            mResumeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, MockTestQuestionActivity.class);
                    intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST, mockTest);
                    intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_RESUME, true);
                    mContext.startActivity(intent);
                }
            });

            //TODO: the activity which should be opened was not created
            //TODO: it is needed because attempt id is only available in that activity
            /*mReviewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, MockTestReviewActivity.class);
                    intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST, mockTest);
                    mContext.startActivity(intent);
                }
            });*/
        }
    }

    private void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void setText(TextView textView, String text) {
        if (textView != null) {
            textView.setText(text);
        }
    }
}