package com.cliqueprep.mocktest.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.mocktest.model.MTQuestion;
import com.cliqueprep.mocktest.model.MockTestAttempt;
import com.cliqueprep.mocktest.model.MockTestReview;
import com.cliqueprep.util.FontUtils;

import java.util.ArrayList;


/**
 * Created by tony on 10/1/17.
 */

public class MockTestQuestionRecyclerViewAdapter extends RecyclerView.Adapter<MockTestQuestionRecyclerViewAdapter.ViewHolder> {

    private final MockTestReview mMockTestReview;
    private final ArrayList<String> mSelectedOptionsArray;
    private final ArrayList<String> mCorrectAnswerArray;
    private final ArrayList<MTQuestion> mMTQuestionArray;

    public MockTestQuestionRecyclerViewAdapter(MockTestReview mockTestReview) {
        mMockTestReview = mockTestReview;
        mCorrectAnswerArray = new ArrayList<>();
        mSelectedOptionsArray = new ArrayList<>();
        mMTQuestionArray = new ArrayList<>();

        addQuestionsDetailsToArray();
    }

    private void addQuestionsDetailsToArray() {
        MockTestAttempt mockTestAttempt = mMockTestReview.getMockTestAttempt();
        String questionId;

        if (mockTestAttempt != null && mockTestAttempt.getSubjects() != null) {
            for (String subjectName : mockTestAttempt.getSubjects()) {

                for (int j = 1; j <= mockTestAttempt.getQuestionCount(subjectName); j++) {
                    MTQuestion currentQuestion = mockTestAttempt.getQuestion(subjectName, j);
                    mMTQuestionArray.add(currentQuestion);

                    if (currentQuestion != null) {
                        questionId = String.valueOf(currentQuestion.getId());
                        //In response the correct answer and selected answer comes in 2 different models
                        //Mocktest attempt model and answer
                        // only link between them is the question id


                        //In this if statement the correct answer is stored to correct answer array
                        //in the order we get the questions and details in response
                        //there would surely be an answer for every id
                        if (mMockTestReview.getAnswers() != null) {
                            mCorrectAnswerArray.add(mMockTestReview.getAnswers().get(questionId).toString());
                        }


                        //in below if statement from the sub section of attempt we get the selected option
                        //the response we get is not option code directly the option string(backend change affects web)
                        //some times we get no selected string(question not answered) in such case we add the hardcoded value none
                        if (currentQuestion.getAnswer() == null) {
                            mSelectedOptionsArray.add("none");
                        } else {
                            mSelectedOptionsArray.add(currentQuestion.getAnswer());
                        }
                    }
                }
            }
        }
    }

    @Override
    public MockTestQuestionRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_mocktest_individual_question, parent, false);
        return new MockTestQuestionRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MockTestQuestionRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        if (mMockTestReview != null && mMockTestReview.getAnswers() != null) {
            return mMockTestReview.getAnswers().size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mItemView;
        private TextView mQuestionText;
        private TextView mResultText;
        private String mCorrectAnswerOption;

        private ViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;

            mQuestionText = (TextView) itemView.findViewById(R.id.mock_test_review_question_text_view);
            mResultText = (TextView) itemView.findViewById(R.id.mock_test_question_result_text);

            FontUtils.setFiraSansRegular(mQuestionText);
            FontUtils.setFiraSansRegular(mResultText);
        }

        private void fillValues(int position) {
            if (mQuestionText != null && mMTQuestionArray != null && mSelectedOptionsArray != null) {

                if (mMTQuestionArray.get(position) != null) {
                    String questionText = mMTQuestionArray.get(position).getQuestion_text();
                    String instructionText = mMTQuestionArray.get(position).getInstructions();

                    if (questionText.equals(instructionText)) {
                        mQuestionText.setText(Html.fromHtml(questionText));
                    } else {
                        mQuestionText.setText(Html.fromHtml(instructionText + "<br>" + questionText));
                        //in comprehension section there are 2 sections for question in other it is single line
                    }

                    mCorrectAnswerOption = getStringFromArray(mCorrectAnswerArray, position);

                    if (mCorrectAnswerOption != null) {
                        mCorrectAnswerOption = mCorrectAnswerOption.toUpperCase();

                        String selectedOption = getStringFromArray(mSelectedOptionsArray, position);

                        if (selectedOption != null) {
                            displayOptionsList(selectedOption, position);
                            //values in id array and selected option array are stored in correct order
                        }
                    }
                }
            }
        }

        private String getStringFromArray(ArrayList<String> arrayList, int position) {
            String stringFromArray = null;

            if (arrayList != null) {
                int len = arrayList.size();
                if (position >= 0 && position < len) {
                    stringFromArray = arrayList.get(position);
                }
            }

            return stringFromArray;
        }

        private void displayOptionsList(String selectedOptionText, int position) {
            ArrayList<String> optionsArray = new ArrayList<>();
            String optionSelectedPosition = null;

            if (mMTQuestionArray != null) {
                MTQuestion currentQuestion = mMTQuestionArray.get(position);
                //code selects individual question from the parent subject using for loop

                if (currentQuestion != null) {
                    for (int k = 0; k < currentQuestion.getOptions().size(); k++) {
                        optionsArray.add(currentQuestion.getOptions().get(k));
                        //we cannot predict the number of options for every questions
                        //so we create option array and stores options to the array

                        if (selectedOptionText.equals(currentQuestion.getOptions().get(k))) {
                            //since  we do not get the selected option code
                            //using for loop we compare all options with the selected option
                            //here 65+k gives us the ascii value A,B,C,D...
                            //thus we can get selected option code
                            optionSelectedPosition = String.valueOf((char) (65 + k));
                        }
                    }

                    if (optionSelectedPosition == null) {
                        //if optionSelectedPosition is null we assume no choices were made
                        optionSelectedPosition = "None";
                    }

                    //there is a textView under options for every question with format "Your Choice A : Correct Answer B "
                    if (mResultText != null && mItemView != null) {
                        mResultText.setText(mItemView.getResources().getString(R.string.string_mock_test_review_result,
                                optionSelectedPosition, mCorrectAnswerOption));
                        setQuestionsOptionsRecyclerView(optionsArray, optionSelectedPosition);
                    }
                }
            }
        }

        private void setQuestionsOptionsRecyclerView(ArrayList<String> mOptionsArray, String optionSelectedPosition) {

            RecyclerView mockTestOptionsReviewRecyclerView = (RecyclerView)
                    itemView.findViewById(R.id.mock_test_questions_options_recycler_view);

            if (mockTestOptionsReviewRecyclerView != null) {
                mockTestOptionsReviewRecyclerView.setHasFixedSize(false);
                mockTestOptionsReviewRecyclerView.setLayoutManager(new LinearLayoutManager(mItemView.getContext()));
                mockTestOptionsReviewRecyclerView.setAdapter(new MockTestOptionsRecyclerView(mOptionsArray,
                        optionSelectedPosition, mCorrectAnswerOption));
                //we send options array selected option and correct option
            }
        }
    }
}
