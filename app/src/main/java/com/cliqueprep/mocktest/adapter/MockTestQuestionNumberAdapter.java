package com.cliqueprep.mocktest.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.mocktest.model.MTQuestion;
import com.cliqueprep.mocktest.model.MTSubject;
import com.cliqueprep.util.FontUtils;

/**
 * Created by sreejith on 12/10/16.
 */
public class MockTestQuestionNumberAdapter extends RecyclerView.Adapter<MockTestQuestionNumberAdapter.ViewHolder> {
    private static final String EXTRA_COURSE_ID = "course_id";

    private Context mContext;
    private MTSubject mQuestions;
    QuestionSelectionListener mListener;
    private int mCurrentQuestionNo;

    public MockTestQuestionNumberAdapter(Context context, MTSubject questions, QuestionSelectionListener listener) {
        mContext = context;
        mQuestions = questions;
        mListener = listener;
    }

    public void setQuestions(MTSubject questions) {
        mQuestions = questions;
        mCurrentQuestionNo = 0;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_count_grid_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        if (mQuestions != null) {
            return mQuestions.size();
        } else {
            return 0;
        }
    }

    private String getItemKey(int position) {
        return String.valueOf(position + 1);
    }

    private MTQuestion getItem(int position) {
        return mQuestions.get(getItemKey(position));
    }

    public MTQuestion getNextQuestion() {
        if (getItemCount() > mCurrentQuestionNo + 1) {
            mCurrentQuestionNo++;
            return getItem(mCurrentQuestionNo);
        }

        return null;
    }

    public int getCurrentQuestionPosition() {
        return mCurrentQuestionNo;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mQuestionNo;

        public ViewHolder(View view) {
            super(view);

            mQuestionNo = (TextView) view.findViewById(R.id.count_text);

            FontUtils.setFiraSansRegular(mQuestionNo);
        }

        public void fillValues(final int position) {
            MTQuestion question = getItem(position);

            if (mQuestionNo != null) {
                mQuestionNo.setText(getItemKey(position));

                if (position == getCurrentQuestionPosition()) {
                    mQuestionNo.setBackgroundResource(R.drawable.ic_white_circle_with_blue_stroke);
                    mQuestionNo.setTextColor(ContextCompat.getColor(mContext, R.color.text_color));
                } else if (question.isMarked()) {
                    mQuestionNo.setBackgroundResource(R.drawable.ic_circle_red);
                    mQuestionNo.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                } else if (question.isAnswered()) {
                    mQuestionNo.setBackgroundResource(R.drawable.ic_circle_green);
                    mQuestionNo.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                } else if (question.isVisited()) {
                    mQuestionNo.setBackgroundResource(R.drawable.ic_circle_blue);
                    mQuestionNo.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                } else {
                    mQuestionNo.setTextColor(ContextCompat.getColor(mContext, R.color.text_color));
                    mQuestionNo.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                }

                mQuestionNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCurrentQuestionNo = position;
                        mListener.onQuestionSelected(getItem(position));
                    }
                });
            }
        }
    }

    public interface QuestionSelectionListener {
        public void onQuestionSelected(MTQuestion question);
    }
}