package com.cliqueprep.mocktest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.mocktest.model.SubjectWiseSummary;
import com.cliqueprep.mocktest.model.QuestionTypeWiseSummary;
import com.cliqueprep.util.FontUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by sreejith on 12/10/16.
 */
public class MockTestResultRecyclerAdapter extends RecyclerView.Adapter<MockTestResultRecyclerAdapter.ViewHolder> {

    private List<String> mQuestionTypeList;
    private SubjectWiseSummary mSubjectSummaryMap;

    public MockTestResultRecyclerAdapter() {
        mQuestionTypeList = new ArrayList<>();
    }

    public void setData(SubjectWiseSummary subjectSummaryMap) {
        if (mQuestionTypeList != null) {
            mQuestionTypeList.clear();
        }

        if (subjectSummaryMap != null) {
            Set<String> keySet = subjectSummaryMap.keySet();
            mQuestionTypeList = new ArrayList<>(keySet);
            mSubjectSummaryMap = subjectSummaryMap;
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mock_test_result_list_item, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        return mQuestionTypeList != null ? mQuestionTypeList.size() : 0;
    }

    private void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    private void setText(TextView textView, String text) {
        if (textView != null) {
            textView.setText(text);
        }
    }

    private void setText(TextView textView, Integer text) {
        if (textView != null) {
            int textVal = text == null ? 0 : text;
            textView.setText(String.valueOf(textVal));
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mQuestionCount;
        private final TextView mAnsweredCount;
        private final TextView mNotAnsweredCount;
        private final TextView mReviewCount;
        private final TextView mNotVisitedCount;
        private final TextView mHeaderText;

        public ViewHolder(View view) {
            super(view);

            mQuestionCount = (TextView) view.findViewById(R.id.question_count);
            mAnsweredCount = (TextView) view.findViewById(R.id.answered_count);
            mNotAnsweredCount = (TextView) view.findViewById(R.id.not_answered_count);
            mReviewCount = (TextView) view.findViewById(R.id.review_count);
            mNotVisitedCount = (TextView) view.findViewById(R.id.not_visited_count);
            mHeaderText = (TextView) view.findViewById(R.id.result_header);

            FontUtils.setFiraSansMedium(mQuestionCount);
            FontUtils.setFiraSansMedium(mAnsweredCount);
            FontUtils.setFiraSansMedium(mNotAnsweredCount);
            FontUtils.setFiraSansMedium(mReviewCount);
            FontUtils.setFiraSansMedium(mNotVisitedCount);
            FontUtils.setFirasansBold(mHeaderText);
        }

        void fillValues(int position) {
            QuestionTypeWiseSummary questionTypeWiseSummary = null;

            if (mQuestionTypeList != null) {
                String questionType = mQuestionTypeList.get(position);
                setText(mHeaderText, questionType);

                if (mSubjectSummaryMap != null) {
                    questionTypeWiseSummary = mSubjectSummaryMap.get(questionType);
                }
            }

            if (questionTypeWiseSummary != null) {
                setText(mQuestionCount, questionTypeWiseSummary.getQuestionsCount());
                setText(mAnsweredCount, questionTypeWiseSummary.getAnsweredCount());
                setText(mNotAnsweredCount, questionTypeWiseSummary.getNotAnsweredCount());
                setText(mReviewCount, questionTypeWiseSummary.getMarkedCount());
                setText(mNotVisitedCount, questionTypeWiseSummary.getNotVisitedCount());
            }
        }
    }
}