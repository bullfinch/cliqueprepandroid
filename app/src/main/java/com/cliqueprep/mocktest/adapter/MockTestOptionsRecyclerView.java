package com.cliqueprep.mocktest.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;

import java.util.ArrayList;

/**
 * Created by tony on 10/1/17.
 */

public class MockTestOptionsRecyclerView extends RecyclerView.Adapter<MockTestOptionsRecyclerView.ViewHolder> {

    private ArrayList<String> mOptionsArray;
    private String mCorrectOption;
    private String mSelectedOption;

    public MockTestOptionsRecyclerView(ArrayList<String> optionsArray, String selectedOption, String correctOption) {
        mOptionsArray = optionsArray;
        mCorrectOption = correctOption;
        mSelectedOption = selectedOption;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mocktest_answer_fragment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        return mOptionsArray != null ? mOptionsArray.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mOptionsText;
        private TextView mOptionCodeText;
        private int mTextColor;

        public ViewHolder(View itemView) {
            super(itemView);

            mOptionsText = (TextView) itemView.findViewById(R.id.mock_test_answer_text_view);
            mOptionCodeText = (TextView) itemView.findViewById(R.id.mock_test_options_code);
            mTextColor = ContextCompat.getColor(itemView.getContext(), R.color.white);

            FontUtils.setFiraSansRegular(mOptionsText);
            FontUtils.setFiraSansRegular(mOptionCodeText);
        }

        public void fillValues(int position) {
            String optionNumberText;

            if (mOptionsText != null && mOptionCodeText != null) {
                optionNumberText = String.valueOf((char) (65 + position));
                mOptionsText.setText(Html.fromHtml(getStringFromArray(mOptionsArray, position)));
                mOptionCodeText.setText(optionNumberText);
                colorOptions(optionNumberText);
            }
        }

        private String getStringFromArray(ArrayList<String> arrayList, int position) {
            String stringFromArray = null;

            if (arrayList != null) {
                int len = arrayList.size();
                if (position >= 0 && position < len) {
                    stringFromArray = arrayList.get(position);
                }
            }

            return stringFromArray;
        }

        public void colorOptions(String optionNumberText) {
            if (mCorrectOption != null && mSelectedOption != null) {
                if (mCorrectOption.equals(mSelectedOption)) {
                    if (mCorrectOption.equals(optionNumberText) && mOptionsText != null) {
                        mOptionCodeText.setBackgroundResource(R.drawable.green_circle);
                    }

                } else {
                    if (mOptionCodeText != null) {
                        if (mCorrectOption.equals(optionNumberText)) {
                            mOptionCodeText.setBackgroundResource(R.drawable.green_circle);
                            mOptionCodeText.setTextColor(mTextColor);
                        }

                        if (mSelectedOption.equals(optionNumberText)) {
                            mOptionCodeText.setBackgroundResource(R.drawable.red_circle);
                            mOptionCodeText.setTextColor(mTextColor);
                        }
                    }
                }
            }
        }
    }
}