package com.cliqueprep.mocktest.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.util.FontUtils;
import com.github.rahulrvp.android_utils.EditTextUtils;

/**
 * Created by sreejith on 13/10/16.
 */

public class RollNumberActivity extends CliquePrepBaseActivity {
    private MockTest mMockTest;
    private EditText mRollnumberField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rollnumber);

        mContext = this;
        mMockTest = (MockTest) getIntent().getSerializableExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST);
        if (mMockTest == null) {
            finish();
        }

        mRollnumberField = (EditText) findViewById(R.id.rollnumber_edittext);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }


        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        setFonts();

        enableGoActionOnRollNo();
    }

    private void enableGoActionOnRollNo() {
        if (mRollnumberField != null) {
            mRollnumberField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;

                    if (actionId == EditorInfo.IME_ACTION_GO) {
                        onSubmitClicked(null);

                        hideKeyboard();

                        handled = true;
                    }

                    return handled;
                }
            });
        }
    }

    private boolean isValidRollNo() {
        String rollNumber = EditTextUtils.getText(mRollnumberField);

        if (TextUtils.isEmpty(rollNumber)) {
            EditTextUtils.setError(mRollnumberField, R.string.err_roll_no_cant_be_empty);
            return false;
        }

        int minLength = 5;
        if (rollNumber.length() < minLength) {
            EditTextUtils.setError(mRollnumberField, R.string.err_at_least_n_chars_needed, minLength);
            return false;
        }

        return true;
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setFonts() {
        FontUtils.setFirasansBold(findViewById(R.id.toolbar_title));
        FontUtils.setFiraSansMedium(findViewById(R.id.instruction_header));
        FontUtils.setFiraSansRegular(mRollnumberField);
        FontUtils.setFiraSansMedium(findViewById(R.id.accept_button));
    }

    public void onSubmitClicked(View view) {
        if (isValidRollNo()) {
            Intent intent = new Intent(mContext, MockTestQuestionActivity.class);
            intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST, mMockTest);
            intent.putExtra(MockTestQuestionActivity.EXTRA_KEY_ROLL_NUMBER, mRollnumberField.getText().toString());
            startActivity(intent);
            finish();
        }
    }
}
