package com.cliqueprep.mocktest.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cliqueprep.R;
import com.cliqueprep.listener.OnSwipeTouchListener;
import com.cliqueprep.mocktest.MockTestAttemptCache;
import com.cliqueprep.mocktest.adapter.MockTestQuestionNumberAdapter;
import com.cliqueprep.mocktest.async.SaveAnswersTask;
import com.cliqueprep.mocktest.async.StartMockTestTask;
import com.cliqueprep.mocktest.model.MTQuestion;
import com.cliqueprep.mocktest.model.MTSubject;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.mocktest.model.MockTestAttempt;
import com.cliqueprep.mocktest.model.MockTestSummary;
import com.cliqueprep.view.OptionView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import io.github.kexanie.library.MathView;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by sreejith on 14/10/16.
 */

public class MockTestQuestionActivity extends AppCompatActivity implements MockTestQuestionNumberAdapter.QuestionSelectionListener {
    public static final String EXTRA_KEY_MOCK_TEST = "mock_test";
    public static final String EXTRA_KEY_RESUME = "resume";
    public static final String EXTRA_KEY_ROLL_NUMBER = "roll_number";

    private final MockTestSummary mCompleteSummary = new MockTestSummary();

    private MockTest mMockTest;
    private MockTestAttempt mMockTestAttempt;
    private Context mContext;
    private ArrayList<OptionView> mOptionViewPool;

    private MockTestQuestionNumberAdapter mQuestionCountRecyclerAdapter;
    private TextView mSetTimerText;
    private MathView mQuestionText;
    private LinearLayout mOptionsLayout;
    private MTQuestion mCurrentQuestion;
    private String mCurrentSubject;
    private Button mMarkForReviewBtn;
    private RecyclerView mQuestionCountRecyclerView;
    private Spinner mSubjectSpinner;
    private View mQNumberMockView;
    private long mSecondsUntilFinished;
    private boolean mHelpVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_test_question);
        mContext = this;
        mCurrentSubject = "";

        mMockTest = (MockTest) getIntent().getSerializableExtra(MockTestQuestionActivity.EXTRA_KEY_MOCK_TEST);
        if (mMockTest == null) {
            finish();
        }

        mSetTimerText = (TextView) findViewById(R.id.timer_text);
        mOptionsLayout = (LinearLayout) findViewById(R.id.options_layout);
        mQuestionText = (MathView) findViewById(R.id.question_text);
        mMarkForReviewBtn = (Button) findViewById(R.id.mark_for_review_button);
        mSubjectSpinner = (Spinner) findViewById(R.id.subject_spinner);
        mQNumberMockView = findViewById(R.id.q_no_mock_view);

        mQuestionCountRecyclerView = (RecyclerView) findViewById(R.id.question_count_recycler);
        mQuestionCountRecyclerAdapter = new MockTestQuestionNumberAdapter(mContext, null, this);
        setAdapter(mQuestionCountRecyclerView, getLinearLayoutManager());

        if (mSetTimerText != null) {
            mSetTimerText.setText(R.string._00_00);
        }

        showHelp();

        initializeOptionViewPool();
        fetchMockTestQuestions();

        if (mQuestionCountRecyclerView != null) {
            mQuestionCountRecyclerView.setOnTouchListener(new OnSwipeTouchListener(this) {

                public void onSwipeTop() {
                    if (mQuestionCountRecyclerView.getLayoutManager().getClass() == GridLayoutManager.class) {
                        setAdapter(mQuestionCountRecyclerView, getLinearLayoutManager());
                    }
                }

                public void onSwipeRight() {
                }

                public void onSwipeLeft() {
                }

                public void onSwipeBottom() {
                    if (mQuestionCountRecyclerView.getLayoutManager().getClass() == LinearLayoutManager.class) {
                        setAdapter(mQuestionCountRecyclerView, getGridLayoutManager());
                    }
                }
            });
        }
    }

    private void showHelp() {
        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500);

        MaterialShowcaseSequence showcaseSequence = new MaterialShowcaseSequence(this, "mt_help");
        showcaseSequence.setConfig(config);

        String gotIt = getString(R.string.help_got_it);

        // Change Subject
        showcaseSequence.addSequenceItem(
                mSubjectSpinner,
                getString(R.string.help_change_sub),
                gotIt);

        // Question numbers display
        showcaseSequence.addSequenceItem(
                mQNumberMockView,
                getString(R.string.help_question_numbers),
                gotIt);

        // Mark for review
        showcaseSequence.addSequenceItem(
                mMarkForReviewBtn,
                getString(R.string.help_mark_for_review),
                gotIt);

        showcaseSequence.setOnItemDismissedListener(new MaterialShowcaseSequence.OnSequenceItemDismissedListener() {
            @Override
            public void onDismiss(MaterialShowcaseView materialShowcaseView, int i) {
                if (i == 2) {
                    // closed the last help screen
                    startMockTestTimer();
                }
            }
        });

        showcaseSequence.start();

        // hadFired returns false means we're showing the help
        setHelpVisible(!showcaseSequence.hasFired());
    }

    private boolean isHelpVisible() {
        return mHelpVisible;
    }

    private void setHelpVisible(boolean helpStatus) {
        mHelpVisible = helpStatus;
    }

    @Override
    public void onQuestionSelected(MTQuestion question) {
        mCurrentQuestion = question;
        updateQuestion();
    }

    private void initializeOptionViewPool() {
        mOptionViewPool = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            mOptionViewPool.add(new OptionView(mContext));
        }
    }

    private OptionView getOptionViewFromPool(int position) {
        if (mOptionViewPool.size() > position) {
            return mOptionViewPool.get(position);
        } else {
            return new OptionView(mContext);
        }
    }

    private void updateQuestion() {
        if (mCurrentQuestion != null) {
            mCurrentQuestion.setVisited();
            addOptions();
            if (mQuestionText != null) {
                mQuestionText.setEngine(MathView.Engine.MATHJAX);
                mQuestionText.config("MathJax.Hub.Config({\n" +
                        " tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']]}});");
                mQuestionText.setText(mCurrentQuestion.getQuestion_text());
            }
            mQuestionCountRecyclerAdapter.notifyDataSetChanged();

            trackNotAnswered();
        } else {
            Toast.makeText(mContext, R.string.no_questions_msg, Toast.LENGTH_SHORT).show();
        }
    }

    private void addOptions() {
        mOptionsLayout.removeAllViews();

        int position = 0;
        for (final String option : mCurrentQuestion.getOptions()) {
            final OptionView optionView = new OptionView(mContext);
            //final OptionView optionView = getOptionViewFromPool(position);
            optionView.setContent(option);

            if (mCurrentQuestion.isAnswered(option)) {
                optionView.setSelected();
            }

            optionView.setCallback(new OptionView.Callback() {
                @Override
                public void helpClicked() {
                }

                @Override
                public void onSelected() {
                    clearOptionSelections();
                    mCurrentQuestion.setAnswered(option);
                    mQuestionCountRecyclerAdapter.notifyDataSetChanged();

                    trackAnswered();
                }
            });

            mOptionsLayout.addView(optionView);
            position++;
        }
    }

    private void clearOptionSelections() {
        int count = mOptionsLayout.getChildCount();

        for (int i = 0; i < count; i++) {
            OptionView optionView = (OptionView) mOptionsLayout.getChildAt(i);
            optionView.clearSelection();
        }
    }

    private void startMockTestTimer() {
        if (mMockTestAttempt.getMock_test() != null) {
            setTimer(mMockTestAttempt.getMock_test().getRemainingTimeInMillis());
        }
    }

    private void fetchMockTestQuestions() {
        String rollNumber = getIntent().getStringExtra(EXTRA_KEY_ROLL_NUMBER);

        new StartMockTestTask(this, getIntent().getBooleanExtra(EXTRA_KEY_RESUME, false), rollNumber, mMockTest, new StartMockTestTask.Callback() {
            @Override
            public void onSuccess(MockTestAttempt attempt) {
                mMockTestAttempt = attempt;

                if (mMockTestAttempt != null) {
                    if (!isHelpVisible()) {
                        startMockTestTimer();
                    }

                    updateMockTestLocalSummary(mMockTestAttempt);

                    if (mSubjectSpinner != null) {
                        mSubjectSpinner.setAdapter(new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, mMockTestAttempt.getSubjects()));
                        mSubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                mCurrentSubject = (String) mSubjectSpinner.getSelectedItem();
                                MTSubject subjectQuestions = mMockTestAttempt.getSubject(mCurrentSubject);
                                if (mQuestionCountRecyclerAdapter != null) {
                                    mQuestionCountRecyclerAdapter.setQuestions(subjectQuestions);
                                }
                                mCurrentQuestion = subjectQuestions.get("1");
                                updateQuestion();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                            }
                        });

                        mCurrentSubject = (String) mSubjectSpinner.getSelectedItem();
                        MTSubject subjectQuestions = mMockTestAttempt.getSubject(mCurrentSubject);
                        if (mQuestionCountRecyclerAdapter != null) {
                            mQuestionCountRecyclerAdapter.setQuestions(subjectQuestions);
                        }
                        mCurrentQuestion = subjectQuestions.get("1");
                        updateQuestion();
                    }
                }
            }

            @Override
            public void onFailure() {
                finish();
            }
        }).execute();

    }

    private LinearLayoutManager getLinearLayoutManager() {
        return new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
    }

    private GridLayoutManager getGridLayoutManager() {
        return new GridLayoutManager(mContext, 7);
    }

    private void setAdapter(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        if (recyclerView != null && layoutManager != null) {
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(mQuestionCountRecyclerAdapter);
        }
    }

    private void showTimeText(long seconds) {
        long minutesLeft = seconds / 60;
        long secondsLeft = seconds % 60;
        String timeText = String.format(Locale.getDefault(), "%2d:%02d", minutesLeft, secondsLeft);
        if (mSetTimerText != null) {
            mSetTimerText.setText(timeText);
        }
    }

    public void setTimer(long duration) {
        showTimeText(duration / 1000);

        new CountDownTimer(duration, 1000) {
            public void onTick(long millisUntilFinished) {
                mSecondsUntilFinished = millisUntilFinished / 1000;
                if (mSecondsUntilFinished <= 0) {
                    mSetTimerText.setText(R.string._00_00);
                } else {
                    if (mCurrentQuestion != null) {
                        mCurrentQuestion.incrementTimeSpent();
                    }

                    showTimeText(mSecondsUntilFinished);
                }

                if (mSecondsUntilFinished % 60 == 0) {
                    new SaveAnswersTask(mContext, mMockTestAttempt, false, null).execute();
                }
            }

            public void onFinish() {
                mSetTimerText.setText(getResources().getText(R.string.timer));
                new SaveAnswersTask(mContext, mMockTestAttempt, false, null).execute();
                String summaryJson = new Gson().toJson(mCompleteSummary);
                MockTestAttemptCache.getInstance().setAttempt(mMockTestAttempt);

                Intent intent = new Intent(mContext, MockTestResultActivity.class);
                intent.putExtra(MockTestResultActivity.EXTRA_SUMMARY, summaryJson);
                intent.putExtra(MockTestResultActivity.EXTRA_SHOULD_AUTO_SUBMIT, true);
                startActivity(intent);
                finish();
            }
        }.start();
    }

    public void clearAnswer(View view) {
        if (mCurrentQuestion != null) {
            mCurrentQuestion.setAnswered(null);
            clearOptionSelections();
            mQuestionCountRecyclerAdapter.notifyDataSetChanged();

            trackNotAnswered();
        }
    }

    public void markQuestion(View view) {
        if (mCurrentQuestion != null) {
            mCurrentQuestion.setMarked(true);
            mQuestionCountRecyclerAdapter.notifyDataSetChanged();

            trackMarked();
        }
    }

    private void loadNextQuestion() {
        MTQuestion question = mQuestionCountRecyclerAdapter.getNextQuestion();
        if (question != null) {
            onQuestionSelected(question);
        }
    }

    public void nextQuestion(View view) {
        // load the next question if available
        loadNextQuestion();
        // scroll the question number palette
        scrollToNewQnNumber();
    }

    private void scrollToNewQnNumber() {
        if (mQuestionCountRecyclerView != null && mQuestionCountRecyclerAdapter != null) {
            int newPosition = mQuestionCountRecyclerAdapter.getCurrentQuestionPosition();
            mQuestionCountRecyclerView.smoothScrollToPosition(newPosition);
        }
    }

    public void onSummaryClicked(View view) {
        new SaveAnswersTask(mContext, mMockTestAttempt, false, null).execute();
        launchSummaryActivity();
        if (mSecondsUntilFinished <= 0) {
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        new SaveAnswersTask(mContext, mMockTestAttempt, false, null).execute();
    }

    // ========== SUMMARY ============

    private void launchSummaryActivity() {
        String summaryJson = new Gson().toJson(mCompleteSummary);
        MockTestAttemptCache.getInstance().setAttempt(mMockTestAttempt);

        Intent intent = new Intent(mContext, MockTestResultActivity.class);
        intent.putExtra(MockTestResultActivity.EXTRA_SUMMARY, summaryJson);
        startActivity(intent);
    }

    private void trackAnswered() {
        trackAnswered(mCurrentSubject, mCurrentQuestion);
    }

    private void trackAnswered(String subject, MTQuestion question) {
        if (!TextUtils.isEmpty(subject) && question != null) {
            mCompleteSummary.addAnswered(subject, question.getType(), question.getId());
        }
    }

    private void trackMarked() {
        trackMarked(mCurrentSubject, mCurrentQuestion);
    }

    private void trackMarked(String subject, MTQuestion question) {
        if (!TextUtils.isEmpty(subject) && question != null) {
            mCompleteSummary.addMarked(subject, question.getType(), question.getId());
        }
    }

    private void trackNotAnswered() {
        trackNotAnswered(mCurrentSubject, mCurrentQuestion);
    }

    private void trackNotAnswered(String subject, MTQuestion question) {
        if (!TextUtils.isEmpty(subject) && question != null) {
            mCompleteSummary.addNotAnswered(subject, question.getType(), question.getId());
        }
    }

    private void trackNotVisited(String subject, MTQuestion question) {
        if (!TextUtils.isEmpty(subject) && question != null) {
            mCompleteSummary.addNotVisited(subject, question.getType(), question.getId());
        }
    }

    private void updateMockTestLocalSummary(MockTestAttempt mockTestAttempt) {
        if (mockTestAttempt != null) {
            ArrayList<String> subjects = mockTestAttempt.getSubjects();

            for (String subject : subjects) {
                MTSubject mtSubject = mockTestAttempt.getSubject(subject);

                if (mtSubject != null) {
                    Collection<MTQuestion> questions = mtSubject.getQuestions();

                    for (MTQuestion question : questions) {
                        trackNotVisited(subject, question);

                        if (question != null) {
                            if (question.isAnswered()) {
                                trackAnswered(subject, question);
                            } else if (question.isMarked()) {
                                trackMarked(subject, question);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        // No back button press in this page
    }
}
