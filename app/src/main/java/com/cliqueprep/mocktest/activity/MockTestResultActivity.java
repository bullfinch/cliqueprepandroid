package com.cliqueprep.mocktest.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.cliqueprep.R;
import com.cliqueprep.adapter.TabedFragmentPagerAdapter;
import com.cliqueprep.mocktest.MockTestAttemptCache;
import com.cliqueprep.mocktest.async.SaveAnswersTask;
import com.cliqueprep.mocktest.fragment.MockTestResultFragment;
import com.cliqueprep.mocktest.model.MockTestAttempt;
import com.cliqueprep.mocktest.model.MockTestSummary;
import com.cliqueprep.mocktest.model.SubjectWiseSummary;
import com.cliqueprep.util.BFUtils;
import com.cliqueprep.util.FontUtils;
import com.cliqueprep.util.NetworkUtils;
import com.google.gson.Gson;

import java.util.Set;

/**
 * Created by sreejith on 20/10/16.
 */

public class MockTestResultActivity extends AppCompatActivity {

    public static final String EXTRA_SUMMARY = "summary";
    public static final String EXTRA_SHOULD_AUTO_SUBMIT = "auto_submit";
    private static final String LOG_TAG = "MockTestSummary";
    private ProgressDialog mProgress;

    private MockTestSummary mSummary;
    private MockTestAttempt mAttempt;
    private Boolean mIsAutoSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_test_result);

        mProgress = new ProgressDialog(this);

        String summaryJson = getIntent().getStringExtra(EXTRA_SUMMARY);
        if (summaryJson != null) {
            try {
                mSummary = new Gson().fromJson(summaryJson, MockTestSummary.class);
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }

        mAttempt = MockTestAttemptCache.getInstance().getAttempt();

        if (mSummary == null || mAttempt == null) {
            finish();
        }

        ViewPager resultViewPager = (ViewPager) findViewById(R.id.result_view_pager);
        TabLayout resultTabLayout = (TabLayout) findViewById(R.id.result_tab_layout);
        LinearLayout bottomLayout = (LinearLayout) findViewById(R.id.bottom_layout);
        TabedFragmentPagerAdapter resultAdapter = new TabedFragmentPagerAdapter(getSupportFragmentManager());
        mIsAutoSubmit = getIntent().getBooleanExtra(EXTRA_SHOULD_AUTO_SUBMIT, false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (resultViewPager != null) {
            resultViewPager.setAdapter(resultAdapter);
        }

        resultAdapter.clearAll();
        Set<String> keySet = mSummary.keySet();
        for (String key : keySet) {
            SubjectWiseSummary subjectSummary = mSummary.get(key);
            resultAdapter.addFragment(MockTestResultFragment.newInstance(subjectSummary), key);
        }

        if (resultTabLayout != null) {
            resultTabLayout.setupWithViewPager(resultViewPager);
        }

        setFont();

        if (bottomLayout != null) {
            if (mIsAutoSubmit) {
                bottomLayout.setVisibility(View.GONE);
                onSubmitClick(null);
            } else {
                bottomLayout.setVisibility(View.VISIBLE);
            }
        }

    }

    private void setFont() {
        FontUtils.setFirasansBold(findViewById(R.id.toolbar_title));

    }

    public void onBackToTestClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        if (mProgress != null && mProgress.isShowing()) {
            // api call is running
            BFUtils.showToast(this, getString(R.string.answer_submission_in_progress));
        } else {
            super.onBackPressed();
        }
    }

    public void onSubmitClick(View view) {
        if (NetworkUtils.isConnectedToNetwork(this)) {
            showProgressDialog(getString(R.string.submitting_answers));

            callSaveApi();
        } else {
            hideProgressDialog();

            BFUtils.showToast(this, getString(R.string.not_connected_try_again));
        }

    }

    private void callSaveApi() {
        new SaveAnswersTask(this, mAttempt, true, new SaveAnswersTask.Callback() {
            @Override
            public void onSuccess() {
                hideProgressDialog();

                BFUtils.showToast(MockTestResultActivity.this, getString(R.string.answer_submitted_successfully));

                if (!mIsAutoSubmit) {
                    launchMockTestList();
                }

                MockTestAttemptCache.getInstance().clear();
            }

            @Override
            public void onFailure() {
                // repeatedly try to call api until we get a success

                onSubmitClick(null);
            }
        }).execute();
    }

    private void launchMockTestList() {
        Intent mtListIntent = new Intent(this, MockTestListActivity.class);
        mtListIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mtListIntent);
        finish();
    }

    private void showProgressDialog(String message) {
        if (mProgress != null) {
            mProgress.setMessage(message);

            if (!mProgress.isShowing()) {
                mProgress.show();
            }
        }
    }

    private void hideProgressDialog() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }
}
