package com.cliqueprep.mocktest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.activity.PracticeSelectionActivity;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.mocktest.async.FetchMockTestsTask;
import com.cliqueprep.mocktest.model.MockTest;
import com.cliqueprep.mocktest.adapter.MockTestRecyclerAdapter;
import com.cliqueprep.util.FontUtils;

/**
 * Created by sreejith on 12/10/16.
 */

public class MockTestListActivity extends CliquePrepBaseActivity {

    private MockTestRecyclerAdapter mMockTestRecyclerAdapter;
    private ProgressBar mProgressBar;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_test_list);

        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mMockTestRecyclerAdapter = new MockTestRecyclerAdapter(mContext);
        RecyclerView mockTestRecyclerView = (RecyclerView) findViewById(R.id.mock_test_recycleriew);
        mProgressBar = (ProgressBar) findViewById(R.id.loader_progress_bar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);

        if (mockTestRecyclerView != null) {
            mockTestRecyclerView.setAdapter(mMockTestRecyclerAdapter);
            mockTestRecyclerView.setLayoutManager(linearLayoutManager);
            mockTestRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }

        setFonts();
    }

    public void setFonts() {
        FontUtils.setFirasansBold(findViewById(R.id.toolbar_title));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_selection_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item != null) {
            if (id == R.id.open_drawer) {
                openSideMenu();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        new FetchMockTestsTask(mContext, User.load(mContext).getSelectedCourseId(), mProgressBar, new FetchMockTestsTask.Callback() {
            @Override
            public void onSuccess(MockTest[] mockTests) {
                mMockTestRecyclerAdapter.setData(mockTests);
            }
        }).execute();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, PracticeSelectionActivity.class));
        finish();
    }
}
