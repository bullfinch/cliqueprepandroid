package com.cliqueprep.mocktest.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.framework.CliquePrepBaseActivity;
import com.cliqueprep.mocktest.adapter.MockTestQuestionRecyclerViewAdapter;
import com.cliqueprep.mocktest.async.MockTestReviewTask;
import com.cliqueprep.mocktest.model.MockTestReview;

/**
 * Created by tony on 10/1/17.
 */

public class MockTestReviewActivity extends CliquePrepBaseActivity {
    private static final String EXTRA_MOCK_TEST_ATTEMPT_ID = "mockTestAttemptId";
    private RecyclerView mMockTestReviewRecyclerView;
    private TextView mNoItemsTextView;
    private ProgressBar mProgressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mocktest_review);
        mContext = this;

        mMockTestReviewRecyclerView = (RecyclerView) findViewById(R.id.mock_test_individual_question_recycler_view);
        mNoItemsTextView = (TextView) findViewById(R.id.no_mock_test_review_text_view);
        mProgressBar = (ProgressBar) findViewById(R.id.mock_test_review_progressbar);

        mMockTestReviewRecyclerView.setHasFixedSize(false);
        mMockTestReviewRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        long mockTestAttemptId = 0;
        //TODO:id is to be recieved from an activity which is not merged to master
        //TODO: it would be completed in next PR

        new MockTestReviewTask(mContext, mockTestAttemptId, new MockTestReviewTask.Callback() {
            @Override
            public void onSuccess(MockTestReview mockTest) {
                displayMockTestReview(mockTest);
            }

            @Override
            public void onFailure() {
                showView(mNoItemsTextView);
                hideView(mProgressBar);
            }

            @Override
            public void onTaskStart() {
                showView(mProgressBar);
            }
        }).execute();
    }

    public void displayMockTestReview(MockTestReview mockTestReview) {
        if (mMockTestReviewRecyclerView != null) {
            hideView(mNoItemsTextView);
            hideView(mProgressBar);
            showView(mMockTestReviewRecyclerView);

            mMockTestReviewRecyclerView.setAdapter(new MockTestQuestionRecyclerViewAdapter(mockTestReview));
        }
    }

    public void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    public void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }
}
