package com.cliqueprep.adapter;

import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.cliqueprep.Model.Subject;
import com.cliqueprep.Model.Topic;
import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.rahulrvp.android_utils.TextViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Rahul Raveendran V P
 *         Created on 4/9/16 @ 7:18 PM
 *         https://github.com/rahulrvp
 */
public class SoloExpandableAdapter extends BaseExpandableListAdapter {

    private final ArrayList<Subject> mSubjectList = new ArrayList<>();

    private final SelectionChangeListener mListener;

    public SoloExpandableAdapter(SelectionChangeListener listener) {
        mListener = listener;
    }

    public void setSubjects(Subject[] subjects) {
        mSubjectList.clear();

        if (subjects != null) {
            Collections.addAll(mSubjectList, subjects);
        }

        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return mSubjectList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Topic> topics = null;

        Subject subject = mSubjectList.get(groupPosition);
        if (subject != null) {
            topics = subject.getTopics();
        }

        return topics != null ? topics.size() : 0;
    }

    @Override
    public Subject getGroup(int groupPosition) {
        return mSubjectList.get(groupPosition);
    }

    @Override
    public Topic getChild(int groupPosition, int topicPosition) {
        Topic topic = null;

        Subject subject = getGroup(groupPosition);
        if (subject != null) {
            ArrayList<Topic> topics = subject.getTopics();
            if (topics != null && topicPosition < topics.size() && topicPosition >= 0) {
                topic = topics.get(topicPosition);
            }
        }

        return topic;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int topicPosition) {
        return topicPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
        SubjectViewHolder viewHolder;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            view = layoutInflater.inflate(R.layout.subject_list_item, viewGroup, false);
            viewHolder = new SubjectViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (SubjectViewHolder) view.getTag();
        }

        Subject subject = getGroup(groupPosition);

        if (subject != null && viewHolder != null) {
            if (!TextUtils.isEmpty(subject.getImageUrl())) {
                viewHolder.setSubjectImg(Uri.parse(subject.getImageUrl()));
            }

            String topicsCount = subject.getSelectedTopicCount() + "-" + getChildrenCount(groupPosition);
            TextViewUtils.setText(viewHolder.mTopicCountText, topicsCount);
            TextViewUtils.setText(viewHolder.mSubjectNameText, subject.getSubjectName());
        }

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int topicPosition, boolean b, View view, ViewGroup viewGroup) {
        TopicViewHolder viewHolder;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            view = layoutInflater.inflate(R.layout.topic_list_item, viewGroup, false);
            viewHolder = new TopicViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (TopicViewHolder) view.getTag();
        }

        Subject subject = getGroup(groupPosition);
        Topic topic = getChild(groupPosition, topicPosition);

        if (viewHolder != null) {
            viewHolder.clearDefaults();
            viewHolder.setSelectAllLayoutVisibility(topicPosition == 0);

            viewHolder.setSubject(subject);
            viewHolder.setTopic(topic);

            if (topic != null) {
                TextViewUtils.setText(viewHolder.mTopicNameText, topic.getTopicName());
            }

            viewHolder.toggleToggleButton();
            viewHolder.toggleCheckBox();

            viewHolder.enableToggleListener();
            viewHolder.enableCheckBoxListener();
        }

        return view;
    }

    public void selectAllTopics() {
        markAllTopics(true);
    }

    public void deselectAllTopics() {
        markAllTopics(false);
    }

    private void markAllTopics(boolean isSelected) {
        for (Subject subject : mSubjectList) {
            if (subject != null) {
                subject.markAll(isSelected);
            }
        }

        notifyDataSetChanged();
    }

    public ArrayList<Integer> getAllSelectedTopics() {
        ArrayList<Integer> selectedTopics = new ArrayList<>();

        for (Subject subject : mSubjectList) {
            if (subject != null) {
                selectedTopics.addAll(subject.getSelectedTopicIds());
            }
        }

        return selectedTopics;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int topicPosition) {
        return true;
    }

    public interface SelectionChangeListener {
        void onSelectionChange(boolean isAllSelected);
    }

    private class SubjectViewHolder {
        private final TextView mSubjectNameText;
        private final TextView mTopicCountText;
        private final SimpleDraweeView mSubjectImage;

        SubjectViewHolder(@NotNull View view) {
            mSubjectNameText = (TextView) view.findViewById(R.id.subject_name_text);
            mTopicCountText = (TextView) view.findViewById(R.id.topic_count_text);
            mSubjectImage = (SimpleDraweeView) view.findViewById(R.id.solo_item_view);

            FontUtils.setFiraSansRegular(mSubjectNameText);
        }

        void setSubjectImg(Uri uri) {
            if (mSubjectImage != null) {
                mSubjectImage.setImageURI(uri);
            }
        }
    }

    private class TopicViewHolder {
        private final CheckBox mSelectTopicCheckBox;
        private final ToggleButton mSelectTopicToggleButton;
        private final RelativeLayout mSelectTopicLayout;
        private final TextView mTopicNameText;
        private Subject mSubject;
        private Topic mTopic;

        TopicViewHolder(@NotNull View view) {
            mTopicNameText = (TextView) view.findViewById(R.id.subject_topic_name);
            mSelectTopicCheckBox = (CheckBox) view.findViewById(R.id.subject_topic_box);
            mSelectTopicToggleButton = (ToggleButton) view.findViewById(R.id.select_topic_button);
            mSelectTopicLayout = (RelativeLayout) view.findViewById(R.id.select_topic_layout);

            FontUtils.setFiraSansRegular(mSelectTopicCheckBox);
            FontUtils.setFiraSansRegular(view.findViewById(R.id.select_topic_text));

            enableTopicNameClick();
        }

        private void enableTopicNameClick() {
            if (mTopicNameText != null) {
                mTopicNameText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mSelectTopicCheckBox != null) {
                            mSelectTopicCheckBox.toggle();
                        }
                    }
                });
            }
        }

        void clearDefaults() {
            if (mSelectTopicCheckBox != null) {
                mSelectTopicCheckBox.setOnCheckedChangeListener(null);
                mSelectTopicCheckBox.setSelected(false);
            }

            if (mSelectTopicToggleButton != null) {
                mSelectTopicToggleButton.setOnCheckedChangeListener(null);
                mSelectTopicToggleButton.setSelected(false);
            }
        }

        void setSelectAllLayoutVisibility(boolean shouldShow) {
            if (mSelectTopicLayout != null) {
                int visibility = shouldShow ? View.VISIBLE : View.GONE;
                mSelectTopicLayout.setVisibility(visibility);
            }
        }

        void setSubject(Subject subject) {
            mSubject = subject;
        }

        void setTopic(Topic topic) {
            mTopic = topic;
        }

        void enableToggleListener() {
            if (mSelectTopicToggleButton != null) {
                mSelectTopicToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (mSubject != null) {
                            mSubject.markAll(isChecked);
                        }

                        if (!isChecked) {
                            if (mListener != null) {
                                mListener.onSelectionChange(false);
                            }
                        }

                        notifyDataSetChanged();
                    }
                });
            }
        }

        void disableToggleListener() {
            if (mSelectTopicToggleButton != null) {
                mSelectTopicToggleButton.setOnCheckedChangeListener(null);
            }
        }

        void enableCheckBoxListener() {
            if (mSelectTopicCheckBox != null) {
                mSelectTopicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (mTopic != null) {
                            mTopic.setSelected(isChecked);
                        }

                        if (!isChecked) {
                            if (mSubject != null) {
                                mSubject.setAllTopicSelected(false);

                                notifyDataSetChanged();
                            }

                            if (mListener != null) {
                                mListener.onSelectionChange(false);
                            }
                        }
                    }
                });
            }
        }

        void disableCheckBoxListener() {
            if (mSelectTopicCheckBox != null) {
                mSelectTopicCheckBox.setOnCheckedChangeListener(null);
            }
        }

        void toggleToggleButton() {
            disableToggleListener();

            if (mSelectTopicToggleButton != null) {
                boolean status = false;

                if (mSubject != null) {
                    status = mSubject.isAllTopicSelected();
                }

                mSelectTopicToggleButton.setChecked(status);
            }

            enableToggleListener();
        }

        void toggleCheckBox() {
            disableCheckBoxListener();

            if (mSelectTopicCheckBox != null) {
                boolean status = false;

                if (mTopic != null) {
                    status = mTopic.isSelected();
                }

                mSelectTopicCheckBox.setChecked(status);
            }

            enableCheckBoxListener();
        }
    }
}