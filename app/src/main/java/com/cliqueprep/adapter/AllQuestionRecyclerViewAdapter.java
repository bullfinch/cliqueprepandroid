package com.cliqueprep.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;

/**
 * Created by tony on 6/11/16.
 */

public class AllQuestionRecyclerViewAdapter extends RecyclerView.Adapter<AllQuestionRecyclerViewAdapter.ViewHolder> {
    private Context mContext;

    public AllQuestionRecyclerViewAdapter(Context context) {
        mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_question_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AllQuestionRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.fillValues();
    }

    @Override
    public int getItemCount() {
        return 2;
        // TODO: 2 times  card view is displayed
        //TODO: no of original card view is only known when data is received  after doing api
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mItemView;
        private TextView mQuestionString;
        private ImageView mCorrectAnswerImage;
        private ImageView mWrongAnswerImage;
        private RelativeLayout mAnswerBasedColorLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;

            mQuestionString = (TextView) itemView.findViewById(R.id.question_string_text_view);
            mAnswerBasedColorLayout = (RelativeLayout) itemView.findViewById(R.id.question_list_item_color_layout);
            mCorrectAnswerImage = (ImageView) itemView.findViewById(R.id.correct_answer_icon);
            mWrongAnswerImage = (ImageView) itemView.findViewById(R.id.wrong_answer_icon);

            FontUtils.setFiraSansRegular(mQuestionString);
            if (mAnswerBasedColorLayout != null) {
                mAnswerBasedColorLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_accent));
                mWrongAnswerImage.setVisibility(View.VISIBLE);
            }
        }

        public void fillValues() {
        }
    }
}
