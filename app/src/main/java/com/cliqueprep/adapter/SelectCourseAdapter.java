package com.cliqueprep.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cliqueprep.Model.Courses;
import com.cliqueprep.fragment.SubscribedCourseFragment;
import com.cliqueprep.fragment.UnsubscribedCourseFragment;

/**
 * Created by noufy on 19/9/16.
 */
public class SelectCourseAdapter extends FragmentPagerAdapter {
    private Courses mCourse;
    private int mSize;
    private Context mContext;

    public SelectCourseAdapter(FragmentManager fm, Context context) {
        super(fm);
        mSize = 0;
        mContext = context;
    }

    public void setCourse(Courses course) {
        if (course != null) {
            mCourse = course;
            mSize = 2;
            notifyDataSetChanged();
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SubscribedCourseFragment.newInstance(mCourse);
            case 1:
                return UnsubscribedCourseFragment.newInstance(mCourse);
        }
        return null;
    }

    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Subscribed";
            case 1:
                return "Unsubscribed";
        }
        return null;
    }
}