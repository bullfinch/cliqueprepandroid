package com.cliqueprep.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabedFragmentPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private FragmentManager mFragmentManager;

    public TabedFragmentPagerAdapter(FragmentManager manager) {
        super(manager);
        mFragmentManager = manager;
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= 0 && position < mFragmentList.size()) {
            return mFragmentList.get(position);
        }

        return null;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);

        notifyDataSetChanged();
    }

    public void clearAll() {
        for(int i = 0; i < mFragmentList.size(); i++) {
            mFragmentManager.beginTransaction().remove(mFragmentList.get(i)).commit();
        }
        mFragmentList.clear();

        notifyDataSetChanged();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}