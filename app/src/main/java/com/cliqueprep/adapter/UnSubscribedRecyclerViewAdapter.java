package com.cliqueprep.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.Model.Course;
import com.cliqueprep.R;
import com.cliqueprep.activity.PlanSelectionActivity;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by noufy on 25/8/16.
 */

public class UnSubscribedRecyclerViewAdapter extends RecyclerView.Adapter<UnSubscribedRecyclerViewAdapter.ViewHolder> {
    private Context mContext;
    private final ArrayList<Course> mUnsubscribedCourseList;

    public UnSubscribedRecyclerViewAdapter(Context context) {
        mUnsubscribedCourseList = new ArrayList<>();
        mContext = context;
    }

    public void addCourse(Course[] unsubscribedcourse) {
        if (unsubscribedcourse != null && unsubscribedcourse.length > 0) {
            Collections.addAll(mUnsubscribedCourseList, unsubscribedcourse);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_course, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        return mUnsubscribedCourseList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mItemView;
        private final SimpleDraweeView mCourseIconView;
        private final TextView mCourseNameView;
        private final TextView mCourseExamYearView;


        public ViewHolder(View view) {
            super(view);

            mItemView = view;
            mCourseIconView = (SimpleDraweeView) view.findViewById(R.id.unsubscibed_course_view);
            mCourseNameView = (TextView) view.findViewById(R.id.unsubscribed_course_name_text);
            mCourseExamYearView = (TextView) view.findViewById(R.id.unsubscribed_exam_year_text);

            FontUtils.setFirasansBold(mCourseNameView);
            FontUtils.setFirasansBold(mCourseExamYearView);
        }

        public void fillValues(int position) {
            final Course course = mUnsubscribedCourseList.get(position);
            if (course != null) {
                if (!TextUtils.isEmpty(course.getCourseName())) {
                    mCourseNameView.setText(course.getCourseName());
                }

                if (!TextUtils.isEmpty(course.getCourseUrl())) {
                    mCourseIconView.setImageURI(Uri.parse(course.getCourseUrl()));
                }

                mItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, PlanSelectionActivity.class);
                        intent.putExtra(PlanSelectionActivity.EXTRA_COURSE, course);
                        mContext.startActivity(intent);
                    }
                });
            }
        }
    }
}
