package com.cliqueprep.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cliqueprep.Model.Course;
import com.cliqueprep.Model.User;
import com.cliqueprep.R;
import com.cliqueprep.activity.PracticeSelectionActivity;
import com.cliqueprep.util.FontUtils;
import com.facebook.drawee.view.SimpleDraweeView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by noufy on 29/8/16.
 */
public class SubscribedRecyclerViewAdapter extends RecyclerView.Adapter<SubscribedRecyclerViewAdapter.ViewHolder> {
    private static final String EXTRA_COURSE_ID = "course_id";

    private Context mContext;
    private final ArrayList<Course> mSubscribedCourseList;

    public SubscribedRecyclerViewAdapter(Context context) {
        mContext = context;
        mSubscribedCourseList = new ArrayList<>();
    }

    public void addCourse(Course[] subscribedcourse) {
        if (subscribedcourse != null && subscribedcourse.length > 0) {
            Collections.addAll(mSubscribedCourseList, subscribedcourse);
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subscribed, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.fillValues(position);
    }

    @Override
    public int getItemCount() {
        return mSubscribedCourseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mSubItemView;
        private final SimpleDraweeView mSubCourseView;
        private final TextView mSubCourseNameText;
        private final TextView mSubCoursePlanText;
        private final TextView mSubCourseExamYearText;
        private final TextView mSubCourseValidityText;

        public ViewHolder(View view) {
            super(view);

            mSubItemView = view;
            mSubCourseView = (SimpleDraweeView) view.findViewById(R.id.subscribed_course_view);
            mSubCourseNameText = (TextView) view.findViewById(R.id.course_subscribed_name_text);
            mSubCoursePlanText = (TextView) view.findViewById(R.id.course_plan_text);
            mSubCourseExamYearText = (TextView) view.findViewById(R.id.exam_year_text);
            mSubCourseValidityText = (TextView) view.findViewById(R.id.course_validity_text);


            FontUtils.setFirasansBold(mSubCourseNameText);
            FontUtils.setFiraSansRegular(mSubCoursePlanText);
            FontUtils.setFirasansBold(mSubCourseExamYearText);
            FontUtils.setFiraSansRegular(mSubCourseValidityText);
            FontUtils.setFiraSansRegular(mSubCourseValidityText);
        }

        public void fillValues(int position) {
            final Course subcourse = mSubscribedCourseList.get(position);
            if (subcourse != null) {
                if (!TextUtils.isEmpty(subcourse.getCourseName())) {
                    mSubCourseNameText.setText(subcourse.getCourseName());
                }

                if (subcourse.getExamDate() != null) {
                    Date date = subcourse.getExamDate();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
                    String examyear = dateFormat.format(date);
                    mSubCourseExamYearText.setText(examyear);
                }

                if (!TextUtils.isEmpty(subcourse.getCoursePlan())) {
                    mSubCoursePlanText.setText(subcourse.getCoursePlan());
                }

                if (!TextUtils.isEmpty(subcourse.getCourseUrl())) {
                    mSubCourseView.setImageURI(Uri.parse(subcourse.getCourseUrl()));
                }

                if (subcourse.getValidity() != null) {
                    Date date = subcourse.getValidity();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String validity = dateFormat.format(date);
                    mSubCourseValidityText.setText(mContext.getString(R.string.valid_till, validity));
                }

                mSubItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User user = User.load(mContext);
                        user.setSelectedCourse(subcourse);
                        user.save(mContext);
                        Intent intent = new Intent(mContext, PracticeSelectionActivity.class);
                        intent.putExtra(EXTRA_COURSE_ID, subcourse.getCourseId());
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                    }
                });
            }
        }
    }
}