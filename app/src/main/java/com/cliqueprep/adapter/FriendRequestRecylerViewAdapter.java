package com.cliqueprep.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cliqueprep.R;
import com.cliqueprep.util.FontUtils;

public class FriendRequestRecylerViewAdapter extends RecyclerView.Adapter<FriendRequestRecylerViewAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_request_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendRequestRecylerViewAdapter.ViewHolder holder, int position) {
        holder.fillValues();
    }

    @Override
    public int getItemCount() {
        return 3;
        // TODO: 3 times  card view is displayed
        //TODO: no of original card view is only known when data is received  after doing api
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mItemView;
        private TextView mFriendName;
        private TextView mFriendEmail;
        private Button mConfirmButton;
        private Button mRejectButton;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;

            mFriendName = (TextView) itemView.findViewById(R.id.user_name_text_view);
            mFriendEmail = (TextView) itemView.findViewById(R.id.user_email_id_text_view);
            mConfirmButton = (Button) itemView.findViewById(R.id.confirm_friend_request_button);
            mRejectButton = (Button) itemView.findViewById(R.id.reject_friend_request_button);

            FontUtils.setFiraSansRegular(mFriendEmail);
            FontUtils.setFiraSansRegular(mFriendName);
            FontUtils.setFirasansBold(mConfirmButton);
            FontUtils.setFirasansBold(mRejectButton);
        }

        public void fillValues() {
        }
    }
}