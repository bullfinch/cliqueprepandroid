package com.cliqueprep.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cliqueprep.Model.Question;
import com.cliqueprep.fragment.SubQuestionFragment;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by noufy on 4/9/16.
 */
public class SubQuestionsFragmentAdapter extends FragmentStatePagerAdapter {
    private Context mContext;
    ArrayList<Question> mQuestions;

    public SubQuestionsFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        mQuestions = new ArrayList<>();
    }

    public void addQuestions(ArrayList<Question> questions) {
        if (questions != null) {
            mQuestions.addAll(questions);
            notifyDataSetChanged();
        }
    }

    public void addQuestions(Question[] questions) {
        if (questions != null) {
            Collections.addAll(mQuestions, questions);
            notifyDataSetChanged();
        }
    }

    @Override
    public Fragment getItem(int position) {
        if (mQuestions != null && mQuestions.get(position) != null
                && mQuestions.get(position).getSubQuestions() != null
                && mQuestions.get(position).getSubQuestions().length > 0) {
            return SubQuestionFragment.newInstance(mQuestions.get(position).getSubQuestions()[0]);
        } else {
            return new SubQuestionFragment();
        }
    }

    @Override
    public int getCount() {
        return mQuestions.size();
    }
}
