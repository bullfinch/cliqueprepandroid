package com.cliqueprep.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cliqueprep.fragment.ReviewAllQuestionFragment;

/**
 * Created by tony on 4/11/16.
 */

public class QuestionListAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public QuestionListAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext=context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ReviewAllQuestionFragment.newInstance();
            case 1:
                return ReviewAllQuestionFragment.newInstance();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "REVIEW ALL";
            case 1:
                return "REVIEW INCORRECT";
        }

        return null;
    }
}
