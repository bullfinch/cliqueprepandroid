package com.cliqueprep.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cliqueprep.Model.CourePlan;
import com.cliqueprep.fragment.PlanDetailsFragment;

import java.util.ArrayList;

/**
 * Created by noufy on 4/9/16.
 */
public class PlansFragmentAdapter extends FragmentStatePagerAdapter {
    private Context mContext;
    private ArrayList<CourePlan> mPlans;

    public PlansFragmentAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        mPlans = new ArrayList<>();
    }

    public void addPlans(ArrayList<CourePlan> plans) {
        if (plans != null) {
            mPlans.addAll(plans);
            notifyDataSetChanged();
        }
    }

    @Override
    public Fragment getItem(int position) {
        return PlanDetailsFragment.newInstance(mPlans.get(position));
    }

    @Override
    public int getCount() {
        return mPlans.size();
    }
}
